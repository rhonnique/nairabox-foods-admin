import React, { useState, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';


const Add = props => {
    const form = useRef(null);
    const [values, setValues] = useState({});

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        let target = e.target.elements;
        await form.current.validateFields();

        if (form.current.isValid())
            props.handleSubmitAdd(target);
    }


    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Amount</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            name="amount" placeholder="Amount" required
                        />

                        <FieldFeedbacks for="amount">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Placeholder Image</label>
                        <input type="file" className="form-control" name="file_image"
                            onChange={handleChange} />
                    </div>

                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal"
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default Add;