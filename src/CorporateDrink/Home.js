import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import TableRow from "./TableRow";
import Toastr from "../Components/Toastr";
import Add from "./Add";
import Edit from "./Edit";
import * as library from './../Components/libraries'
import $ from 'jquery'

var pluralize = require('pluralize');

const Axios = axios.create();

const CorporateDrink = props => {
    const { api_url } = props;
    const title = `Corporate Drink`;
    const api_path = `drinkcorporate`;
    const permission = `CORPORATE_DRINKS`;
    
    Axios.interceptors.request.use((config) => {
        const token = localStorage.getItem("token");
        config.headers.Authorization = `Bearer ${token}`;
        config.baseURL = api_url;
        return config;
    })

    const [data, setData] = useState([]);
    const [dataLoader, setdataLoader] = useState({});
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [actionLoading, setActionLoading] = useState(false);
    const [actionError, setActionError] = useState(false);

    const [toast, setToast] = useState({
        click: false,
        title:'', 
        type:'',
        msg:'',
    });
    const [toasType, setToastType] = useState('');

    const Actions = () => {
        return (
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)" 
            data-toggle="modal" data-target="#modalForm" 
                onClick={() => setModalTitle(`Add ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> Add {title}
        </a>
        )
    }

    // Get main data
    const fetchData = async () => {
        setdataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        await Axios.get(`/api/backend/${api_path}`)
            .then(response =>{
                setData(response.data);
                setdataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setdataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            })
    };

    const handleSubmitAdd = async (elements) => {
        const name = elements.name.value;
        const amount = elements.amount.value;
        const get_image = elements.file_image.files;
        const image = get_image[0];

        let formData = new FormData();
        formData.append('name', name);
        formData.append('amount', amount);
        if(image !== undefined) formData.append('image', image);

        setActionLoading(true)
        setActionError(false)

        await Axios.post(`/api/backend/${api_path}`, formData)
        .then(response =>{
            const newData = [...data, response.data];
            setData(newData);
            document.getElementById("close-modal").click();
            document.getElementById("form").reset();
        }).catch(error =>{
            console.error(error)
            setActionLoading(false)
            setActionError(true)
        })
    };


    const handleSubmitEdit = async (elements, Id) => {
        const name = elements.name.value;
        const amount = elements.amount.value;
        const status = elements.status.value;
        const get_image = elements.file_image.files;
        const image = get_image[0];

        let formData = new FormData();
        formData.append('name', name);
        formData.append('amount', amount);
        formData.append('status', status);
        if(image !== undefined) formData.append('image', image);

        setActionLoading(true)
        setActionError(false)

        await Axios.put(`/api/backend/${api_path}/${Id}`, formData)
        .then(response =>{
            setData(data.map(d => d._id === Id ? response.data : d));
            document.getElementById("close-modal").click();
            document.getElementById("form").reset();
        }).catch(error =>{
            console.error(error)
            setActionLoading(false)
            setActionError(true)
        })
    }

    const deleteData = async (Id) => {
        try {
            const response = await Axios.delete(`/api/backend/${api_path}/${Id}`);
            setData(data.filter(data => data._id !== Id));
        } catch (error) {
            //setVendorsError(true);
        } finally {
            //setVendorsLoading(false);
        }
    };

    const handleOpenEdit = (row) =>{
        setModalTitle(`Edit ${title}`)
        setEditMode(true);
        setEditData(row);
        
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        fetchData();
    }, [])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions />}
            pageContentCss={`page-content-table`}
        >

            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col" className="cell-200">Amount</th>
                                <th scope="col" className="cell-200 text-center">status</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                    />)
                            })}
                        </tbody>
                    </table>
                )}


                <div className="modal fade" id="modalForm" aria-hidden="true" aria-labelledby="modalForm"
            role="dialog" tabIndex="-1">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    {editMode?(
                    <Edit 
                       handleSubmitEdit={handleSubmitEdit}
                       editData={editData}
                       clearFormData={clearFormData}
                    />
                    ):(
                    <Add handleSubmitAdd={handleSubmitAdd} />
                    )}
                    


                </div>
            </div>
        </div>

        </MasterInner>
    );
}



export default withContext(CorporateDrink);