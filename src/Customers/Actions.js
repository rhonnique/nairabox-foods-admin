import React, { Fragment, useRef, useState, useEffect } from "react";
import { withRouter } from 'react-router'
import { Link, Redirect } from 'react-router-dom';
import queryString from 'query-string'
import $ from 'jquery'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as libraries from "./../Components/libraries"


const Actions = ({ setModalTitle, title, location,
    handleSearch, handleSearchReset, mealCat, restaurants }) => {
    const form = useRef(null);
    const [error, setError] = useState(null)
    const [query, setQuery] = useState(null)
    const [showReset, setShowReset] = useState(false)
    const [startDate, setStartDate] = useState(new Date())

    const handleSubmit = (event) => {
        event.preventDefault();

        let query = {};
        const sDetails = event.target.elements.sDetails.value;
        const sSource = event.target.elements.sSource.value;
        const sDateStart = event.target.elements.sDateStart.value;
        const sDateEnd = event.target.elements.sDateEnd.value;
        const sStatus = event.target.elements.sStatus.value;

        if (!sDetails && !sSource && !sDateStart && !sDateEnd && !sStatus) {
            setError('Plase select at least on option!');
            return;
        }

        const sDate = sDateStart && sDateEnd ? sDateStart + '-' + sDateEnd : null;

        sDetails && Object.assign(query, { details: sDetails });
        sSource && Object.assign(query, { source: sSource });
        sDate && Object.assign(query, { date: sDate });
        sStatus && Object.assign(query, { status: sStatus });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        setQuery(query);
        handleSearch(query);
        $('.dropdown.dropdown-fw.dropdown-mega').trigger('click.bs.dropdown');
    }

    useEffect(() => {
        const { details, source, date, status } = queryString.parse(location.search);
        if (details || source || date || status) {
            setShowReset(true);
        }

        $("[name=sDetails]").val(details);
        $("[name=sSource]").val(source);
        $("[name=sStatus]").val(status);

        if (date) {
            let dates = libraries.dateFormat(date);
            if (dates !== null) {
                $("[name=sDateStart]").val(dates.dateStartString);
                $("[name=sDateEnd]").val(dates.dateEndString);
            }
        } else {
            let getDateNow = libraries.getDateNow();
            $("[name=sDateStart]").val(getDateNow.fullDateSlash);
            $("[name=sDateEnd]").val(getDateNow.fullDateSlash);
        }

        return () => {
            setShowReset(false)
        }
    }, [query])

    useEffect(() => {
        let getDateNow = libraries.getDateNow();
        $("[name=sDateStart]").val(getDateNow.fullDateSlash);
        $("[name=sDateEnd]").val(getDateNow.fullDateSlash);
    }, [showReset])

    return (
        <Fragment>
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)"
                data-toggle="modal" data-target="#modalForm"
                onClick={() => setModalTitle(`Add New ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> <span>Add {title}</span>
            </a>

            <span className="dropdown dropdown-fw dropdown-mega">
                <a className="list-group-item"
                    data-toggle="dropdown" aria-expanded="false"
                    role="button" href="javascript:void(0)">
                    <i className="icon wb-search" aria-hidden="true"></i> <span>Filter {title}</span>
                </a>
                <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right dropdown-menu-form" role="menu">

                    <form onSubmit={handleSubmit} ref={form}>
                        <div className="form-group">
                            <input type="text" className="form-control"
                                name="sDetails" placeholder="Name/Email/Phone"
                            />
                        </div>

                        <div className="form-group">
                            <select name="sSource" className="form-control">
                                <option value="" >- Source -</option>
                                <option value="Web">Web</option>
                                <option value="Mobile">Mobile</option>
                                <option value="Partner">Partner</option>
                            </select>
                        </div>

                        <div className="form-group clearfix">
                            <div className="input-daterange" data-plugin="datepicker">
                                <div className="input-group">
                                    <span className="input-group-addon">
                                        <i className="icon wb-calendar" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" className="form-control" name="sDateStart" />
                                </div>
                                <div className="input-group">
                                    <span className="input-group-addon">to</span>
                                    <input type="text" className="form-control" name="sDateEnd" />
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <select name="sStatus" className="form-control">
                                <option value="" >- Status -</option>
                                <option value="active" >Active</option>
                                <option value="inactive" >Inactive</option>
                            </select>
                        </div>



                        <button type="submit" className="btn btn-block btn-primary">Search</button>
                    </form>
                </div>
            </span>

            {showReset && (<a style={{ float: 'right' }} id="addLabelToggle"
                onClick={() => {
                    setShowReset(false);
                    handleSearchReset();
                    form.current.reset();
                }}
                className="list-group-item" href="javascript:void(0)">
                <i className="icon wb-replay" aria-hidden="true"></i> <span>Reset</span>
            </a>)}







        </Fragment>

    );
}

export default withRouter(Actions);
//export default Actions;