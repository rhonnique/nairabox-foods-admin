import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import TableRow from "./TableRow";
import * as library from './../Components/libraries'
import Add from "./Add";
import Edit from "./Edit";
import Actions from './Actions'
import $ from 'jquery'
import queryString from 'query-string'
import Pagination from "react-js-pagination";

var pluralize = require('pluralize');
const Axios = axios.create();

const Customers = props => {
    const { api_url } = props;
    const title = `Customer`;
    const api_path = `customer`;
    const path = `customers`;
    const permission = `CUSTOMERS`;
    
    Axios.interceptors.request.use((config) => {
        const token = localStorage.getItem("token");
        config.headers.Authorization = `Bearer ${token}`;
        config.baseURL = api_url;
        return config;
    })

    const getUrlQuery = () =>{
        let query = {};
        const { page, details, source, date, status } = queryString.parse(props.location.search);
        page && Object.assign(query, {page : page });
        details && Object.assign(query, {details : details });
        source && Object.assign(query, {source : source });
        date && Object.assign(query, {date : date });
        status && Object.assign(query, {status : status });
        const queryRaw = query;
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        return { query, queryRaw, page, details, source, date, status };
    }

    const initQuery = () =>{
        const {query, queryRaw} = getUrlQuery();
        console.log(queryRaw, 'query')
        return query;
    }

    const paginate = (pageNumber) =>{
        let query = {};
        const { queryRaw } = getUrlQuery();
        Object.assign(query, queryRaw, { page : pageNumber });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        setQuery(query);
        props.history.push(`/${path}` + query);
    }

    const [query, setQuery] = useState(initQuery());

    const [data, setData] = useState([]);
    const [dataMeta, setDataMeta] = useState({});
    const [dataLoader, setdataLoader] = useState({});
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [actionLoading, setActionLoading] = useState(false);
    const [actionError, setActionError] = useState(false);

    const [restaurants, setRestaurants] = useState([]);
    const [restaurantsLL, setRestaurantsLL] = useState(false);
    const [restaurantsErr, setRestaurantsErr] = useState(false);

    const [mealCat, setMealCat] = useState([]);
    const [mealCatLL, setMealCatLL] = useState(false);
    const [mealCatErr, setMealCatErr] = useState(false);

    const [restaurantDetails, setRestaurantDetails] = useState({});
    const [vendorMeals, setVendorMeals] = useState([]);

    const [choices, setChoices] = useState([]);

    /* const Actions = () => {
        return (
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)" 
            data-toggle="modal" data-target="#modalForm" 
                onClick={() => setModalTitle(`Add New ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> Add {title}
        </a>
        )
    } */

    // Get main data
    const fetchData = async () => {
        setdataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        await Axios.get(`${api_url}/api/backend/${api_path}${query}`)
            .then(response =>{
                setData(response.data.data);
                setdataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
                setDataMeta(dt => ({ 
                    ...dt, 
                    total: response.data.total,
                    limit: response.data.limit,
                    current_page: response.data.current_page,
                }));
            })
            .catch(err => {
                setdataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            })
    };

    

    // Meal Restaurantt
    const fetchRestaurant = async () => {
        setRestaurantsLL(true);
        setRestaurantsErr(false);

        try {
            const response = await Axios.get(`/api/site/restaurant`);
            setRestaurants(response.data.data);
        } catch (error) {
            setRestaurantsErr(true);
        } finally {
            setRestaurantsLL(false);
            setRestaurantsErr(false);
        }
    };

    const fetchRestaurantDetails = async (id, callback = ()=>{}) => {
        setRestaurantDetails([]);
        if(id === null)
           return false;
        try {
          const response = await axios.get(`${api_url}/api/site/restaurant/${id}`);
          const details = response.data;
          setRestaurantDetails(details);
          //setCuisines(details.cuisines.map(dc => (dc.cuisine.name)));
        } catch (error) {
            setRestaurantDetails([]);
        } finally {
          //setRestaurantLoading(false);
        }

        if (callback instanceof Function)
           callback();
      };

      const fetchMeals = async (id, callback = ()=>{}) => {
        setVendorMeals([]);
        if(id === null)
           return false;
        try {
          const response = await axios.get(`${api_url}/api/site/mealByRestaurant/${id}`);
          const details = response.data;
          setVendorMeals(details);
        } catch (error) {
            setVendorMeals([]);
        } finally {
            
        };

        if (callback instanceof Function)
           callback();
      };

      const fetchChoice = async (id) => {
        if(id === null)
           return false;
        try {
          const response = await axios.get(`${api_url}/api/site/selectChoice`);
          const details = response.data;
          setChoices(details);
        } catch (error) {
            setChoices([]);
        } finally {
            
        }
      };

    // Meal Category
    const fetchMealCat = async () => {
        setMealCatLL(true);
        setMealCatErr(false);

        try {
            const response = await Axios.get(`/api/site/mealcategory`);
            setMealCat(response.data);
        } catch (error) {
            setMealCatErr(true);
        } finally {
            setMealCatLL(false);
            setMealCatErr(false);
        }
    };

    

    const handleSubmitAdd = async (elements, selectedOptions) => {
        setActionLoading(true)
        setActionError(false)

        const name = elements.name.value;
        const restaurant = elements.restaurant.value;
        const category = elements.category.value;
        const amount = elements.amount.value;
        const pack = elements.pack.value;
        const choice = elements.choice.value;
        const description = elements.description.value;
        const get_image = elements.file_image.files;
        const image = get_image[0];

        let formData = new FormData();
        formData.append('name', name);
        formData.append('restaurant', restaurant);
        formData.append('category', category);
        formData.append('amount', amount);
        formData.append('pack', pack);
        formData.append('choice', choice);
        formData.append('options', selectedOptions.join(','));
        formData.append('description', description);
        if(image !== undefined) formData.append('image', image);

        try {
            const response = await Axios.post(`/api/backend/${api_path}`, formData);
            fetchData();
            //const newData = [...data, response.data];
            //setData(newData);
            document.getElementById("close-modal").click();
            //document.getElementById("form_add").reset();
            //$("#form_add").reset();
        } catch (error) {
            console.error(error)
            setActionLoading(false)
            setActionError(true)
        } finally {
            setActionLoading(false)
            setActionError(false)
        }
    };


    const handleSubmitEdit = async (elements, selectedOptions, Id) => {
        setActionLoading(true)
        setActionError(false)

        const name = elements.name.value;
        const restaurant = elements.restaurant.value;
        const category = elements.category.value;
        const amount = elements.amount.value;
        const pack = elements.pack.value;
        const choice = elements.choice.value;
        const description = elements.description.value;
        const get_image = elements.file_image.files;
        const image = get_image[0];

        let formData = new FormData();
        formData.append('name', name);
        formData.append('restaurant', restaurant);
        formData.append('category', category);
        formData.append('amount', amount);
        formData.append('pack', pack);
        formData.append('choice', choice);
        formData.append('options', selectedOptions.join(','));
        formData.append('description', description);
        if(image !== undefined) formData.append('image', image);
        
        try {
            const response = await Axios.put(`/api/backend/${api_path}/${Id}`, formData);
            setData(data.map(d => d._id === Id ? response.data : d));
            document.getElementById("close-modal").click();
            //document.getElementById("form_edit").reset();
            //$("#form_edit").reset();
        } catch (error) {
            console.error(error)
            setActionLoading(false)
            setActionError(true)
        } finally {
            setActionLoading(false)
            setActionError(false)
        }
    }

    const deleteData = async (Id) => {
        try {
            const response = await Axios.delete(`/api/backend/${api_path}/${Id}`);
            setData(data.filter(data => data._id !== Id));
        } catch (error) {
            //setVendorsError(true);
        } finally {
            //setVendorsLoading(false);
        }
    };

    const handleOpenEdit = (row) =>{
        console.log(row, 'editData');

        setModalTitle(`Edit ${title}`)
        setEditMode(true);
        setEditData(row);
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    const handleSearch = (query) => {
        setQuery(query);
        props.history.push(`/${path}` + query);
    }

    const handleSearchReset = () => {
        setQuery("");
        props.history.push(`/${path}`);
    }

    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        fetchRestaurant();
        fetchMealCat();
        fetchChoice();
    }, [])

    useEffect(() => {
        fetchData();
    }, [query])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions 
                title={title} 
                setModalTitle={setModalTitle} 
                handleSearch={handleSearch} 
                handleSearchReset={handleSearchReset}
                mealCat={mealCat}
                restaurants={restaurants}
                />}
            pageContentCss={`page-content-table`}
        >


            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (<Fragment>
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col" className="cell-350">Details</th>
                                <th scope="col" className="cell-200">Source</th>
                                <th scope="col" className="cell-200 text-center">Status</th>
                                <th scope="col" className="cell-300 text-right">Created At</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                    />)
                            })}
                        </tbody>
                    </table>
                    <Pagination
                    activePage={dataMeta.current_page}
                    itemsCountPerPage={dataMeta.limit}
                    totalItemsCount={dataMeta.total}
                    pageRangeDisplayed={5}
                    onChange={paginate}
                    linkClass="page-link"
                    itemClass="page-item"
                    />
                </Fragment>)}


                <div className="modal fade" id="modalForm" aria-hidden="true" aria-labelledby="modalForm"
            role="dialog" data-backdrop="static" data-keyboard="false">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    {editMode?(
                    <Edit 
                       restaurants={restaurants}
                       mealCat={mealCat}
                       choices={choices}
                       handleSubmitEdit={handleSubmitEdit}
                       editData={editData}
                       clearFormData={clearFormData}
                       fetchMeals={fetchMeals}
                       vendorMeals={vendorMeals}
                       fetchRestaurantDetails={fetchRestaurantDetails}
                       restaurantDetails={restaurantDetails}
                    />
                    ):(
                    <Add 
                       restaurants={restaurants}
                       mealCat={mealCat}
                       choices={choices}
                       handleSubmitAdd={handleSubmitAdd} 
                       clearFormData={clearFormData}
                       fetchRestaurantDetails={fetchRestaurantDetails}
                       restaurantDetails={restaurantDetails}
                       fetchMeals={fetchMeals}
                       vendorMeals={vendorMeals}
                       />
                    )}
                    


                </div>
            </div>
        </div>
        {/* <Toastr 
           trigger={toast}
        /> */}

        {/* <a className="btn btn-primary btn-outline" 
        data-plugin="toastr" style={{display:'none'}}
        ref={click => this.click = click}
                      data-message={toast.msg}
                      data-container-id="toast-top-right" data-title={toast.title} 
                      data-icon-class={`toast-just-text toast-${toast.type}`}
                      href="javascript:void(0)" role="button"></a> */}

        </MasterInner>
    );
}



export default withContext(Customers);