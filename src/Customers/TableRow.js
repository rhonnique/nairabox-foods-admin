import React, { Fragment } from "react";
import moment from "moment";

const TableRow = ({row, handleOpenEdit, handleDeletedata}) => {
    
    return (
        <tr>
            
            <td className="cell-350">
            <strong>{row.first_name} {row.last_name}</strong>
            <div>{row.phone}</div>
            <div>{row.email}</div>
            </td>
            <td className="cell-200">{row.source}</td>
            <td  className="cell-150 text-center">
                {row.status?(
                    <span className="badge badge-outline badge-success">Active</span>
                ):(
                    <span className="badge badge-outline badge-default">Inactive</span>
                )}
            
            </td>
            <td className="cell-300 text-right">
            {moment(row.created_at).format("MMM D, YYYY [at] h:mm a")}
            </td>
            <td className="cell-30">
                <div className="btn-group dropdown">
                    <button type="button" data-toggle="dropdown" aria-expanded="false"
                        className="btn btn-pure btn-default icon wb-more-vertical"></button>
                    <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">
                        <a className="dropdown-item" onClick={() => handleOpenEdit(row)} 
                        href="javascript:void(0)" role="menuitem"
                        data-toggle="modal" data-target="#modalForm">Reset Password</a>
                        <a onClick={() => handleDeletedata(row._id)} 
                        className="dropdown-item" href="javascript:void(0)" role="menuitem">View Orders</a>
                    </div>
                </div>
            </td>
        </tr>

    );
}

export default TableRow;