import React, { Fragment, useRef, useState, useEffect } from "react";
import { withRouter } from 'react-router'
import { withContext } from "./../AppContext";
import { Link, Redirect } from 'react-router-dom';
import queryString from 'query-string'
import $ from 'jquery'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import * as libraries from "./../Components/libraries"


const Actions = ({ setModalTitle, title, location,
    handleSearch, handleSearchReset, mealCat, restaurants, toastr }) => {
    const form = useRef(null);
    const [error, setError] = useState(null)
    const [query, setQuery] = useState(null)
    const [showReset, setShowReset] = useState(false)
    const [startDate, setStartDate] = useState(new Date())

    const handleSubmit = (event) => {
        event.preventDefault();

        let query = {};
        const sDetails = event.target.elements.sDetails.value;
        const sPayOption = event.target.elements.sPayOption.value;
        const sDelivery = event.target.elements.sDelivery.value;
        const sDateStart = event.target.elements.sDateStart.value;
        const sDateEnd = event.target.elements.sDateEnd.value;
        const sStatus = event.target.elements.sStatus.value;
        const sVendor = event.target.elements.sVendor.value;
        const sType = event.target.elements.sType.value;
        const sOrderId = event.target.elements.sOrderId.value;

        if (!sOrderId && !sDetails && !sVendor && !sPayOption && !sDelivery && !sDateStart && !sDateEnd && !sStatus && !sType) {
            toastr('Search Error!', 'Plase select at least one option', 'error');
            return;
        }

        const sDate = sDateStart && sDateEnd ? sDateStart + '-' + sDateEnd : null;

        sOrderId && Object.assign(query, { orderid: sOrderId });
        sType && Object.assign(query, { type: sType });
        sDetails && Object.assign(query, { details: sDetails });
        sVendor && Object.assign(query, { vendor: sVendor });
        sPayOption && Object.assign(query, { payoption: sPayOption });
        sDelivery && Object.assign(query, { delivery: sDelivery });
        sDate && Object.assign(query, { date: sDate });
        sStatus && Object.assign(query, { status: sStatus });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        setQuery(query);
        handleSearch(query);
        $('.dropdown.dropdown-fw.dropdown-mega').trigger('click.bs.dropdown');
    }

    useEffect(() => {
        const { details, payoption, delivery, date, status, orderid, vendor, type } = queryString.parse(location.search);
        if (details || payoption || delivery || date || status || orderid || vendor || type) {
            setShowReset(true);
        }

        $("[name=sDetails]").val(details);
        $("[name=sPayOption]").val(payoption);
        $("[name=sDelivery]").val(delivery);
        $("[name=sStatus]").val(status);
        $("[name=sType]").val(type);
        $("[name=sOrderId]").val(orderid);
        $("[name=sVendor]").val(vendor);

        if (date) {
            let dates = libraries.dateFormat(date);
            if (dates !== null) {
                $("[name=sDateStart]").val(dates.dateStartString);
                $("[name=sDateEnd]").val(dates.dateEndString);
            }
        }

        //console.log(query, 'action::query')

        return () => {
            //console.log('route changed...')
            setShowReset(false)
        }

    }, [query])

    useEffect(() => {
        /* let getDateNow = libraries.getDateNow();
        $("[name=sDateStart]").val(getDateNow.fullDateSlash);
        $("[name=sDateEnd]").val(getDateNow.fullDateSlash); */
    }, [showReset])

    return (
        <Fragment>
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)">
                <i className="icon wb-download" aria-hidden="true"></i> <span>Export to Excel</span>
            </a>

            <span className="dropdown dropdown-fw dropdown-mega">
                <a className="list-group-item"
                    data-toggle="dropdown" aria-expanded="false"
                    role="button" href="javascript:void(0)">
                    <i className="icon wb-search" aria-hidden="true"></i> <span>Filter</span>
                </a>
                <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right dropdown-menu-form" role="menu">

                    <form onSubmit={handleSubmit} ref={form}>


                        <div className="form-group clearfix">
                            <div className="row row-less">
                                <div className="col-6">
                                    <input type="text" className="form-control"
                                        name="sOrderId" placeholder="Order ID"
                                    />
                                </div>

                                <div className="col-6">
                                    <input type="text" className="form-control"
                                        name="sDetails" placeholder="Customer Details"
                                    />
                                </div>
                            </div>
                        </div>




                        <div className="form-group clearfix">
                            <div className="input-daterange" data-plugin="datepicker">
                                <div className="input-group">
                                    <span className="input-group-addon">
                                        <i className="icon wb-calendar" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" autoComplete="off" className="form-control" name="sDateStart" />
                                </div>
                                <div className="input-group">
                                    <span className="input-group-addon">to</span>
                                    <input type="text" autoComplete="off" className="form-control" name="sDateEnd" />
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <select name="sVendor" className="form-control">
                                <option value="" >- Vendor -</option>
                                {libraries.checkObject(restaurants) && restaurants.map((rs, index) => {
                                    return (<option key={index} value={rs._id}>{rs.name}</option>)
                                })}
                            </select>
                        </div>

                        <div className="form-group clearfix">
                            <div className="row row-less">
                                <div className="col-6">
                                    <select name="sPayOption" className="form-control">
                                        <option value="" >- Payment Option -</option>
                                        <option value="CASH ON DELIVERY">Cash On Delivery</option>
                                        <option value="PAY ON DELIVERY">Pay On Delivery</option>
                                        <option value="ONLINE">Online</option>
                                    </select>
                                </div>

                                <div className="col-6">
                                    <select name="sDelivery" className="form-control">
                                        <option value="" >- Delivery Option -</option>
                                        <option value="PICKUP">Pickup</option>
                                        <option value="DELIVERY">Delivery</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div className="form-group clearfix">
                            <div className="row row-less">
                                <div className="col-6">
                                    <select name="sType" className="form-control">
                                        <option value="" >Type</option>
                                        <option value="normal" >Normal</option>
                                        <option value="corporate" >Corporate</option>
                                        <option value="partner" >Partner</option>
                                    </select>
                                </div>

                                <div className="col-6">
                                    <select name="sStatus" className="form-control">
                                        <option value="" >- Status -</option>
                                        <option value="PENDING" >Pending</option>
                                        <option value="CONFIRMED" >Confirmed</option>
                                        <option value="CANCELLED" >Cancelled</option>
                                        <option value="ENROUTE" >Enroute</option>
                                        <option value="DELIVERED" >Delivered</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        <button type="submit" className="btn btn-block btn-primary">Search</button>
                    </form>
                </div>
            </span>

            {showReset && (<a style={{ float: 'right' }} id="addLabelToggle"
                onClick={() => {
                    setShowReset(false);
                    handleSearchReset();
                    form.current.reset();
                }}
                className="list-group-item" href="javascript:void(0)">
                <i className="icon wb-replay" aria-hidden="true"></i> <span>Reset</span>
            </a>)}







        </Fragment>

    );
}

export default withRouter(withContext(Actions));
//export default Actions;