import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import TableRow from "./TableRow";
import Actions from './Actions'
import * as library from './../Components/libraries'
import $ from 'jquery'
import queryString from 'query-string'
import Pagination from "react-js-pagination";
import Details from "./Details";


var pluralize = require('pluralize');
const Axios = axios.create();

const Orders = props => {
    const { api_url } = props;
    const title = `Customer Orders`;
    const api_path = `order`;
    const page = `orders`;
    const permission = `ORDERS`;
    
    Axios.interceptors.request.use((config) => {
        const token = localStorage.getItem("token");
        config.headers.Authorization = `Bearer ${token}`;
        config.baseURL = api_url;
        return config;
    })

    const getUrlQuery = () =>{
        let query = {};
        const { page, details, payoption, delivery, date, status, orderid, type, vendor } = queryString.parse(props.location.search);
        page && Object.assign(query, {page : page });
        details && Object.assign(query, {details : details });
        payoption && Object.assign(query, {payoption : payoption });
        delivery && Object.assign(query, {delivery : delivery });
        date && Object.assign(query, {date : date });
        status && Object.assign(query, {status : status });
        orderid && Object.assign(query, {orderid : orderid });
        type && Object.assign(query, {type : type });
        vendor && Object.assign(query, {vendor : vendor });
        const queryRaw = query;
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        return { query, queryRaw, page, details, payoption, delivery, date, status, orderid, type, vendor };
    }

    const initQuery = () =>{
        const {query, queryRaw} = getUrlQuery();
        //console.log(queryRaw, 'initQuery...')
        return query;
    }

    const paginate = (pageNumber) =>{
        let query = {};
        const { queryRaw } = getUrlQuery();
        Object.assign(query, queryRaw, { page : pageNumber });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        setQuery(query);
        props.history.push(`/${page}` + query);
    }

    const [query, setQuery] = useState(initQuery());

    const [data, setData] = useState([]);
    const [dataMeta, setDataMeta] = useState({});
    const [dataLoader, setDataLoader] = useState({});
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [actionLoading, setActionLoading] = useState(false);
    const [actionError, setActionError] = useState(false);

    const [restaurants, setRestaurants] = useState([]);

    const [mealCat, setMealCat] = useState([]);
    const [mealCatLL, setMealCatLL] = useState(false);
    const [mealCatErr, setMealCatErr] = useState(false);

    const [orderDetails, setOrderDetails] = useState({});
    const [orderList, setOrderList] = useState({loading:true});
    const [orderStatusList, setOrderStatusList] = useState({loading:true});

    // Get main data
    const fetchData = async () => {
        setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.orderGet(query)
            .then(response =>{
                setData(response.data.data);
                setDataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
                setDataMeta(dt => ({ 
                    ...dt, 
                    total: response.data.total,
                    limit: response.data.limit,
                    current_page: response.data.current_page,
                }));
            })
            .catch(err => {
                setDataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    };

    const fetchRestaurant = async () => {
        props.vendorSelect()
            .then(response =>{
                setRestaurants(response.data);
            })
            .catch(err => {
                setRestaurants([]);
            });
    };

    // Get order list
    const fetchOrderList = async (order_id) => {
        setOrderList(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.orderList(order_id)
            .then(response =>{
                setOrderList(dt => ({ ...dt, data: response.data, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setOrderList(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    };

    // Get order status list
    const fetchOrderStatusList = async (order_id) => {
        setOrderStatusList(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.orderStatusList(order_id)
            .then(response =>{
                setOrderStatusList(dt => ({ ...dt, data: response.data, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setOrderStatusList(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    };

    const handleOpenDetails = (row) =>{
        setModalTitle(`ORDER #${row.order_id.toUpperCase()}`)
        setOrderDetails(row);
        fetchOrderList(row.order_id);
        fetchOrderStatusList(row.order_id)
    }

    const handleAddStatus = (formData, order_id) => {
        props.setLoader(true);
        props.orderAddStatus(formData, order_id)
            .then((response) => {
                //var newData = {...data.data}
                let newData = [...orderStatusList.data, response.data];
                //newData = response.data;
                //this.setState({someProperty})


                //const newData = [...data, response.data];
                setOrderStatusList(dt => ({...dt, data: newData }));
            })
            .catch(err => {
                //setActionLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            }).then(()=>props.setLoader(false))
            
    };

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    const handleSearch = (query) => {
        setQuery(query);
        props.history.push(`/${page}` + query);
    }

    const handleSearchReset = () => {
        setQuery("");
        props.history.push(`/${page}`);
    }

    useEffect(() => {
        fetchData();
    }, [query])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    useEffect(() => {
        if(props.location.search === ''){
            handleSearchReset();
        }
    },[props.location.search])

    useEffect(()=>{
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        fetchRestaurant();
    },[])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions 
                title={title} 
                setModalTitle={setModalTitle} 
                handleSearch={handleSearch} 
                handleSearchReset={handleSearchReset}
                mealCat={mealCat}
                restaurants={restaurants}
                toastr={props.setToastr}
                />}
            pageContentCss={`page-content-table`}
        >


            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (<Fragment>
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col" className="cell-250">Order Details</th>
                                {props.user && props.user.auth !== 'VENDOR' && (<th scope="col" className="cell-300">Customer Details</th>)}
        
                                <th scope="col" className="cell-150">Total Amount</th>
                                <th scope="col" className="cell-150">Payment Option</th>
                                <th scope="col" className="cell-150">Delivery Option</th>
                                <th scope="col" className="cell-100 text-center">Status</th>
                                <th scope="col" className="cell-200 text-right">Ordered At</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenDetails={handleOpenDetails}
                                    />)
                            })}
                        </tbody>
                    </table>
                    <Pagination
                    activePage={dataMeta.current_page}
                    itemsCountPerPage={dataMeta.limit}
                    totalItemsCount={dataMeta.total}
                    pageRangeDisplayed={5}
                    onChange={paginate}
                    linkClass="page-link"
                    itemClass="page-item"
                    />
                </Fragment>)}


           <div className="modal fade" id="modalDetails" aria-hidden="true" aria-labelledby="modalDetails"
            role="dialog" data-backdrop="static" data-keyboard="false">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    
                    <Details 
                       details={orderDetails}
                       orderList={orderList}
                       orderStatusList={orderStatusList}
                       vat_rate={props.vat_rate}
                       consumption_rate={props.consumption_rate}
                       handleAddStatus={handleAddStatus}
                       />

                </div>
            </div>
        </div>
        


        </MasterInner>
    );
}



export default withContext(Orders);