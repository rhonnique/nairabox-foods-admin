import React, { Fragment } from "react";
import { withContext } from "./../AppContext";
import moment from "moment";
import Cf from './../Components/Cf';
import * as library from './../Components/libraries';
var pluralize = require('pluralize');

const TableRow = props => {
    const {row, handleOpenDetails} = props;

    return (
        <tr>
            <td>
            #{row.order_id.toUpperCase()}<br/>
            {`${pluralize('Item', row.item_count, true)}`} ordered<br/>
            from {`${pluralize('Restaurant', row.restaurant && row.restaurant.length, true)}`}<br/>
            {library.checkObject(row.discount) && (<Fragment><span className="badge badge-primary">DISCOUNTED</span><br/></Fragment>)}
            {row.is_corp && (<span className="badge badge-warning">CORPORATE ORDER</span>)}
            </td>
            {props.user && props.user.auth !== 'VENDOR' && (
            <td>
                <strong>{row.customer.name}</strong>
                <div>{row.customer.phone}</div>
                <div>{row.customer.email}</div>
                
            </td>)}
            <td><Cf value={row.total_amount} /></td>
            <td>{row.payment_option}</td>
            <td>{row.delivery_option}</td>
            <td className="text-center">
                {row.status}
            </td>
            <td className="text-right">
                {moment(row.created_at).format(`MMM D, YYYY[\n]h:mm a`)}
            </td>
            <td>
                <div className="btn-group dropdown">
                    <button type="button" data-toggle="dropdown" aria-expanded="false"
                        className="btn btn-pure btn-default icon wb-more-vertical"></button>
                    <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">

                        <a className="dropdown-item" onClick={() => handleOpenDetails(row)}
                            href="javascript:void(0)" role="menuitem"
                            data-toggle="modal" data-target="#modalDetails">Order Details</a>
                    </div>
                </div>
            </td>
        </tr>

    );
}

export default withContext(TableRow);