import React, { Fragment, useEffect, useState, useRef } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import moment from "moment";
import Cf from './../Components/Cf';
import { CartSummary, Summary } from "./../Components/Carts";
import * as library from './../Components/libraries';
var pluralize = require('pluralize');

const Details = props => {
    const { details, orderList, vat_rate, consumption_rate, handleAddStatus, orderStatusList } = props;
    const form = useRef();
    const panel = useRef();
    const [values, setValues] = useState({ status: '', comment: '' });

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        await form.current.validateFields();

        if (form.current.isValid()) {
            handleAddStatus(values, details.order_id);
        }
    }

    useEffect(() => {
        //const { sub_total, pack_fee, vat_amount, consumption_amount, total, item_count } 
        // = Summary(orderList, vat_rate, consumption_rate);
        form.current.reset();
        panel.current.click();
        setValues({ status: '', comment: '' })
        console.log('updated...')

    }, [orderStatusList])

    return (<div className="data-summary">
        <ul className="nav nav-tabs nav-tabs-line" role="tablist">
            <li className="nav-item" role="presentation"><a className="nav-link active" data-toggle="tab" href="#tab1"
                aria-controls="tab1" role="tab">ORDER DETAILS</a></li>
            <li className="nav-item" role="presentation"><a className="nav-link" data-toggle="tab" href="#tab2"
                aria-controls="tab2" role="tab">STATUS HISTORY</a></li>
        </ul>


        <div className="modal-body pb-0">
            <div className="row">
                <div className="col-sm-6">
                    <address>
                        {`${pluralize('Item', details.item_count, true)} ordered at `}
                        <Cf value={details.total_amount} /><br />
                        {library.checkObject(details.restaurant) && (<div className="line-height-1-4 py-10">
                            <div>{`${pluralize('Restaurant', details.restaurant && details.restaurant.length)}`}:</div>
                            {details.restaurant.map((rest, index) => {
                                return (
                                    <div key={index} className="mb-10" dangerouslySetInnerHTML={{ __html: `<span class="font-weight-600 text-black">${rest.name}</span><br/><span class="font-size-12">${rest.address}</span>` }} />)
                            })}
                        </div>)}



                        <div className="mb-10">Status is <span className="badge badge-primary">{details.status}</span></div>
                        Placed at
                        <strong className="text-dark">
                            {moment(details.created_at).format(" MMM D, YYYY [at] h:mm a")}
                        </strong><br />
                        Updated at <strong className="text-dark">
                            {moment(details.updated_at).format(" MMM D, YYYY [at] h:mm a")}
                        </strong>
                    </address>
                </div>

                <div className="col-sm-6 text-right">

                    <div className="mb-15">
                        <strong className="text-dark">{details.delivery_option}</strong><br />
                        {details.delivery_option === 'DELIVERY' ? (<Fragment>
                            {details.delivery && details.delivery.address},<br />
                            {details.delivery && details.delivery.location}, {details.delivery && details.delivery.city}
                        </Fragment>) : (<Fragment>
                            {details.pickup && details.pickup.pickup_date}<br />
                            {details.pickup && details.pickup.pickup_time}
                        </Fragment>)}

                    </div>

                    <div className="mb-15">
                        <strong className="text-dark">PAYMENT OPTION</strong><br />
                        {details.payment_option}
                        {details.payment_option === 'CASH ON DELIVERY' && (<Fragment>
                            <br />{details.payment_cash_comment}
                        </Fragment>)}
                    </div>

                    {props.user && props.user.auth !== 'VENDOR' && (<div className="mb-15">
                        <strong className="text-dark">CUSTOMER DETAILS</strong><br />
                        {details.customer && (<Fragment>
                            {details.customer.name}<br />
                            {details.customer.email}<br />
                            {details.customer.phone}
                        </Fragment>)}
                    </div>)}

                    {details.order_comment && (<Fragment>
                        <div className="mb-15">
                            <strong className="text-dark">ORDER COMMENT</strong><br />
                            {details.order_comment}
                        </div>
                    </Fragment>)}


                </div>
            </div>
        </div>

        <hr className="mb-0" />



        <div className="tab-content">
            <div className="tab-pane active" id="tab1" role="tabpanel">


                {orderList.loading && (<div className="modal-body">loading order list, please wait..</div>)}
                {orderList.error && (<div className="modal-body">An error occurred, please refresh page!</div>)}

                {orderList.rd && (<table className="table">
                    <thead>
                        <tr>
                            <th>Item Details</th>
                            <th className="text-right">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orderList.rd && orderList.data && orderList.data.length > 0 && orderList.data.map((row, index) => {
                            let options = [];
                            if (row.options.length > 0) {
                                options = row.options.map(ep => ep.name);
                            }
                            return (<tr key={index}>
                                <td>
                                    <strong className="text-dark">{row.meal ? row.meal.name : 'N/A'}</strong>
                                    {options.length > 0 && (<div><strong>Options:</strong> {options.join(', ')}</div>)}
                                    {row.choice && (<div><strong>Choice:</strong> {row.choice}</div>)}
                                    <div>{pluralize('Portion', row.qty, true)}</div>
                                </td>
                                <td className="text-right"><Cf value={(row.amount * row.qty)} /></td>
                            </tr>)
                        })}
                    </tbody>
                </table>)}

                <div className="modal-body pt-0 clearfix">
                    {orderList.rd && orderList.data && orderList.data.length > 0 && (<CartSummary
                        carts={orderList.data}
                        vat_rate={details.vat_rate}
                        consumption_rate={details.consumption_rate}
                        charges={details.restaurant.charges}
                        delivery_amount={details.delivery_amount}
                        discount={details.discount && details.discount.percentage ? details.discount.percentage : 0}
                    />)}
                </div>

            </div>

            <div className="tab-pane" id="tab2" role="tabpanel">

                {orderStatusList.loading && (<div className="modal-body">loading order list, please wait..</div>)}
                {orderStatusList.error && (<div className="modal-body">An error occurred, please refresh page!</div>)}
                {orderStatusList.rd && orderStatusList.data.length === 0 && (<div className="modal-body">No order status added yet!</div>)}
                {orderStatusList.rd && orderStatusList.data.length > 0 && (
                    <table className="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th scope="col" >Status</th>
                                <th className="cell-200 text-right">Updated At</th>
                            </tr>
                        </thead>
                        <tbody>

                            {orderStatusList.data.map((row, index) => {
                                return (<tr key={index}>
                                    <td>
                                        {row.status}
                                        {row.comment && (<Fragment><br />{row.comment}</Fragment>)}
                                    </td>
                                    <td className="text-right">{moment(row.created_at).format(" MMM D, YYYY [at] h:mm a")}</td>
                                </tr>)
                            })}
                        </tbody>
                    </table>)}


                <div className="panel-group panel-group-continuous m-0" id="exampleAccrodion1" aria-multiselectable="true"
                    role="tablist">

                    <div className="panel">
                        <div className="panel-heading" id="exampleHeadingThird" role="tab">
                            <a ref={panel} className="panel-title collapsed" data-parent="#exampleAccrodion1" data-toggle="collapse"
                                href="#exampleCollapseThird" aria-controls="exampleCollapseThird"
                                aria-expanded="false">
                                <span className="font-weight-900 text-success">UPDATE ORDER STATUS</span>
                            </a>
                        </div>
                        <div className="panel-collapse collapse" id="exampleCollapseThird" aria-labelledby="exampleHeadingThird"
                            role="tabpanel">
                            <div className="panel-body">
                                <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>


                                    <div className="form-group">
                                        <select name="status" value={values.status}
                                            className="form-control" onChange={handleChange} required>
                                            <option value="" >- Status -</option>
                                            <option value="PENDING" >Pending</option>
                                            <option value="CONFIRMED" >Confirmed</option>
                                            <option value="CANCELLED" >Cancelled</option>
                                            <option value="ENROUTE" >Enroute</option>
                                            <option value="DELIVERED" >Delivered</option>
                                        </select>

                                        <FieldFeedbacks for="status">
                                            <FieldFeedback when="*" />
                                        </FieldFeedbacks>
                                    </div>

                                    <div className="form-group">
                                        <textarea className="form-control"
                                            onChange={handleChange} rows="3"
                                            name="comment" placeholder="Comment..."></textarea>
                                    </div>

                                    <hr />

                                    <div className="modal-footer">
                                        <a className="btn btn-sm btn-white" id="close-modal"
                                            data-dismiss="modal" href="javascript:void(0)">Cancel</a>

                                        <button className="btn btn-primary" type="submit">Save</button>
                                    </div>
                                </FormWithConstraints>
                            </div>
                        </div>
                    </div>
                </div>





            </div>

        </div>

















    </div>);
}

export default Details;