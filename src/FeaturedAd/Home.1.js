import React, { useState, useEffect, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import $ from 'jquery'
require('select2');

const Home = (props) => {
    const {featuredMeal} = props;

    return (
        <Fragment>


  

<div class="panel" id="followers">
              <div class="panel-heading">
                <h3 class="panel-title">
                  Followers
                </h3>
                <div class="panel-actions panel-actions-keep">
                  <div class="dropdown">
                    <a class="panel-action" id="examplePanelDropdown" data-toggle="dropdown" href="#"
                      aria-expanded="false" role="button"><i class="icon wb-more-vertical" aria-hidden="true"></i></a>
                    <div class="dropdown-menu dropdown-menu-bullet dropdown-menu-right" aria-labelledby="examplePanelDropdown"
                      role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-flag" aria-hidden="true"></i> Action</a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-print" aria-hidden="true"></i> Another action</a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-heart" aria-hidden="true"></i> Something else here</a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i> Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-body">




                <ul class="list-group list-group-dividered list-group-full h-300" data-plugin="scrollable">
                  <div data-role="container">
                    <div data-role="content">
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-online" href="javascript:void(0)">
                              <img src="../../../global/portraits/9.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-outline btn-default btn-sm">Follow</button>
                            </div>
                            <div>
                              <span>Willard Wood</span>
                            </div>
                            <small>@heavybutterfly920</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-away" href="javascript:void(0)">
                              <img src="../../../global/portraits/10.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-success btn-sm"><i class="icon wb-check" aria-hidden="true"></i>Following</button>
                            </div>
                            <div>
                              <span>Ronnie Ellis</span>
                            </div>
                            <small>@kingronnie24</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-busy" href="javascript:void(0)">
                              <img src="../../../global/portraits/11.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-outline btn-default btn-sm">Follow</button>
                            </div>
                            <div>
                              <span>Gwendolyn Wheeler</span>
                            </div>
                            <small>@perttygirl66</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-off" href="javascript:void(0)">
                              <img src="../../../global/portraits/12.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-outline btn-default btn-sm">Follow</button>
                            </div>
                            <div>
                              <span>Daniel Russell</span>
                            </div>
                            <small>@danieltiger08</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-online" href="javascript:void(0)">
                              <img src="../../../global/portraits/9.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-outline btn-default btn-sm">Follow</button>
                            </div>
                            <div>
                              <span>Willard Wood</span>
                            </div>
                            <small>@heavybutterfly920</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-away" href="javascript:void(0)">
                              <img src="../../../global/portraits/10.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-success btn-sm"><i class="icon wb-check" aria-hidden="true"></i>Following</button>
                            </div>
                            <div>
                              <span>Ronnie Ellis</span>
                            </div>
                            <small>@kingronnie24</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-busy" href="javascript:void(0)">
                              <img src="../../../global/portraits/11.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-outline btn-default btn-sm">Follow</button>
                            </div>
                            <div>
                              <span>Gwendolyn Wheeler</span>
                            </div>
                            <small>@perttygirl66</small>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="media">
                          <div class="pr-20">
                            <a class="avatar avatar-off" href="javascript:void(0)">
                              <img src="../../../global/portraits/12.jpg" alt=""/>
                              <i></i>
                            </a>
                          </div>
                          <div class="media-body w-full">
                            <div class="float-right">
                              <button type="button" class="btn btn-outline btn-default btn-sm">Follow</button>
                            </div>
                            <div>
                              <span>Daniel Russell</span>
                            </div>
                            <small>@danieltiger08</small>
                          </div>
                        </div>
                      </li>
                    </div>
                  </div>
                </ul>


                
              </div>
            </div>





        </Fragment>
    );
}

export default withContext(Home);