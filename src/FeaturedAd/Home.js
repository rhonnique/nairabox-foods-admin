import React, { useState, useEffect, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import $ from 'jquery'
require('select2');

const Home = (props) => {
    const { featuredAd } = props;
    const [details, setDetails] = useState()

    useEffect(() => {
        if(featuredAd.data !== null){
            setDetails(featuredAd.data)
        }
    }, [featuredAd.data])

    return (
        <Fragment>

            <div className="panel">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        Featured AD
                </h3>
                    <div className="panel-actions panel-actions-keep">
                        <a data-toggle="modal" data-target="#modal-featured-ad"
                            href="javascript:void(0)" >
                            <i className="icon wb-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div className="panel-body">

                    <ul className="list-group list-group-dividered list-group-full h-300" data-plugin="scrollable">
                        <div data-role="container">
                            <div data-role="content">

                                {featuredAd.loading && (<div>loading, please wait...</div>)}
                                {featuredAd.error && (<div>An error occurred, please reload page</div>)}
                                {featuredAd.rd && featuredAd.data === null && (<div>No featured AD added yet!</div>)}
                                {featuredAd.rd && featuredAd.data && (<Fragment>

                                    <li className="list-group-item">
                                        <div className="media">
                                            <div className="pr-20">
                                                <a className="avatar avatar-no-radius" href="javascript:void(0)">
                                                    {featuredAd.data.left_image ? (<img src={`${props.image_url}/uploads/${featuredAd.data.left_image}`} alt="" />) : (<img src="./assets/images/placeholder.png" alt="" />)}
                                                </a>
                                            </div>
                                            <div className="media-body w-full">
                                                <div className="text-dark font-weight-500">
                                                    <span>Left Side Advert</span>
                                                </div>
                                                <small>{featuredAd.data.left_link}</small>
                                            </div>
                                        </div>
                                    </li>

                                    <li className="list-group-item">
                                        <div className="media">
                                            <div className="pr-20">
                                                <a className="avatar avatar-no-radius" href="javascript:void(0)">
                                                    {featuredAd.data.righttop_image ? (<img src={`${props.image_url}/uploads/${featuredAd.data.righttop_image}`} alt="" />) : (<img src="./assets/images/placeholder.png" alt="" />)}
                                                </a>
                                            </div>
                                            <div className="media-body w-full">
                                                <div className="text-dark font-weight-500">
                                                    <span>Right Top Side Advert</span>
                                                </div>
                                                <small>{featuredAd.data.righttop_link}</small>
                                            </div>
                                        </div>
                                    </li>

                                    <li className="list-group-item">
                                        <div className="media">
                                            <div className="pr-20">
                                                <a className="avatar avatar-no-radius" href="javascript:void(0)">
                                                    {featuredAd.data.rightbottom_image ? (<img src={`${props.image_url}/uploads/${featuredAd.data.rightbottom_image}`} alt="" />) : (<img src="./assets/images/placeholder.png" alt="" />)}
                                                </a>
                                            </div>
                                            <div className="media-body w-full">
                                                <div className="text-dark font-weight-500">
                                                    <span>Right Bottom Side Advert</span>
                                                </div>
                                                <small>{featuredAd.data.rightbottom_link}</small>
                                            </div>
                                        </div>
                                    </li>
                                </Fragment>)}


                            </div>
                        </div>
                    </ul>

                </div>
            </div>

        </Fragment>
    );
}

export default withContext(Home);