import React, { useState, useRef, Fragment, useEffect } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import $ from 'jquery'


const Edit = props => {
    const form = useRef(null);
    const [values, setValues] = useState(props.featuredAd);

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        let target = e.target.elements;
        await form.current.validateFields();

        if (form.current.isValid()) {
            const get_left_image = target.left_image.files;
            const left_image = get_left_image[0];
            const get_righttop_image = target.righttop_image.files;
            const righttop_image = get_righttop_image[0];
            const get_rightbottom_image = target.rightbottom_image .files;
            const rightbottom_image = get_rightbottom_image[0];

            props.handleSubmitFeaturedAd(left_image, righttop_image, rightbottom_image, values);
        }

    }


    return (
        <Fragment>
            <FormWithConstraints id="form-edit" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                <div className="form-group">
                        <label>Left AD Image <small>(size: 585 × 418")</small></label>
                        <input type="file" className="form-control" name="left_image"
                             onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <label>Left AD Link (Optional)</label>
                        <input type="url" className="form-control"
                            onChange={handleChange} value={values.left_link}
                            name="left_link" placeholder="Left AD Link"
                        />

                        <FieldFeedbacks for="left_link">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Right Top AD Image <small>(size: 585 × 194")</small></label>
                        <input type="file" className="form-control" name="righttop_image"
                             onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <label>Right Top AD Link (Optional)</label>
                        <input type="url" className="form-control"
                            onChange={handleChange} value={values.righttop_link}
                            name="righttop_link" placeholder="Right Top AD Link"
                        />

                        <FieldFeedbacks for="righttop_link">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Right Bottom AD Image <small>(size: 585 × 194")</small></label>
                        <input type="file" className="form-control" name="rightbottom_image"
                             onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <label>Right Bottom AD Link (Optional)</label>
                        <input type="url" className="form-control"
                            onChange={handleChange} value={values.rightbottom_link}
                            name="rightbottom_link" placeholder="Right Bottom AD Link"
                        />

                        <FieldFeedbacks for="rightbottom_link">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" onChange={handleChange}
                        value={values.status}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>


                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal-ad"
                    data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default Edit;