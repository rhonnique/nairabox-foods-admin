import React, { useState, useEffect, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import $ from 'jquery'
require('select2');

const Add = props => {
    const form = useRef(null);
    const [values, setValues] = useState({});
    const [selectMeal, setSelectMeal] = useState([]);
    const [meal, setMeal] = useState({});

    const fetchMeal = () => {
        setMeal({ loading: true, error: false, rd: false })
        props.mealSelect()
            .then(response => {
                setMeal({ loading: true, data: response.data, error: false, rd: true })
                /* if(response.data.length > 0)
                   $("[name=meal]").val(response.data).trigger("change"); */
            }).catch(error => {
                setMeal({ loading: false, error: true, rd: false })
            })
    }

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitFeaturedMeal(selectMeal, values.status);
        }


    }


    useEffect(() => {
        fetchMeal();
        $("[name=meal]").select2({
            placeholder: 'Select meal',
            allowClear: true,
            multiple: true,
            //minimumSelectionLength: 4,
            //maxiumSelectionLength: 4
        }).on('select2:select', function (e) {
            var element = e.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
            setSelectMeal($(this).val());
        }).on('select2:unselecting', function (e) {
            setSelectMeal($(this).val());
        });

        /* if(props.featuredMeal){
            const setOptions = props.featuredMeal.data.meal.map(s => s._id)
            console.log(setOptions, 'setOptions');
        } */



        /* props.fetchMeals(values.meal._id, () => {
            console.log(setOptions, setOptions.length);
            if(setOptions.length > 0)
               $("[name=options]").val(setOptions).trigger("change");
        }); */

        return () => {
            console.log('edit reset..')
            form.current.reset();
        }

    }, [])

    useEffect(() => {
        if(props.featuredMeal.data){
            const setOptions = props.featuredMeal.data.meal.map(s => s._id)
            $("[name=meal]").val(setOptions).trigger("change");
            setSelectMeal($("[name=meal]").val());
            setValues({status:props.featuredMeal.data.status})
        }
    },[props.featuredMeal])

    useEffect(() => {
        console.log(selectMeal, 'selectMeal..');
    },[selectMeal])


    return (
        <Fragment>
            <FormWithConstraints id="form-featured-meal" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Select Meals</label>
                        <select name="meal" className="form-control" required
                            onChange={handleChange}>
                            {meal.rd && meal.data.length > 0 && meal.data.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{`${rs.name} - ${rs.restaurant.name}`}</option>)
                            })}
                        </select>
                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" value={values.status} onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal-meal"
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default withContext(Add);