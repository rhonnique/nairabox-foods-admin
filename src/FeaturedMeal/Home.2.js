import React, { useState, useEffect, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import $ from 'jquery'
require('select2');

const Home = (props) => {
    const {featuredMeal} = props;

    return (
        <Fragment>

            <div className="panel">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        Featured Meal
                </h3>
                    <div className="panel-actions panel-actions-keep">
                        <a data-toggle="modal" data-target="#modal-featured-meal"
                            href="javascript:void(0)" >
                            <i className="icon wb-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    {featuredMeal.loading && (<div>loading, please wait...</div>)}
                    {featuredMeal.error && (<div>An error occurred, please reload page</div>)}
                    {featuredMeal.rd && featuredMeal.data === null && (<div>No featured meal added yet!</div>)}

                    {featuredMeal.rd && featuredMeal.data && (<ul className="list-group list-group-dividered list-group-full h-300" data-plugin="scrollable">
                        <div data-role="container">
                            <div data-role="content">

                                {featuredMeal.data.meal.map((row, index) => {
                                    return (<li key={index} className="list-group-item">
                                        <div className="media">
                                            {row.image && (<div className="pr-20">
                                                <a className="avatar avatar-online" href="javascript:void(0)">
                                                    <img src={`${props.image_url}/uploads/${row.image}`} alt="" />
                                                    <i></i>
                                                </a>
                                            </div>)}
                                            <div className="media-body w-full">
                                                <div className="text-dark font-weight-500">
                                                    <span>{row.name}</span>
                                                </div>
                                                <small>{row.restaurant.name}</small>
                                            </div>
                                        </div>
                                    </li>)
                                })}

                            </div>
                        </div>
                    </ul>)}


                </div>
            </div>



        </Fragment>
    );
}

export default withContext(Home);