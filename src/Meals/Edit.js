import React, { useState, useRef, Fragment, useEffect } from "react";
import {
    FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps
} from 'react-form-with-constraints';

import $ from 'jquery'
require('select2');

const Edit = props => {

    const form = useRef(null);
    const [values, setValues] = useState(props.editData);
    const setOptions = values.options.map(mp => mp._id); //setOptions.split(',');
    const [selectedOptions, setSelectedOptions] = useState([]);

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        let target = e.target.elements;
        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitEdit(target, selectedOptions, values._id);
        }
        
    }

    const handleChangeSelect2 = (name, value) => {
        setValues({ ...values, [name]: value })
    }

    useEffect(() => {
        

        $( "[name=restaurant]" ).select2({
            placeholder: 'Select vendor',
            allowClear: true
        }).on('select2:select', function (e) {
            var data = e.params.data;
            handleChangeSelect2('restaurant', data.id);
            props.fetchRestaurantDetails(data.id)
            props.fetchMeals(data.id)
        }).on('select2:unselecting', function (e) {
            handleChangeSelect2('restaurant', '');
            props.fetchRestaurantDetails(null)
            props.fetchMeals(null)
        });

        $( "[name=category]" ).select2({
            placeholder: 'Select meal category',
            allowClear: true,
        }).on('select2:select', function (e) {
            var data = e.params.data;
            handleChangeSelect2('category', data.id);
        });

        $( "[name=options]" ).select2({
            placeholder: 'Select meal options',
            allowClear: true,
            multiple: true
        }).on('select2:select', function (e) {
            var element = e.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
            setSelectedOptions($(this).val());
        }).on('select2:unselecting', function (e) {
            setSelectedOptions([]);  
        });

        props.fetchMeals(values.restaurant._id, () => {
            console.log(setOptions, setOptions.length);
            if(setOptions.length > 0)
               //$("[name=options]").select2('val', setOptions);
               $("[name=options]").val(setOptions).trigger("change");
        });

        props.fetchRestaurantDetails(values.restaurant._id, () => {
            $("[name=pack]").val(values.pack)
        });

        return () => {
            console.log('edit reset..')
            form.current.reset();
        }

    }, [])

    useEffect(() => {
        console.log('test...')
    }, [])

    return (
        <Fragment>
            <FormWithConstraints id="form_edit" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            value={values.name}
                            name="name" placeholder="Name" required
                        />
                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Vendor</label>
                        <select name="restaurant" className="form-control"
                            value={values.restaurant._id}
                            required onChange={handleChange}>
                            <option value="" ></option>
                            {props.restaurants && props.restaurants.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>

                        <FieldFeedbacks for="restaurant">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Meal Category</label>
                        <select name="category" className="form-control"
                            value={values.category._id}
                            required onChange={handleChange}>
                            <option value="" >- Select Category -</option>
                            {props.mealCat && props.mealCat.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>
                        <FieldFeedbacks for="category">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Meal Options</label>
                        <select name="options" className="form-control"
                             onChange={handleChange}>
                            {props.vendorMeals && props.vendorMeals.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Meal Choice</label>
                        <select name="choice" className="form-control"
                        value={values.choice && values.choice._id}
                             onChange={handleChange}>
                             <option value="">- Select -</option>
                            {props.choices && props.choices.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Pack</label>
                        <select name="pack" className="form-control"
                            onChange={handleChange}>
                            <option value="">None</option>
                            {(props.restaurantDetails && props.restaurantDetails.pack_fee) && props.restaurantDetails.pack_fee.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.category && `${rs.category.name} - ${rs.amount}`}</option>)
                            })}
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Amount</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} value={values.amount}
                            name="amount" placeholder="Amount" required
                        />
                        <FieldFeedbacks for="amount">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>



                    <div className="form-group">
                        <label>Placeholder Image</label>
                        <input type="file" className="form-control" name="file_image"
                            onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control"
                            onChange={handleChange} rows="4" value={values.description}
                            name="description" placeholder="Description"></textarea>
                    </div>

                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal"
                        onClick={props.clearFormData}
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default Edit;