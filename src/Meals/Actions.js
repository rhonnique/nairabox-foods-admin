import React, { Fragment, useRef, useState, useEffect } from "react";
import { withRouter } from 'react-router'
import { Link, Redirect } from 'react-router-dom';
import queryString from 'query-string'
import $ from 'jquery'
require('select2');

const Actions = ({ setModalTitle, title, location,
    handleSearch, handleSearchReset, mealCat, restaurants }) => {
    const form = useRef(null);
    const [error, setError] = useState(null)
    const [query, setQuery] = useState(null)
    const [showReset, setShowReset] = useState(false)

    const handleSubmit = (event) => {
        event.preventDefault();

        let query = {};
        const sName = event.target.elements.sName.value;
        const sCategory = event.target.elements.sCategory.value;
        const sVendor = event.target.elements.sVendor.value;
        const sStatus = event.target.elements.sStatus.value;

        if (!sName && !sCategory && !sVendor && !sStatus) {
            setError('Plase select at least on option!');
            return;
        }

        sName && Object.assign(query, { name: sName });
        sCategory && Object.assign(query, { category: sCategory });
        sVendor && Object.assign(query, { vendor: sVendor });
        sStatus && Object.assign(query, { status: sStatus });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        setQuery(query);
        handleSearch(query);
        $('.dropdown.dropdown-fw.dropdown-mega').trigger('click.bs.dropdown');
    }

    useEffect(() => {
        const { name, vendor, category, status } = queryString.parse(location.search);
        if (name || vendor || category || status) {
            setShowReset(true);
        }

        $("[name=sName]").val(name);
        $("[name=sVendor]").val(vendor);
        $("[name=sCategory]").val(category);
        $("[name=sStatus]").val(status);

        return () => {
            setShowReset(false)
        }
    }, [query])

    return (
        <Fragment>
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)"
                data-toggle="modal" data-target="#modalForm"
                onClick={() => setModalTitle(`Add New ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> <span>Add {title}</span>
            </a>

            <span className="dropdown dropdown-fw dropdown-mega">
                <a className="list-group-item"
                    data-toggle="dropdown" aria-expanded="false"
                    role="button" href="javascript:void(0)">
                    <i className="icon wb-search" aria-hidden="true"></i> <span>Filter {title}</span>
                </a>
                <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right dropdown-menu-form" role="menu">

                    <form onSubmit={handleSubmit} ref={form}>
                        <div className="form-group">
                            <input type="text" className="form-control"
                                name="sName" placeholder="Meal Name"
                            />
                        </div>

                        <div className="form-group">
                            <select name="sCategory" className="form-control"
                            >
                                <option value="" >- Category -</option>
                                {mealCat && mealCat.map((rs, index) => {
                                    return (<option key={index} value={rs._id}>{rs.name}</option>)
                                })}
                            </select>
                        </div>

                        <div className="form-group">
                            <select name="sVendor" className="form-control">
                                <option value="" >- Vendor -</option>
                                {restaurants && restaurants.map((rs, index) => {
                                    return (<option key={index} value={rs._id}>{rs.name}</option>)
                                })}
                            </select>
                        </div>

                        <div className="form-group">
                            <select name="sStatus" className="form-control">
                                <option value="" >- Status -</option>
                                <option value="active" >Active</option>
                                <option value="inactive" >Inactive</option>
                            </select>
                        </div>

                        <button type="submit" className="btn btn-block btn-primary">Search</button>
                    </form>
                </div>
            </span>

            {showReset && (<a style={{ float: 'right' }} id="addLabelToggle"
                onClick={() => {
                    setShowReset(false);
                    handleSearchReset();
                    form.current.reset();
                }}
                className="list-group-item" href="javascript:void(0)">
                <i className="icon wb-replay" aria-hidden="true"></i> <span>Reset</span>
            </a>)}







        </Fragment>

    );
}

export default withRouter(Actions);
//export default Actions;