import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { withContext } from "./../AppContext";
import Footer from "./Footer";

class MasterInner extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { children, setHeader, actions, title, pageContentCss, noBgWhite } = this.props;
        const pgContentCss = pageContentCss ? `page-content ${pageContentCss}` : `page-content`;
        const pgNoBgWhite = noBgWhite ? '' : 'bg-white';
        return (<div className={`page ${pgNoBgWhite}`}>

            {setHeader && (
                <div className="page-header navbar-mega">
                    <h1 className="page-title">{title}</h1>
                    <div className="page-header-actions">{actions}</div>
                </div>
            )}


            <div className={pgContentCss} >{children}</div>
        </div>)
    }

}

export default withContext(MasterInner);