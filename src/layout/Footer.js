import React from "react";
import { withContext } from "./../AppContext";

const Footer = () => {
    return (
        <footer className="site-footer">
        <div className="site-footer-legal">© 2019 <a target="_blank" href="http://www.nairabox.com">Nairabox</a></div>
        <div className="site-footer-right">
            Crafted with <i className="red-600 wb wb-heart"></i> by <a target="_blank" href="https://digicomme.com/wp/">Digicomme</a>
        </div>
    </footer>
    );
}

export default withContext(Footer);