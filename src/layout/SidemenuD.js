import React from "react";
import { Link, NavLink } from 'react-router-dom';
import { withRouter } from 'react-router'

const SidemenuD = props => {
    const { location } = props;
    const current = location.pathname;
    //console.log(location.pathname)
    return (
        <div className="site-menubar">

            <div className="site-menubar-body">
                <div>
                    <div>
                        <ul className="site-menu" data-plugin="menu">

                        </ul>
                    </div>
                </div>
            </div>



        </div>
    );
}

export default withRouter(SidemenuD);