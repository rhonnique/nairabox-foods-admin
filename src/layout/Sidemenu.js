import React, { Fragment } from "react";
import { Link, NavLink } from 'react-router-dom';
import { withRouter } from 'react-router'
import { withContext } from "./../AppContext";
import * as library from './../Components/libraries'

const Sidemenu = props => {
    const { location, user } = props;
    const current = location.pathname;
    return (
        <div className="site-menubar">

            <div className="site-menubar-body">
                <div>
                    <div>
                        <ul className="site-menu" data-plugin="menu">

                            <li className="site-menu-item">
                                <Link to="/">
                                    <i className="site-menu-icon wb-desktop" aria-hidden="true"></i>
                                    <span className="site-menu-title">Dashboard</span>
                                </Link>
                            </li>

                            {library.permissionLink(props.user, 'SETTINGS', 'Settings', 'settings', 'wb-settings')}

                            {props.user && props.user.auth === 'VENDOR' ? (<Fragment>
                                <li className="site-menu-item">
                                    <Link to="/meals">
                                        <i className="site-menu-icon wb-folder" aria-hidden="true"></i>
                                        <span className="site-menu-title">Meals</span>
                                    </Link>
                                </li>

                                <li className="site-menu-item">
                                    <Link to="/corporate-meals">
                                        <i className="site-menu-icon wb-folder" aria-hidden="true"></i>
                                        <span className="site-menu-title">Corporate Meals</span>
                                    </Link>
                                </li>

                                <li className="site-menu-item">
                                    <Link to="/orders">
                                        <i className="site-menu-icon wb-shopping-cart" aria-hidden="true"></i>
                                        <span className="site-menu-title">Orders</span>
                                    </Link>
                                </li>
                            </Fragment>) : (<Fragment>
                                {library.permissionLink(props.user, 'ADMIN', 'Account', 'administrators', 'wb-desktop', true)}


                                {user && user.permissions && user.permissions.length > 0 && user.permissions.map(s => s.slug).includes('CATALOGS') && (<li className={`site-menu-item has-sub`}>
                                    <a href="javascript:void(0)">
                                        <i className="site-menu-icon wb-plugin" aria-hidden="true"></i>
                                        <span className="site-menu-title">Catalog</span>
                                        <span className="site-menu-arrow"></span>
                                    </a>
                                    <ul className="site-menu-sub">
                                        <li className={`site-menu-item`}>
                                            <NavLink className="animsition-link" to="/pack-category">
                                                <span className="site-menu-title">Pack Category</span>
                                            </NavLink>
                                        </li>
                                        <li className={`site-menu-item`}>
                                            <NavLink className="animsition-link" to="/choices">
                                                <span className="site-menu-title">Choices</span>
                                            </NavLink>
                                        </li>
                                        <li className={`site-menu-item`}>
                                            <NavLink className="animsition-link" to="/locations">
                                                <span className="site-menu-title">Locations</span>
                                            </NavLink>
                                        </li>
                                    </ul>
                                </li>)}



                                {library.permissionLink(props.user, 'REPORTS', 'Report', 'reports', 'wb-stats-bars')}

                                {library.permissionLink(props.user, 'ORDERS', 'Orders', 'orders', 'wb-shopping-cart')}

                                {library.permissionLink(props.user, 'CUSTOMERS', 'Customers', 'customers', 'wb-users')}

                                {library.permissionLink(props.user, 'PROMOTIONS', 'Promotions', 'promotions', 'wb-users')}




                                {user && user.permissions && user.permissions.length > 0 && user.permissions.map(s => s.slug).includes('CATALOGS') && (<li className={`site-menu-item has-sub ${current === `/vendors` && `active open`}`}>
                                    <a href="javascript:void(0)">
                                        <i className="site-menu-icon wb-user-circle" aria-hidden="true"></i>
                                        <span className="site-menu-title">Manage Vendor</span>
                                        <span className="site-menu-arrow"></span>
                                    </a>
                                    <ul className="site-menu-sub">
                                        <li className={`site-menu-item ${current === `/vendors` && `active`}`}>
                                            <Link className="animsition-link" to="/vendors">
                                                <span className="site-menu-title">Vendors</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </li>)}


                                {user && user.permissions && user.permissions.length > 0 && user.permissions.map(s => s.slug).includes('CATALOGS') && (<li className={`site-menu-item has-sub ${current === (`/meal-category` || `/meals`) && `active open`}`}>
                                    <a href="javascript:void(0)">
                                        <i className="site-menu-icon wb-folder" aria-hidden="true"></i>
                                        <span className="site-menu-title">Manage Meal</span>
                                        <span className="site-menu-arrow"></span>
                                    </a>
                                    <ul className="site-menu-sub">
                                        <li className={`site-menu-item ${current === `/meal-category` && `active`}`}>
                                            <Link className="animsition-link" to="/meal-category">
                                                <span className="site-menu-title">Meal Category</span>
                                            </Link>
                                        </li>
                                        <li className={`site-menu-item ${current === `/meals` && `active`}`}>
                                            <Link className="animsition-link" to="/meals">
                                                <span className="site-menu-title">Meals</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </li>)}


                                {user && user.permissions && user.permissions.length > 0 && user.permissions.map(s => s.slug).includes('CATALOGS') && (<li className={`site-menu-item has-sub ${current === (`/corporate-drinks` || `/corporate-meals`) && `active open`}`}>
                                    <a href="javascript:void(0)">
                                        <i className="site-menu-icon wb-folder" aria-hidden="true"></i>
                                        <span className="site-menu-title">Corporate Meal</span>
                                        <span className="site-menu-arrow"></span>
                                    </a>
                                    <ul className="site-menu-sub">
                                        <li className={`site-menu-item ${current === `/corporate-drinks` && `active`}`}>
                                            <Link className="animsition-link" to="/corporate-drinks">
                                                <span className="site-menu-title">Drinks</span>
                                            </Link>
                                        </li>
                                        <li className={`site-menu-item ${current === `/corporate-meals` && `active`}`}>
                                            <Link className="animsition-link" to="/corporate-meals">
                                                <span className="site-menu-title">Meals</span>
                                            </Link>
                                        </li>
                                    </ul>
                                </li>)}

                            </Fragment>)}

                        </ul>
                    </div>
                </div>
            </div>

            <div className="site-menubar-footer">
                <a href="javascript: void(0);" className="fold-show" data-placement="top" data-toggle="tooltip"
                    data-original-title="Settings">
                    <span className="icon wb-settings" aria-hidden="true"></span>
                </a>
                <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
                    <span className="icon wb-eye-close" aria-hidden="true"></span>
                </a>
                <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                    <span className="icon wb-power" aria-hidden="true"></span>
                </a>
            </div>


        </div>
    );
}

export default withRouter(withContext(Sidemenu));