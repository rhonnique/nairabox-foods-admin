import React from "react";
import { NavLink } from 'react-router-dom';
import { withContext } from "./../AppContext";

const NavbarD = props => {
    return (
        <nav className="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

            <div className="navbar-header">
                
            </div>

            <div className="navbar-container container-fluid">
                

            </div>
        </nav>
    );
}

export default withContext(NavbarD);