import React from "react";
import { NavLink } from 'react-router-dom';
import { withContext } from "./../AppContext";
import * as library from './../Components/libraries'

const Navbar = props => {
    return (
        <nav className="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

            <div className="navbar-header">
                <button type="button" className="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                    data-toggle="menubar">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="hamburger-bar"></span>
                </button>
                <button type="button" className="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                    data-toggle="collapse">
                    <i className="icon wb-more-horizontal" aria-hidden="true"></i>
                </button>
                <div className="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                    <img className="navbar-brand-logo" src="http://food-server.nairabox.com:8080/nairabox-assets/admin/assets/images/logo.png" title="Nairabox" />
                    <span className="navbar-brand-text hidden-xs-down">Nairabox</span>
                </div>
                <button type="button" className="navbar-toggler collapsed" data-target="#site-navbar-search"
                    data-toggle="collapse">
                    <span className="sr-only">Toggle Search</span>
                    <i className="icon wb-search" aria-hidden="true"></i>
                </button>
            </div>

            <div className="navbar-container container-fluid">
                <div className="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                    <ul className="nav navbar-toolbar">
                        <li className="nav-item hidden-float" id="toggleMenubar">
                            <a className="nav-link" data-toggle="menubar" href="#" role="button">
                                <i className="icon hamburger hamburger-arrow-left">
                                    <span className="sr-only">Toggle menubar</span>
                                    <span className="hamburger-bar"></span>
                                </i>
                            </a>
                        </li>
                        
                     </ul>

                    <ul className="nav navbar-toolbar navbar-right navbar-toolbar-right">
                       <li className="nav-item dropdown">
                            <a className="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                                data-animation="scale-up" role="button">
                                <span>
                                    {props.user && props.user.name}<br/>
                                    <strong>{props.user && library.accountType(props.user.auth)}</strong>
                                </span>
                                <span className="avatar avatar-online">
                                    <img src="http://food-server.nairabox.com:8080/nairabox-assets/admin/global/portraits/5.png" />
                                    <i></i>
                                </span>
                            </a>
                            <div className="dropdown-menu" role="menu">
                            <a className="dropdown-item" href="javascript:void(0)" role="menuitem"><i className="icon wb-settings" aria-hidden="true"></i> Settings</a>
                                <div className="dropdown-divider" role="presentation"></div>
                                <a className="dropdown-item" href="javascript:void(0)" role="menuitem"
                                onClick={() => props.logout()}>
                                <i className="icon wb-power" aria-hidden="true"></i> Logout</a>
                            </div>
                        </li>
                         </ul>

                </div>


                <div className="collapse navbar-search-overlap" id="site-navbar-search">
                    <form role="search">
                        <div className="form-group">
                            <div className="input-search">
                                <i className="input-search-icon wb-search" aria-hidden="true"></i>
                                <input type="text" className="form-control" name="site-search" placeholder="Search..." />
                                <button type="button" className="input-search-close icon wb-close" data-target="#site-navbar-search"
                                    data-toggle="collapse" aria-label="Close"></button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </nav>
    );
}

export default withContext(Navbar);