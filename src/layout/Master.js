import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { withContext } from "./../AppContext";
import Navbar from "./Navbar";
import Sidemenu from "./Sidemenu";
import Footer from "./Footer";
import SidemenuD from "./SidemenuD";
import NavbarD from "./NavbarD";

class Master extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { children, btn } = this.props;
        return (<Fragment>
            {
                this.props.token ?
                    <Fragment>

                        {this.props.user_loading && (<Fragment><NavbarD /><SidemenuD /></Fragment>)}
                        <Fragment>
                            <div style={{ display: this.props.user_loading?'none':'' }}>

                                <Navbar />
                                <Sidemenu />

                                {children}
                                <Footer />
                            </div>
                        </Fragment>
                    </Fragment>
                    :
                    <Fragment>{children}</Fragment>
            }







        </Fragment>)
    }

}

export default withContext(Master);