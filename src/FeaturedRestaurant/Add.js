import React, { useState, useEffect, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import $ from 'jquery'
require('select2');

const Add = props => {
    const form = useRef(null);
    const [values, setValues] = useState({});
    const [selectRestaurant, setSelectRestaurant] = useState([]);
    const [restaurant, setRestaurant] = useState({});

    const fetchRestaurant = () => {
        setRestaurant({ loading: true, error: false, rd: false })
        props.vendorSelect()
            .then(response => {
                setRestaurant({ loading: true, data: response.data, error: false, rd: true })
                /* if(response.data.length > 0)
                   $("[name=restaurant]").val(response.data).trigger("change"); */
            }).catch(error => {
                setRestaurant({ loading: false, error: true, rd: false })
            })
    }

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitFeaturedRestaurant(selectRestaurant, values.status);
        }


    }


    useEffect(() => {
        fetchRestaurant();
        $("[name=restaurant]").select2({
            placeholder: 'Select restaurant',
            allowClear: true,
            multiple: true,
            //minimumSelectionLength: 4,
            //maxiumSelectionLength: 4
        }).on('select2:select', function (e) {
            var element = e.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
            setSelectRestaurant($(this).val());
        }).on('select2:unselecting', function (e) {
            setSelectRestaurant($(this).val());
        });

        /* if(props.featuredRestaurant){
            const setOptions = props.featuredRestaurant.data.restaurant.map(s => s._id)
            console.log(setOptions, 'setOptions');
        } */



        /* props.fetchMeals(values.restaurant._id, () => {
            console.log(setOptions, setOptions.length);
            if(setOptions.length > 0)
               $("[name=options]").val(setOptions).trigger("change");
        }); */

        return () => {
            console.log('edit reset..')
            form.current.reset();
        }

    }, [])

    useEffect(() => {
        if(props.featuredRestaurant.data){
            const setOptions = props.featuredRestaurant.data.restaurant.map(s => s._id)
            $("[name=restaurant]").val(setOptions).trigger("change");
            setSelectRestaurant($("[name=restaurant]").val());
            setValues({status:props.featuredRestaurant.data.status})
        }
    },[props.featuredRestaurant])

    useEffect(() => {
        console.log(selectRestaurant, 'selectRestaurant..');
    },[selectRestaurant])


    return (
        <Fragment>
            <FormWithConstraints id="form-featured-restaurant" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Select Restaurants</label>
                        <select name="restaurant" className="form-control" required
                            onChange={handleChange}>
                            {restaurant.rd && restaurant.data.length > 0 && restaurant.data.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>
                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" value={values.status} onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal-restaurant"
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default withContext(Add);