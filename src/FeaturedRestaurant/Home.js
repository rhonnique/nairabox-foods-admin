import React, { useState, useEffect, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import $ from 'jquery'
import * as library from './../Components/libraries'
require('select2');

const Home = (props) => {
    const { featuredRestaurant } = props;

    return (
        <Fragment>

            <div className="panel">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        Featured Restaurant
                </h3>
                    <div className="panel-actions panel-actions-keep">
                        <a data-toggle="modal" data-target="#modal-featured-restaurant"
                            href="javascript:void(0)" >
                            <i className="icon wb-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div className="panel-body">

                    <ul className="list-group list-group-dividered list-group-full h-300" data-plugin="scrollable">
                        <div data-role="container">
                            <div data-role="content">

                                {featuredRestaurant.loading && (<div>loading, please wait...</div>)}
                                {featuredRestaurant.error && (<div>An error occurred, please reload page</div>)}
                                {featuredRestaurant.rd && featuredRestaurant.data === null && (<div>No featured restaurant added yet!</div>)}

                                {featuredRestaurant.rd && library.checkObject(featuredRestaurant.data) && featuredRestaurant.data.restaurant.map((row, index) => {
                                    return (<li key={index} className="list-group-item">
                                        <div className="media">
                                            <div className="pr-20">
                                                <a className="avatar avatar-online" href="javascript:void(0)">
                                                    {row.image ? (<img src={`${props.image_url}/uploads/${row.image}`} alt="" />) : (<img src="./assets/images/placeholder.png" alt="" />)}
                                                    <i></i>
                                                </a>
                                            </div>
                                            <div className="media-body w-full">
                                                <div className="text-dark font-weight-500">
                                                    <span>{row.name}</span>
                                                </div>
                                                <small>{row.location && row.location.name}</small>
                                            </div>
                                        </div>
                                    </li>)
                                })}

                            </div>
                        </div>
                    </ul>

                </div>
            </div>

        </Fragment>
    );
}

export default withContext(Home);