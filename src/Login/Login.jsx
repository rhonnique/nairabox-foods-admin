import React, { Component } from 'react';
import { withContext } from "../AppContext"
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import $ from 'jquery'

class Login extends Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            errorMessage: ""
        }
    }

    componentWillMount() {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}

        var style = document.createElement('link')
        style.rel = 'stylesheet'
        style.type = 'text/css'
        style.href = 'http://food-server.nairabox.com:8080/nairabox-assets/admin/assets/examples/css/pages/login-v3.css'
        document.head.appendChild(style)

        document.body.classList.add('layout-full', 'page-login-v3');
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.form.validateFields(e.target);
        this.setState({
            [name]: value
        })
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        await this.form.validateFields();

        if (this.form.isValid()) {
            this.props.setLoader(true);
            this.props.login(this.state)
            .then((response) => {
                this.props.setLogin(response.data)
                window.location = this.props.host;
            })
            .catch(error => {
                if(error.response && error.response.data){
                    const { message, code } = error.response.data;
                    this.props.setToastr('Login Error!', `${message} (${code})`, 'error');
                } else {
                    this.props.setToastr('Login Error!', 'Can not log you in, please retry', 'error');
                }
                //this.setState({ errorMessage: err.response.data.message });
                //document.body.classList.remove('layout-full', 'page-login-v3');
            }).then(this.props.setLoader(false))
        } else {
            this.props.setToastr('Login Error!', 'Username and password required', 'error');
        }

        
    }

    clearInputs = () => {
        this.setState({
            username: "",
            password: "",
            errorMessage: ""
        })
    }

    render() {
        return (

            <div className="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
                <div className="page-content vertical-align-middle animation-slide-top animation-duration-1">
                    <div className="panel">
                        <div className="panel-body login-form">
                            <div className="brand">
                                <img className="brand-img" src="http://food-server.nairabox.com:8080/nairabox-assets/admin/assets/images/logo-colored.png" alt="..." />
                                <h2 className="brand-text font-size-18">Nairabox</h2>
                            </div>
                            <FormWithConstraints ref={form => this.form = form} onSubmit={this.handleSubmit} noValidate>

                                {
                                    this.state.errorMessage &&
                                    <p style={{ color: "red" }}>{this.state.errorMessage}</p>
                                }
                                <div className="form-group">
                                    <label>Email</label>
                                    <input
                                        onChange={this.handleChange}
                                        value={this.state.username}
                                        name="username"
                                        type="text" required
                                        className="form-control"
                                        placeholder="Email Address" />
                                    <FieldFeedbacks for="username">
                                        <FieldFeedback when="*" />
                                    </FieldFeedbacks>
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input
                                        onChange={this.handleChange}
                                        value={this.state.password}
                                        name="password"
                                        type="password" required
                                        className="form-control"
                                        placeholder="password" />
                                    <FieldFeedbacks for="password" className="invalid-feedback">
                                        <FieldFeedback when="*" />
                                    </FieldFeedbacks>
                                </div>
                                <div className="form-group clearfix">
                                    <div className="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                                        <input type="checkbox" id="inputCheckbox" name="remember" />
                                        <label htmlFor="inputCheckbox">Remember me</label>
                                    </div>
                                    {/* <a className="float-right" href="forgot-password.html">Forgot password?</a> */}
                                </div>
                                <button className="btn btn-primary btn-block btn-lg mt-40" type="submit">Sign in</button>

                            </FormWithConstraints>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default withContext(Login);