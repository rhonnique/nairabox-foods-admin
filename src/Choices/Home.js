import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import TableRow from "./TableRow";
import Toastr from "../Components/Toastr";
import Add from "./Add";
import Edit from "./Edit";
import $ from 'jquery'

var pluralize = require('pluralize');
const axiosCreate = axios.create();

const Home = props => {
    const { api_url } = props;
    const title = `Choice`;
    const api_path = `choice`;
    
    axiosCreate.interceptors.request.use((config) => {
        const token = localStorage.getItem("token");
        config.headers.Authorization = `Bearer ${token}`;
        config.baseURL = api_url;
        return config;
    })

    const [data, setData] = useState([]);
    const [dataLoading, setDataLoading] = useState(false);
    const [DataError, setDataError] = useState(false);
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [actionLoading, setActionLoading] = useState(false);
    const [actionError, setActionError] = useState(false);

    const [clearForm, setClearForm] = useState(false);

    const Actions = () => {
        return (
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)" 
            data-toggle="modal" data-target="#modalForm" 
                onClick={() => setModalTitle(`Add ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> Add {title}
        </a>
        )
    }

    // Get main data
    const fetchData = async () => {
        setDataLoading(true);
        setDataError(false);

        try {
            const response = await axiosCreate.get(`/api/backend/${api_path}`);
            setData(response.data);
        } catch (error) {
            setDataLoading(false);
            setDataError(true);
        } finally {
            setDataLoading(false);
            setDataError(false);
        }
    };

    const handleSubmitAdd = async (formData, choices) => {
        setActionLoading(true)
        setActionError(false)

        formData.choices = choices;
        try {
            const response = await axiosCreate.post(`/api/backend/${api_path}`, formData);
            const newData = [...data, response.data];
            setData(newData);
            document.getElementById("close-modal").click();
            document.getElementById("form_add").reset();
            setClearForm(true);
        } catch (error) {
            console.error(error)
            setActionLoading(false)
            setActionError(true)
        } finally {
            setActionLoading(false)
            setActionError(false)
            setClearForm(false);
        }
    };


    const handleSubmitEdit = async (formData, choices, Id) => {
        setActionLoading(true)
        setActionError(false)

        formData.choices = choices;
        try {
            const response = await axiosCreate.put(`/api/backend/${api_path}/${Id}`, formData);
            setData(data.map(d => d._id === Id ? response.data : d));
            document.getElementById("close-modal").click();
            document.getElementById("form").reset();
            setClearForm(true);
        } catch (error) {
            
            console.error(error)
            setActionLoading(false)
            setActionError(true)
        } finally {
            setActionLoading(false)
            setActionError(false)
            setClearForm(false);
        }
    }

    const deleteData = async (Id) => {

        const conf = window.confirm('Do you want to delete selected value?');
        if(!conf)
           return false;

        try {
            const response = await axiosCreate.delete(`/api/backend/${api_path}/${Id}`);
            setData(data.filter(data => data._id !== Id));
        } catch (error) {
            //setVendorsError(true);
        } finally {
            //setVendorsLoading(false);
        }
    };

    const handleOpenEdit = (row) =>{
        setModalTitle(`Edit ${title}`)
        setEditMode(true);
        setEditData(row);
        
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        fetchData();
    }, [])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions />}
            pageContentCss={`page-content-table`}
        >

            {DataError && (
                <div>an error occured</div>
            )}

            {dataLoading && (
                <div>Loading ...</div>
            )}

            {!DataError && !dataLoading && data.length === 0 && (
                <div>No {title} added yet.</div>
            )}

            {!DataError && !dataLoading && data.length > 0 && (
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col" className="cell-200">Choices</th>
                                <th scope="col" className="cell-200 text-center">status</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                    />)
                            })}
                        </tbody>
                    </table>
                )}


                <div className="modal fade" id="modalForm" aria-hidden="true" aria-labelledby="modalForm"
            role="dialog" tabIndex="-1">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    {editMode?(
                    <Edit 
                       handleSubmitEdit={handleSubmitEdit}
                       editData={editData}
                       clearFormData={clearFormData}
                       clearForm={clearForm}
                    />
                    ):(
                    <Add 
                       handleSubmitAdd={handleSubmitAdd}
                       clearForm={clearForm}
                    />
                    )}
                    


                </div>
            </div>
        </div>
        {/* <Toastr 
           trigger={toast}
        /> */}

        {/* <a className="btn btn-primary btn-outline" 
        data-plugin="toastr" style={{display:'none'}}
        ref={click => this.click = click}
                      data-message={toast.msg}
                      data-container-id="toast-top-right" data-title={toast.title} 
                      data-icon-class={`toast-just-text toast-${toast.type}`}
                      href="javascript:void(0)" role="button"></a> */}

        </MasterInner>
    );
}



export default withContext(Home);