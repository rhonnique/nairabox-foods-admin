import React, { useState, useRef, Fragment, useEffect } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
var pluralize = require('pluralize');

const Edit = props => {
    const form = useRef(null);
    const [values, setValues] = useState(props.editData);
    const [selectedChoices, setSelectedChoices] = useState(props.editData.choices);
    const [spValue, setSpValue] = useState(props.editData.choices.length > 0 ? true : "");

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitEdit(values, selectedChoices, values._id);
        }
    }

    const addChoice = (e) => {
        const { value } = e.target;
        if (value === "")
            return false;

        const newChoice = [...selectedChoices, ""];
        setSelectedChoices(newChoice)
        setSpValue("true");
    }

    const updateSelectedChoice = (e, index) => {
        const { value } = e.target;
        const newChoice = [...selectedChoices];
        newChoice[index] = value;
        setSelectedChoices(newChoice);
    }

    const removeChoice = index => {
        const newChoice = [...selectedChoices];
        newChoice.splice(index, 1);
        setSelectedChoices(newChoice);
    };

    useEffect(() => {
        if (props.clearForm === true) {
            setSelectedChoices([])
        }
        
    }, [props.clearForm]);


    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">


                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            value={values.name}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>




                    <div className="form-group">
                        <label>Add Choice</label>
                        <select name="add_choice" className="form-control"
                            value={spValue}
                            onChange={addChoice} required>
                            <option value="" >- Select -</option>
                            {selectedChoices.length > 0 && (
                                <option value={true} >
                                    {`${selectedChoices.length} ${pluralize('choice', selectedChoices.length > 1 ? 2 : 1)} added`}
                                </option>
                            )}

                            <option value="1">Add</option>
                        </select>
                        <FieldFeedbacks for="add_choice">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    {selectedChoices.length > 0 && selectedChoices.map((choice, index) => {
                        return (<div key={index} className="form-group option-list full">
                            <i className="wb-close-mini" onClick={() => removeChoice(index)} aria-hidden="true"></i>
                            <input type="text" className="form-control form-control-sm"
                                value={choice}
                                onChange={(e) => updateSelectedChoice(e, index)}
                                name={`choice_${index}`} placeholder="Add Choice" required />
                            <FieldFeedbacks for={`choice_${index}`}>
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>)
                    })}






                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" value={values.status} onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>


                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal"
                        onClick={props.clearFormData}
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default Edit;