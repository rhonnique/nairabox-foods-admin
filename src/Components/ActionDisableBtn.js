import React from "react";

const ActionDisableBtn = () => <button type="button" class="btn btn-pure btn-default icon wb-more-vertical"></button>

export default ActionDisableBtn;