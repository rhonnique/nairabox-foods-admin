import React, {useState, useEffect, forwardRef, useRef, useImperativeHandle} from "react";
import axios from "axios";

const axiosCreate = axios.create();

const CityDD = ({handleCityChange}) => {
    const [city, setCity] = useState([]);
    const [cityLL, setCityLL] = useState(false);
    const [cityErr, setCityErr] = useState(false);

    const fetchCity = async () => {
        setCityErr(false);
        setCityLL(true);

        try {
            const response = await axiosCreate.get(`/api/site/city`);
            setCity(response.data);
        } catch (error) {
            setCityErr(true);
        } finally {
            setCityLL(false);
        }
    };

    useEffect(() => {
        fetchCity();
    }, [])

    return (
        <select onChange={e => handleCityChange(e.target.value)} className="form-control">
        <option value="" >- Select City {place} -</option>
        {city.map((city, index)=>{
            return (<option key={index} value={city._id}>{city.name}</option>)
        })}
        </select>
    );
}

export default CityDD;