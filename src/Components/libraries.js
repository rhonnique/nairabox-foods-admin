import React from "react";
import { Link } from 'react-router-dom';

/* const libraries = {
    dateFormat(date_string){
        let date = date_string.split('-');
        if(!date[0] || !date[1]){
            return null;
        }
    
        if(isDate(date[0]) === true || isDate(date[1]) === true){
            return null;
        }
    
        const dateStartString = date[0];
        const dateEndString = date[1];
        const dateStartFormat = getDates(date[0]).fullDate;
        const dateEndFormat = getDates(date[1]).fullDate;
    
        return { dateStartString, dateEndString, dateStartFormat, dateEndFormat }
    },

    isDate(date){
        return isNaN(new Date(date));
    },
    getDates(date){
        date = new Date(date);
        const day = date.getDate();
        const month = (date.getMonth() + 1);
        const year = date.getFullYear();
        const fullDate = `${year}, ${month}, ${day}`;
        return { day, month, year, fullDate }
    },

    bar() { return 47744 },
}

export default libraries */

export const dateFormat = (date_string) => {
    let date = date_string.split('-');
    if (!date[0] || !date[1]) {
        return null;
    }

    if (isDate(date[0]) === true || isDate(date[1]) === true) {
        return null;
    }

    const dateStartString = date[0];
    const dateEndString = date[1];
    const dateStartFormat = getDates(date[0]).fullDate;
    const dateEndFormat = getDates(date[1]).fullDate;

    return { dateStartString, dateEndString, dateStartFormat, dateEndFormat }
}

export const isDate = (date) => {
    return isNaN(new Date(date));
}

export const getDates = (date) => {
    date = new Date(date);
    const day = date.getDate();
    const month = (date.getMonth() + 1);
    const year = date.getFullYear();
    //const fullDate = `${year}, ${month}, ${day}`;
    const fullDate = `${month}/${day}, ${year}`;
    return { day, month, year, fullDate }
}

export const getDateNow = () => {
    const date = new Date();
    const day = date.getDate();
    const month = (date.getMonth() + 1);
    const year = date.getFullYear();
    const fullDate = `${year}, ${month}, ${day}`;
    const fullDateSlash = `${month}/${day}/${year}`;
    return { day, month, year, fullDate, fullDateSlash }
}

export const checkObject = (obj) => {
    return obj && obj !== null && Object.entries(obj).length > 0;
}

export const accountType = (type) => {
    let name = '';
    switch(type){
        case 'ADMIN':
        name = 'Administrator';
        break;
        case 'VENDOR':
        name = 'Vendor';
        break;
        case 'PARTNER':
        name = 'Partner';
        break;
    }
    return name;
}

export const permission = (user, permission, host) => {
    const vendor = ['MEALS','CORPORATE_MEALS','ORDERS']
    if (user) {
        if(user.auth === 'VENDOR' && !vendor.includes(permission)){
            window.location = host;
        } else if (user.permissions && user.permissions.length > 0) {
            if (!user.permissions.map(s => s.slug).includes(permission)) {
                window.location = host;
            }
        }/*  else {
            window.location = host;
        } */


        /* if(user.auth !== 'VENDOR' && !vendor.includes(permission)){
            if (user.permissions && user.permissions.length > 0) {
                if (!user.permissions.map(s => s.slug).includes(permission)) {
                    window.location = host;
                }
            } else {
                window.location = host;
            }
        } */
    }
}

export const permissionLink = (user, permission, name, link, icon) => {
    if (user) {
        //if(admin){
            if (user.permissions && user.permissions.length > 0) {
                if (user.permissions.map(s => s.slug).includes(permission)) {
                    return (<li className="site-menu-item">
                        <Link to={`/${link}`}>
                            <i className={`site-menu-icon ${icon}`} aria-hidden="true"></i>
                            <span className="site-menu-title">{name}</span>
                        </Link>
                    </li>)
                }
            }
        //}  
    }
}

export const permissionLinkGroup = (user, permission, name, link, icon) => {
    if (user) {
        //if(admin){
            if (user.permissions && user.permissions.length > 0) {
                if (user.permissions.map(s => s.slug).includes(permission)) {
                    return (<li className="site-menu-item">
                        <Link to={`/${link}`}>
                            <i className={`site-menu-icon ${icon}`} aria-hidden="true"></i>
                            <span className="site-menu-title">{name}</span>
                        </Link>
                    </li>)
                }
            }
        //}  
    }
}

