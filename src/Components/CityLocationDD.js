import React, { useState, useEffect, forwardRef, useRef, useImperativeHandle } from "react";
import axios from "axios";

const axiosCreate = axios.create();

const City = ({ name, handleCityChange, value = false }) => {
    const [city, setCity] = useState([]);
    const [cityLL, setCityLL] = useState(false);
    const [cityErr, setCityErr] = useState(false);

    const [cityValue, setCityValue] = useState(value);
    const [currentValue, setCurrentValue] = useState([]);

    const fetchCity = async () => {
        setCityErr(false);
        setCityLL(true);

        try {
            const response = await axiosCreate.get(`/api/site/city`);
            setCity(response.data);
            console.log(response.data, 'fetchCity')
        } catch (error) {
            setCityErr(true);
        } finally {
            setCityLL(false);
        }
    };

    /* if(value){
        handleCityChange(value)
        console.log(value, 'value._id')
    } */

    useEffect(() => {
        fetchCity();
        /* if(value){
            handleCityChange(currentValue)
        } */
    }, [])

    return (
        <select value={value?value:currentValue} name={name} onChange={(e) => {
            handleCityChange(e.target.value);
            setCurrentValue(e.target.value)
            }} className="form-control">
            <option value="" >- Select City -</option>
            {city.map((city, index) => {
                return (<option key={index} value={city._id}>{city.name}</option>)
            })}
        </select>
    );
}



const Location = ({ cityValue, name, value }) => {
    const [location, setLocation] = useState([]);
    const [locationLL, setLocationLL] = useState(false);
    const [locationErr, setLocationErr] = useState(false);

    const fetchLocation = async () => {
        setLocationErr(false);
        setLocationLL(true);

        try {
            const response = await axiosCreate.get(`/api/site/locationByCity/${cityValue}`);
            setLocation(response.data);
            console.log(response.data,'fetchLocation')
        } catch (error) {
            setLocationErr(true);
        } finally {
            setLocationLL(false);
        }
    };

    useEffect(() => {
        if (cityValue !== '') {
            fetchLocation()
        }

    }, [cityValue]);

    /* watch(setCityValue, (newId) => {
        console.log(newId)
      }); */

    return (<div>
        <select value={value?value._id:value} name={name} className="form-control">
            <option value="" >- Select Location -</option>
            {location.map((location, index) => {
                return (<option key={index} value={location._id}>{location.name}</option>)
            })}
        </select>
    </div>)
}

export { City, Location };