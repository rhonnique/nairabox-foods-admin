import React, {useState, useEffect, Fragment} from "react";

const Toastr = ({trigger}) => {

    useEffect(() => {
        if(trigger.click === true){
            console.log(trigger, 'trigger')
            this.click.click()
        }
        
    }, [trigger])

    /* const trigger = () =>{
        alert(3333333)
    } */

    return (
        <a className="btn btn-primary btn-outline" 
        data-plugin="toastr" style={{display:'none'}}
        ref={click => this.click = click}
                      data-message={trigger.msg?trigger.msg:'message'}
                      data-container-id="toast-top-right" data-title={trigger.title?trigger.title:'title'} 
                      data-icon-class={`toast-just-text toast-${trigger.type}`}
                      href="javascript:void(0)" role="button"></a>
    );
}

export default Toastr;