import React, { useState, useEffect, Fragment } from "react";

const ActionDrops = ({ row, handleOpenEdit, handleDeletedata }) => {
    //console.log(row)

    useEffect(() => {

    }, [])

    return (
        <div className="btn-group dropdown">
            <button type="button" data-toggle="dropdown" aria-expanded="false"
                className="btn btn-pure btn-default icon wb-more-vertical"></button>
            <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">
                <a className="dropdown-item"  href="javascript:void(0)" role="menuitem"
                    data-toggle="modal" data-target="#modalForm" onClick={() => handleOpenEdit(row)}>Edit</a>
                <a className="dropdown-item" href="javascript:void(0)" role="menuitem" 
                onClick={() => handleDeletedata(row._id)}>Delete</a>
            </div>
        </div>
    );
}

export default ActionDrops;