import React, { Fragment, useState, useEffect, forwardRef, useRef, useImperativeHandle } from "react";
import Cf from './Cf';


const CartSummary = ({ carts, vat_rate, consumption_rate, delivery_amount, discount }) => {

    const { sub_total, pack_fee, vat_amount, consumption_amount, total, discount_amount } =
        Summary(carts, vat_rate, consumption_rate, delivery_amount, discount);

    /* useEffect(() => {
        handleSummaryTotal(total)
    }, [total]) */

    return (
        <Fragment>
            {carts.length > 0 && (
                <ul className="basket-summary">
                    <li>Subtotal <span><Cf value={sub_total} /></span></li>
                    <li>VAT <span><Cf value={vat_amount} /></span></li>
                    <li>Pack Fee <span><Cf value={pack_fee} /></span></li>
                    <li>Consumption Fee <span><Cf value={consumption_amount} /></span></li>
                    <li>Delivery Fee <span><Cf value={delivery_amount?delivery_amount:0} /></span></li>
                    <li>Discount <span>- <Cf value={discount_amount} /></span></li>
                    <li>Total <span className="_discount__" ><Cf value={total} /></span></li>
                </ul>
            )}
        </Fragment>
    )
}


const Summary = (carts, vat_rate, consumption_rate, delivery_amount, discount = 0) => {
    let item_count = carts.reduce((a, b) => +a + b.qty, 0);
    let sub_total = carts.reduce((a, b) => +a + (+b.amount * b.qty), 0);
    let pack_fee = carts.reduce((a, b) => +a + (+b.pack_amount * b.qty), 0);
    let vat_amount = (sub_total / 100) * vat_rate;
    let consumption_amount = (sub_total / 100) * consumption_rate;
    let consumption_charge_count = 0;

    /* Count how many restaurant to charge for consumption if corporate */
    const cartsOnDrinks = carts.filter(s => s.is_corp_drink === false)

    try {
        consumption_charge_count = Array.from(new Set(cartsOnDrinks.map(s => s.restaurant._id)))
            .map(id => {
                return {
                    id: id,
                    charges: cartsOnDrinks.find(s => s.restaurant._id === id).restaurant.charges
                }
            }).filter(s => s.charges === true).length;
    } catch (e) {
        consumption_charge_count = 0;
    }

    consumption_amount = (consumption_charge_count * consumption_amount)

    /* Set discount */
    let discount_amount = discount ? (sub_total / 100) * discount : 0;

    item_count = item_count && !isNaN(item_count) ? item_count : 0;
    sub_total = sub_total && !isNaN(sub_total) ? sub_total : 0;
    pack_fee = pack_fee && !isNaN(pack_fee) ? pack_fee : 0;
    vat_amount = vat_amount && !isNaN(vat_amount) ? vat_amount : 0;
    delivery_amount = delivery_amount && !isNaN(delivery_amount) ? delivery_amount : 0;
    consumption_amount = consumption_amount && !isNaN(consumption_amount) ? consumption_amount : 0;
    discount_amount = discount_amount && !isNaN(discount_amount) ? discount_amount : 0;

    let total = ((sub_total - discount_amount) + vat_amount + pack_fee + consumption_amount + delivery_amount);

    return { sub_total, pack_fee, vat_amount, consumption_amount, total, item_count, discount_amount };
}



/* 
const Summary = (carts, vat_rate, consumption_rate, charges) => {
    let item_count = carts.reduce((a, b) => +a + b.qty, 0);
    let sub_total = carts.reduce((a, b) => +a + (+b.amount * b.qty), 0);
    let pack_fee = carts.reduce((a, b) => +a + (+b.pack_amount * b.qty), 0);
    let vat_amount = (sub_total / 100) * vat_rate;
    let consumption_amount = charges ? (sub_total / 100) * consumption_rate : 0;
    
    item_count = item_count && !isNaN(item_count) ?item_count:0;
    sub_total = sub_total && !isNaN(sub_total) ?sub_total:0;
    pack_fee = pack_fee && !isNaN(pack_fee) ?pack_fee:0;
    vat_amount = vat_amount && !isNaN(vat_amount) ?vat_amount:0;
    consumption_amount = consumption_amount && !isNaN(consumption_amount) ?consumption_amount:0;
    
    let total = (sub_total + vat_amount + pack_fee + consumption_amount);

    return { sub_total, pack_fee, vat_amount, consumption_amount, total, item_count };
} */

export { CartSummary, Summary };