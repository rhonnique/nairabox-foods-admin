import React, {Fragment} from 'react';
import NumberFormat from 'react-number-format';

function Cf(props) {
    
    return (
        <NumberFormat value={props.value} decimalScale={2} fixedDecimalScale={true}
        displayType={'text'} thousandSeparator={true}
        renderText={value => <span className='_currency___' dangerouslySetInnerHTML={{__html:`&#8358;` + value }} />} />
    )
}

export default Cf;
 