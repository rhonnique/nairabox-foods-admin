import React, { Fragment, useRef, useState, useEffect } from "react";
import { withRouter } from 'react-router'
import { Link, Redirect } from 'react-router-dom';
import queryString from 'query-string'
import $ from 'jquery'

const Actions = props => {
    const form = useRef(null);
    const [error, setError] = useState(null)
    const [query, setQuery] = useState(null)
    const [showReset, setShowReset] = useState(false)

    const handleSubmit = (event) => {
        event.preventDefault();

        let query = {};
        const sName = event.target.elements.sName.value;
        const sCity = event.target.elements.sCity.value;
        const sLocation = event.target.elements.sLocation.value;
        const sStatus = event.target.elements.sStatus.value;

        if (!sName && !sCity && !sLocation && !sStatus) {
            setError('Plase select at least on option!');
            return;
        }
        
        sName && Object.assign(query, { name: sName });
        sCity && Object.assign(query, { city: sCity });
        sLocation && Object.assign(query, { location: sLocation });
        sStatus && Object.assign(query, { status: sStatus });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        setQuery(query);
        props.handleSearch(query);
        $('.dropdown.dropdown-fw.dropdown-mega').trigger('click.bs.dropdown');
    }

    useEffect(() => {
        const { name, city, location, status } = queryString.parse(props.location.search);
        if (name || city || location || status) {
            setShowReset(true);
        }

        $("[name=sName]").val(name);
        $("[name=sLocation]").val(city);
        $("[name=sCity]").val(location);
        $("[name=sStatus]").val(status);

        return () => {
            setShowReset(false)
        }
    }, [query])

    return (
        <Fragment>
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)"
                data-toggle="modal" data-target="#modalForm"
                onClick={() => props.setModalTitle(`Add New ${props.title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> <span>Add {props.title}</span>
            </a>

            <span className="dropdown dropdown-fw dropdown-mega">
                <a className="list-group-item"
                    data-toggle="dropdown" aria-expanded="false"
                    role="button" href="javascript:void(0)">
                    <i className="icon wb-search" aria-hidden="true"></i> <span>Filter {props.title}</span>
                </a>
                <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right dropdown-menu-form" role="menu">

                    <form onSubmit={handleSubmit} ref={form}>
                        <div className="form-group">
                            <input type="text" className="form-control"
                                name="sName" placeholder="Name"
                            />
                        </div>

                        <div className="form-group">
                                <label>City</label>
                                <select name="sCity" onChange={(e) => {
                                    props.handleCityChange(e.target.value)
                                }} className="form-control">
                                    <option value="" >- City -</option>
                                    {props.city && props.city.map((city, index) => {
                                        return (<option key={index} value={city._id}>{city.name}</option>)
                                    })}
                                </select>
                            </div>

                            <div className="form-group add">
                                <label>Location</label>
                                <select name='sLocation' className="form-control">
                                    <option value="" >- Location -</option>
                                    {props.locations && props.locations.map((location, index) => {
                                        return (<option key={index} value={location._id}>{location.name}</option>)
                                    })}
                                </select>
                            </div>

                        <div className="form-group">
                            <select name="sStatus" className="form-control">
                                <option value="" >- Status -</option>
                                <option value="active" >Active</option>
                                <option value="inactive" >Inactive</option>
                            </select>
                        </div>

                        <button type="submit" className="btn btn-block btn-primary">Search</button>
                    </form>
                </div>
            </span>

            {showReset && (<a style={{ float: 'right' }} id="addLabelToggle"
                onClick={() => { 
                    setShowReset(false); 
                    props.handleSearchReset(); 
                    form.current.reset();
                }}
                className="list-group-item" href="javascript:void(0)">
                <i className="icon wb-replay" aria-hidden="true"></i> <span>Reset</span>
            </a>)}







        </Fragment>

    );
}

export default withRouter(Actions);
//export default Actions;