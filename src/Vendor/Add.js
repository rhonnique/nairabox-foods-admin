import React, { useState, useRef, Fragment, useEffect } from "react";
import { City, Location } from "../Components/CityLocationDD"
import {
    FormWithConstraints, FieldFeedbacks,
    FieldFeedback, Async as Async_, AsyncProps
} from 'react-form-with-constraints';
import * as Toastr from 'toastr';
var slug = require('slug')
var pluralize = require('pluralize');

const VendorFormAdd = props => {
    const form = useRef(null);
    const [selectedPackCat, setSelectedPackCat] = useState([]);
    const [spValue, setSpValue] = useState("");
    const [selectedCuisines, setSelectedCuisines] = useState(new Map());

    const handleChange = (e) => {
        form.current.validateFields(e.target);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        let target = e.target.elements;
        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitAdd(target, selectedPackCat, document.getElementsByName('selected_cuisines'))
        } else {
            Toastr.error('Feilds required',
                'Validation Error!');
        }


    }

    const removePack = Id => {
        setSelectedPackCat(selectedPackCat.filter(c => c._id !== Id));
    }

    const addContainer = (e) => {
        const { value } = e.target;
        if (value === "")
            return false;

        const _id = value;
        const index = e.nativeEvent.target.selectedIndex;
        const name = e.nativeEvent.target[index].text;
        const amount = "";

        let check = selectedPackCat.some(c => c._id === _id)
        if (!check) {
            const newCat = [...selectedPackCat, { _id, name, amount }];
            setSelectedPackCat(newCat)
            setSpValue("true");
        }
    }

    const updateSelectedPack = (e, Id) => {
        const { value } = e.target;
        var current = selectedPackCat.find(e => e._id === Id);
        current.amount = value;
        setSelectedPackCat(selectedPackCat.map(c => c._id === Id ? current : c));
    }

    const handleCuisineChange = (e) => {
        /* const item = e.target.name;
        const isChecked = e.target.checked;
        const newCheck = [...selectedCuisines, { item, isChecked }];
        setSelectedCuisines(newCheck) */
    }

    useEffect(() => {
        console.log(selectedCuisines, 'selectedPackCat')
    },[selectedCuisines]);

    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Address</label>
                        <textarea rows="3" name="address" placeholder="Address"
                            onChange={handleChange} required
                            className="form-control"></textarea>
                        <FieldFeedbacks for="address">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group add">
                                <label>City</label>
                                <select name="city" required onChange={(e) => {
                                    props.handleCityChange(e.target.value)
                                }} className="form-control">
                                    <option value="" >- Select City -</option>
                                    {props.city && props.city.map((city, index) => {
                                        return (<option key={index} value={city._id}>{city.name}</option>)
                                    })}
                                </select>
                                <FieldFeedbacks for="city">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group add">
                                <label>Location</label>
                                <select name='location' className="form-control" required>
                                    <option value="" >- Select Location -</option>
                                    {props.location && props.location.map((location, index) => {
                                        return (<option key={index} value={location._id}>{location.name}</option>)
                                    })}
                                </select>
                                <FieldFeedbacks for="location">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Placeholder Image</label>
                                <input type="file" className="form-control" name="file_image"
                                    onChange={handleChange} required />
                                <FieldFeedbacks for="file_image">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Banner</label>
                                <input type="file" className="form-control" name="file_banner"
                                    onChange={handleChange} required />
                                <FieldFeedbacks for="file_banner">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Weekdays Opening Time</label>
                                <input type="text" required className="form-control" onChange={handleChange}
                                    name="weekdays_opening" placeholder="Weekdays Opening Time" />
                                <FieldFeedbacks for="weekdays_opening">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Weekend Opening Time</label>
                                <input type="text" required className="form-control" onChange={handleChange}
                                    name="weekends_opening" placeholder="Weekend Opening Time" />
                                <FieldFeedbacks for="weekends_opening">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Contact Person</label>
                                <input type="text" className="form-control" onChange={handleChange}
                                    name="contact_name" placeholder="Contact Person" required />
                                <FieldFeedbacks for="contact_name">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Contact Phone</label>
                                <input type="text" className="form-control" onChange={handleChange}
                                    name="contact_phone" placeholder="Contact Phone" required />
                                <FieldFeedbacks for="contact_phone">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Delivery Time</label>
                                <input type="text" className="form-control" onChange={handleChange}
                                    name="delivery" placeholder="Delivery Time" required />
                                <FieldFeedbacks for="delivery">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Status</label>
                                <select name="status" onChange={handleChange}
                                    className="form-control" required>
                                    <option value="">- Select -</option>
                                    <option value={true}>Active</option>
                                    <option value={false}>Inactive</option>
                                </select>
                                <FieldFeedbacks for="status">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <hr />

                    {/* ****** Cuisines ****** */}
                    <div className="form-group">
                        <label>Cuisine</label>
                        {props.cuisinesLoading && (<div>..loading cuisines</div>)}
                        {props.cuisinesError && (<div>Can not load cuisines</div>)}
                        {props.cuisines && (
                            <div className="row">
                                {props.cuisines.map((cuisine, index) => {
                                    return (<div key={index} className="col-sm-4">
                                        <div className="checkbox-custom checkbox-primary checkbox-inline">
                                            <input name="selected_cuisines" onChange={handleCuisineChange}
                                                value={cuisine._id} type="checkbox" id={'cuisine_' + cuisine._id} />
                                            <label htmlFor={'cuisine_' + cuisine._id}>{cuisine.name}</label>
                                        </div>
                                    </div>)
                                })}
                                <input style={{ display: 'none' }} type="text" name="cuisines" />
                            </div>

                        )}

                        <FieldFeedbacks for="cuisines">
                            <FieldFeedback when="*" >Select at least one option</FieldFeedback>
                        </FieldFeedbacks>
                    </div>
                    {/* ****** Cuisines ****** */}

                    <hr />


                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>MOU</label>
                                <input type="file" className="form-control" name="file_mou"
                                    onChange={handleChange} />
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Logo</label>
                                <input type="file" className="form-control" name="file_logo"
                                    onChange={handleChange} />

                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-4">
                            <div className="form-group">
                                <label>Bank Name</label>
                                <input type="text" className="form-control"
                                    onChange={handleChange}
                                    name="account_bank" placeholder="Bank Name"
                                />
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="form-group">
                                <label>Account Name</label>
                                <input type="text" className="form-control"
                                    onChange={handleChange}
                                    name="account_name" placeholder="Account Name"
                                />
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="form-group">
                                <label>Account Number</label>
                                <input type="text" className="form-control"
                                    onChange={handleChange}
                                    name="account_number" placeholder="Account Number"
                                />
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Vendor Percentage</label>
                                <div className="input-group">
                                    <input type="text" className="form-control"
                                        onChange={handleChange} value={0}
                                        name="share_percentage" placeholder="Charges Rate"
                                    />
                                    <span className="input-group-btn">
                                        <button type="button" className="btn btn-default btn-outline">%</button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Charge Consumption Fee</label>
                                <select name="charges" onChange={handleChange}
                                    className="form-control" required>
                                    <option value="">- Select -</option>
                                    <option value={true}>Yes</option>
                                    <option value={false}>No</option>
                                </select>
                                <FieldFeedbacks for="charges">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <br />


                    <div>

                        <div className="form-group">
                            <label>Pack Fee</label>
                            <select name="pack_category" className="form-control"
                                value={spValue}
                                onChange={addContainer} required>
                                <option value="" >- Add Pack Category -</option>
                                {selectedPackCat.length > 0 && (
                                    <option value={true} >
                                        {/* `${selectedPackCat.length} ${pluralize('pack category', selectedPackCat.length > 1 ? 2 : 1)} added` */}
                                        {`${selectedPackCat.length} pack category added`}
                                    </option>
                                )}

                                {props.packCategory && props.packCategory.map((pack, index) => {
                                    return (<option key={index} value={pack._id}>{pack.name}</option>)
                                })}
                            </select>
                            <FieldFeedbacks for="pack_category">
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>

                        {selectedPackCat.length > 0 && selectedPackCat.map((pack, index) => {
                            const form_name = slug(pack.name, { lower: true, replacement: '_' });
                            return (<div key={index} className="form-group option-list">
                                <label>{pack.name}</label>
                                <i className="wb-close-mini" onClick={() => removePack(pack._id)} aria-hidden="true"></i>
                                <input type="number" className="form-control form-control-sm" min="1"
                                    value={pack.amount}
                                    onChange={(e) => updateSelectedPack(e, pack._id)}
                                    name={form_name} placeholder={`${pack.name} Amount`} required />
                                <FieldFeedbacks for={form_name}>
                                    <span style={{ paddingLeft: '120px', display: 'inline-block' }}><FieldFeedback when="*" /></span>
                                </FieldFeedbacks>
                            </div>)
                        })}
                    </div>


                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default VendorFormAdd;