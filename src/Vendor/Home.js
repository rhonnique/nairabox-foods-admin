import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import VensorsTableRow from "./TableRow";
import Add from "./Add";
import Edit from "./Edit";
import ArrangeMenu from "./ArrangeMenu";
import Actions from './Actions'
import * as library from './../Components/libraries'
import $ from 'jquery'
import queryString from 'query-string'
import Pagination from "react-js-pagination";

const pluralize = require('pluralize');
const Axios = axios.create();

const Vendor = props => {
    const { api_url } = props;
    const title = `Vendor`;
    const api_path = `restaurant`;
    const path = `vendors`;
    const permission = `VENDORS`;

    Axios.interceptors.request.use((config) => {
        const token = localStorage.getItem("token");
        config.headers.Authorization = `Bearer ${token}`;
        config.baseURL = api_url;
        return config;
    })

    const getUrlQuery = () =>{
        let query = {};
        const { page, name, vendor, category, status } = queryString.parse(props.location.search);
        page && Object.assign(query, {page : page });
        name && Object.assign(query, {name : name });
        vendor && Object.assign(query, {vendor : vendor });
        category && Object.assign(query, {category : category });
        status && Object.assign(query, {status : status });
        const queryRaw = query;
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        return { query, queryRaw, page, name, vendor, category, status };
    }

    const initQuery = () =>{
        const {query, queryRaw} = getUrlQuery();
        console.log(queryRaw, 'query')
        return query;
    }

    const paginate = (pageNumber) =>{
        let query = {};
        const { queryRaw } = getUrlQuery();
        Object.assign(query, queryRaw, { page : pageNumber });
        query = $.isEmptyObject(query) ? '' : `?${decodeURIComponent($.param(query))}`;
        console.log(query, 'paginate query')
        setQuery(query);
        props.history.push(`/${path}` + query);
    }
    
    const [query, setQuery] = useState(initQuery());

    const [data, setData] = useState([]);
    const [dataMeta, setDataMeta] = useState({});
    const [dataLoader, setDataLoader] = useState({});


    const [actionLoader, setActionLoader] = useState({});

    const [cuisines, setCuisines] = useState([]);
    const [cuisinesLoading, setCuisinesLoading] = useState([]);
    const [cuisinesError, setCuisinesError] = useState([]);
    
    const [city, setCity] = useState([]);
    const [cityLL, setCityLL] = useState(false);
    const [cityErr, setCityErr] = useState(false);
    const [cityValue, setCityValue] = useState('');
    

    const [location, setLocation] = useState([]);
    const [locationLL, setLocationLL] = useState(false);
    const [locationErr, setLocationErr] = useState(false);
    const [locationValue, setLocationValue] = useState('');


    const [actionLoading, setActionLoading] = useState(false);
    const [actionError, setActionError] = useState(false);
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [page, setPage] = useState(1);

    const [packCategory, setPackCategory] = useState([]);

    const [meals, setMeals] = useState({data:[],loading:false,error:false,rd:false});
    const [selectedVendorId, setSelectedVendorId] = useState(null);

    /* Fetch main data */
    const fetchData = async () => {
        setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.vendorGet(query)
            .then((response) => {
                setData(response.data.data);
                setDataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
                setDataMeta(dt => ({ 
                    ...dt, 
                    total: response.data.total,
                    limit: response.data.limit,
                    current_page: response.data.current_page,
                }));
            })
            .catch(error => {
                setDataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    };

    // Get cuisines
    const fetchCuisines = async () => {
        setCuisinesLoading(true);
        setCuisinesError(false);

        try {
            const response = await Axios.get(`/api/site/cuisine`);
            setCuisines(response.data);
        } catch (error) {
            setCuisinesError(true);
        } finally {
            setCuisinesLoading(false);
        }
    };

    const fetchCity = async () => {
        props.selectHandler("[name=city]", true);
        const response = await props.citySelect();
        setCity(response.data);
        props.selectHandler("[name=city]", false);
    };

    const fetchLocation = async (city) => {
        if(!city){
            setLocation([])
            return;
        }
        props.selectHandler("[name=city]", true);
        props.selectHandler("[name=location]", true);
        const response = await props.locationSelect(city);
        setLocation(response.data);
        props.selectHandler("[name=city]", false);
        props.selectHandler("[name=location]", false);
    };

    const fetchPackCategory = () => {

        props.packCategoryGet()
            .then(response =>{
                setPackCategory(response.data)
            })
            .catch(err => {
                
            }).then(() => {

        })
    }

    const fetchMealByVendor = (restaurant_id) => {
        setMeals(dt => ({ ...dt, loading: true, error: false }));
        props.getMealByVendor(restaurant_id)
            .then(response =>{
                setMeals(dt => ({ ...dt, data: response.data, loading: false, error: false, rd:true }))
            })
            .catch(err => {
                setMeals(dt => ({ ...dt, loading: false, error: true }));
            }).then(() => {});
    }

    const handleSubmitAdd = async (elements, pack_fee, all_cuisines) => {

        var selected_cuisines = [];
        for (var i = 0; i < all_cuisines.length; i++) {
            if (all_cuisines[i].checked) {
                selected_cuisines.push(all_cuisines[i].value);
            }
        }

        if(selected_cuisines.length === 0){
            props.setToastr('Validation Error!','Select at least one cuisine','error');
            return;
        }

        const name = elements.name.value;
        const address = elements.address.value;
        const city = elements.city.value;
        const location = elements.location.value;
        const weekdays_opening = elements.weekdays_opening.value;
        const weekends_opening = elements.weekends_opening.value;
        const contact_name = elements.contact_name.value;
        const contact_phone = elements.contact_phone.value;
        const delivery = elements.delivery.value;

        const account_bank = elements.account_bank.value;
        const account_name = elements.account_name.value;
        const account_number = elements.account_number.value;

        const share_percentage = elements.share_percentage.value;
        const charges = elements.charges.value;
        const status = elements.status.value;

        const get_image = elements.file_image.files;
        const image = get_image[0];
        const get_banner = elements.file_banner.files;
        const banner = get_banner[0];
        const get_mou = elements.file_mou.files;
        const mou = get_mou[0];
        const get_logo = elements.file_logo.files;
        const logo = get_logo[0];

        //let geoData = null;
        let geoData = {
            address_formatted:'',
            longitude:3.429396,
            latitude:6.434524,
            place_id:"",
        };

        /* await props.getLongLats(address)
        .then(response => {
            const {formatted_address, place_id} = response.results[0];
            const { lat, lng } = response.results[0].geometry.location;
            geoData = { lat, lng, formatted_address, place_id };
            },
            error => {
              props.setToastr('Geo Location Error!','Can not get address data, please retry','error')
              return;
            }
          );

          if(geoData === null){
              return;
          } */

        let formData = new FormData();
        formData.append('name', name);
        formData.append('address', address);
        formData.append('city', city);
        formData.append('location', location);
        formData.append('openings', JSON.stringify({
            'weekdays': weekdays_opening,
            'weekends': weekends_opening,
        }));

        formData.append('contact', JSON.stringify({
            'name': contact_name,
            'phone': contact_phone,
        }));

        formData.append('account', JSON.stringify({
            'bank': account_bank,
            'name': account_name,
            'number': account_number,
        }));

        formData.append('geo', JSON.stringify(geoData));
        formData.append('pack_fee', JSON.stringify(pack_fee));

        formData.append('cuisines', selected_cuisines.join(','));
        formData.append('delivery', delivery);
        formData.append('share_percentage', share_percentage);
        formData.append('charges', charges);
        formData.append('status', status);

        formData.append('image', image);
        formData.append('banner', banner);
        formData.append('mou', mou);
        if(logo !== undefined) formData.append('logo', logo);

        SubmitAdd(formData);
    };

    const SubmitAdd = async (formData) => {
        props.setLoader(true);
        try {
            const response = await Axios.post(`/api/backend/restaurant`, formData);
            const newData = [...data, response.data];
            setData(newData);
            document.getElementById("close-modal").click();
            document.getElementById("form").reset();
            props.setToastr('Success!','New data successfully added','success')
        } catch (error) {
            props.setToastr('Error!','An error occurred, please retry','error')
        } finally {
            props.setLoader(false);
        }
    }


    const handleSubmitEdit = async (elements, Id, pack_fee, all_cuisines) => {
        var selected_cuisines = [];
        for (var i = 0; i < all_cuisines.length; i++) {
            if (all_cuisines[i].checked) {
                selected_cuisines.push(all_cuisines[i].value);
            }
        }

        if(selected_cuisines.length === 0){
            props.setToastr('Validation Error!','Select at least one cuisine','error');
            return;
        }

        const name = elements.name.value;
        const address = elements.address.value;
        const city = elements.city.value;
        const location = elements.location.value;
        const weekdays_opening = elements.weekdays_opening.value;
        const weekends_opening = elements.weekends_opening.value;
        const contact_name = elements.contact_name.value;
        const contact_phone = elements.contact_phone.value;
        const delivery = elements.delivery.value;

        const account_bank = elements.account_bank.value;
        const account_name = elements.account_name.value;
        const account_number = elements.account_number.value;

        const share_percentage = elements.share_percentage.value;
        const status = elements.status.value;
        const charges = elements.charges.value;

        const get_image = elements.file_image.files;
        const image = get_image[0];
        const get_banner = elements.file_banner.files;
        const banner = get_banner[0];
        const get_mou = elements.file_mou.files;
        const mou = get_mou[0];
        const get_logo = elements.file_logo.files;
        const logo = get_logo[0];

        //let geoData = null;
        let geoData = {
            address_formatted:'',
            longitude:3.429396,
            latitude:6.434524,
            place_id:"",
        };

        /* await props.getLongLats(address)
        .then(response => {
            const {formatted_address, place_id} = response.results[0];
            const { lat, lng } = response.results[0].geometry.location;
            geoData = { lat, lng, formatted_address, place_id };
            },
            error => {
              props.setToastr('Geo Location Error!','Can not get address data, please retry','error')
              return;
            }
          );

          if(geoData === null){
              return;
          } */

        let formData = new FormData();
        formData.append('name', name);
        formData.append('address', address);
        formData.append('city', city);
        formData.append('location', location);
        formData.append('openings', JSON.stringify({
            'weekdays': weekdays_opening,
            'weekends': weekends_opening,
        }));

        formData.append('contact', JSON.stringify({
            'name': contact_name,
            'phone': contact_phone,
        }));

        formData.append('account', JSON.stringify({
            'bank': account_bank,
            'name': account_name,
            'number': account_number,
        }));

        formData.append('geo', JSON.stringify(geoData));

        formData.append('pack_fee', JSON.stringify(pack_fee));

        formData.append('cuisines', selected_cuisines.join(','));
        formData.append('delivery', delivery);
        formData.append('share_percentage', share_percentage);
        formData.append('charges', charges);
        formData.append('status', status);

        if(image !== undefined) formData.append('image', image);
        if(banner !== undefined) formData.append('banner', banner);
        if(mou !== undefined) formData.append('mou', mou);
        if(logo !== undefined) formData.append('logo', logo);
        SubmitEdit(formData, Id);
    }


    const SubmitEdit = async (formData, Id) => {
        props.setLoader(true);
        try {
            const response = await Axios.put(`/api/backend/restaurant/${Id}`, formData);
            setData(data.map(d => d._id === Id ? response.data : d));

            document.getElementById("close-modal").click();
            document.getElementById("form").reset();
            props.setToastr('Success!','Data successfully modified','success')
        } catch (error) {
            props.setToastr('Error!','An error occurred, please retry','error')
        } finally {
            props.setLoader(false);
        }
    }


    const handleCityChange = cityId => {
        //setCityValue(cityId)
        fetchLocation(cityId);
        console.log(cityId, 'city change')
    }

    const deleteData = async (Id) => {
        const conf = window.confirm('Do you want to delete selected value?');
        if(!conf)
           return false;

        props.setLoader(true);
        const response = await Axios.delete(`/api/backend/restaurant/${Id}`);
        setData(data.filter(data => data._id !== Id));
        props.setLoader(false);
    };

    const handleOpenEdit = (row) =>{
        setModalTitle('Edit Vendor')
        setEditMode(true);
        setEditData(row);
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    const handleOpenArrangeMenu = (id) =>{
        setSelectedVendorId(id);
        fetchMealByVendor(id);
    }

    const handleSubmitArrangeMenu = async (menuList) =>{
        const data = { menuList, selectedVendorId}
        await Axios.post(`/api/backend/meal/arrangemenu`, data)
        .then(response =>{
            console.log(response.data, 'response...')
            //setMeals(response.data);
        })
        .catch(err => {
            console.log(err, 'handleSubmitArrangeMenu')
        }).then(() => {
            
        })
    }

    const objectKey = (obj) => {
        return Object.keys(obj);
    }

    const filterMeals = () => {
        return meals.data.reduce((prev, now) => {
            if (!prev[now.category._id]) {
                prev[now.category._id] = [];
            }

            prev[now.category._id].push(now);
            return prev;
        }, {});
    }

    const clearArrangeMenu = () =>{
        setMeals({data:[], loading:false, error:false, rd:false});
    }

    const handleSearch = (query) => {
        setQuery(query);
        props.history.push(`/${path}` + query);
    }

    const handleSearchReset = () => {
        setQuery("");
        props.history.push(`/${path}`);
    }

    useEffect(() => {
        fetchCuisines();
        fetchCity();
        fetchPackCategory();
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
    }, [])

    useEffect(() => {
        fetchData();
    }, [query])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            pageContentCss={`page-content-table`}

            actions={<Actions 
                title={title} 
                setModalTitle={setModalTitle} 
                handleSearch={handleSearch} 
                handleSearchReset={handleSearchReset}
                city={city}
                handleCityChange={handleCityChange}
                locations={location}
                />}
        >


{actionLoader.loading && (<div className="_loader_block__ vertical-align text-center">
<div className="loader vertical-align-middle loader-cube-grid"></div>
</div>)}

            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (<Fragment>
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                
                                <th className="cell-300" scope="col">Name</th>
                                <th className="cell-300" scope="col">Location</th>
                                <th scope="col">City</th>
                                <th scope="col">status</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        {data.map((row, index) => {

                                return (
                                    <VensorsTableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                        handleOpenArrangeMenu={handleOpenArrangeMenu}
                                    />)
                            })}
                        </tbody>
                    </table>

                    {dataMeta.total > dataMeta.limit && (
                        <Pagination
                        activePage={dataMeta.current_page}
                        itemsCountPerPage={dataMeta.limit}
                        totalItemsCount={dataMeta.total}
                        pageRangeDisplayed={5}
                        onChange={paginate}
                        linkClass="page-link"
                        itemClass="page-item"
                        />
                    )}

                    
            </Fragment>)}


            <div className="modal fade" id="modalForm" aria-hidden="true" 
            aria-labelledby="modalForm"  role="dialog" data-backdrop="static" data-keyboard="false">
            <div className="modal-dialog modal-simple modal-lg">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    {editMode?(
                    <Edit
                        handleSubmitEdit={handleSubmitEdit}
                        editData={editData}
                        cuisines={cuisines}
                        cuisinesLoading={cuisinesLoading}
                        cuisinesError={cuisinesError}
                        city={city}
                        handleCityChange={handleCityChange}
                        location={location}
                        clearFormData={clearFormData}
                        packCategory={packCategory}
                    />
                    ):(
                    <Add
                    handleSubmitAdd={handleSubmitAdd}
                    cuisines={cuisines}
                    cuisinesLoading={cuisinesLoading}
                    cuisinesError={cuisinesError}
                    city={city}
                    handleCityChange={handleCityChange}
                    location={location}
                    clearFormData={clearFormData}
                    packCategory={packCategory}
                    />
                    )}
                    


                </div>
            </div>
        </div>
       
        

        <div className="modal fade" id="modalArrangeMenu" aria-hidden="true" 
            aria-labelledby="modalArrangeMenu"  role="dialog" data-backdrop="static" data-keyboard="false">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" 
                        onClick={clearArrangeMenu}
                        aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">Arrange Menu</h4>
                    </div>
                    
                    <ArrangeMenu 
                       meals={meals}
                       mealCat={objectKey(filterMeals())}
                       handleSubmitArrangeMenu={handleSubmitArrangeMenu}
                       clearArrangeMenu={clearArrangeMenu}
                       />

                </div>
            </div>
        </div>
       
       


        </MasterInner>
    );
}



export default withContext(Vendor);