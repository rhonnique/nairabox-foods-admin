import React, { useState, useEffect, Fragment, useRef } from "react";
import {
    FormWithConstraints, FieldFeedbacks, FieldFeedback,
    Async as Async_, AsyncProps
} from 'react-form-with-constraints';
var slug = require('slug');

const VendorFormAdd = props => {
    //console.log(props.editData, 'props.editData')
    const form = useRef(null);
    const [editData, setEditData] = useState(props.editData);
    const [selectCuisines, setSelectCuisines] = useState(props.editData.cuisines.map((c) => c.cuisine._id));
    const [selectedPackCat, setSelectedPackCat] = useState(props.editData.pack_fee.map(mp => ({
        _id: mp.category._id,
        amount: mp.amount,
        name: mp.category.name
    })));
    const [spValue, setSpValue] = useState(props.editData.pack_fee.length > 0 ? true : "");

    const handleChange = (e) => {
        form.current.validateFields(e.target);
        const { name, value } = e.target
        setEditData({ ...editData, [name]: value })
    }

    const handleChangeContactName = (e) => {
        form.current.validateFields(e.target);
        const { value } = e.target
        var contact = { ...editData.contact }
        contact.name = value;
        setEditData({ ...editData, contact })
    }

    const handleChangeContactPhone = (e) => {
        form.current.validateFields(e.target);
        const { value } = e.target
        var contact = { ...editData.contact }
        contact.phone = value;
        setEditData({ ...editData, contact })
    }

    const handleChangeAccountBank = (e) => {
        form.current.validateFields(e.target);
        const { value } = e.target
        var account = { ...editData.account }
        account.bank = value;
        setEditData({ ...editData, account })
    }

    const handleChangeAccountName = (e) => {
        form.current.validateFields(e.target);
        const { value } = e.target
        var account = { ...editData.account }
        account.name = value;
        setEditData({ ...editData, account })
    }

    const handleChangeAccountNumber = (e) => {
        form.current.validateFields(e.target);
        const { value } = e.target
        var account = { ...editData.account }
        account.number = value;
        setEditData({ ...editData, account })
    }



    const handleSubmit = async (e) => {
        e.preventDefault();
        let target = e.target.elements;

        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitEdit(target, editData._id, selectedPackCat, document.getElementsByName('selected_cuisines'));
        }
    }


    const removePack = Id => {
        setSelectedPackCat(selectedPackCat.filter(c => c._id !== Id));
    }

    const addContainer = (e) => {
        const { value } = e.target;
        if (value === "")
            return false;

        const _id = value;
        const index = e.nativeEvent.target.selectedIndex;
        const name = e.nativeEvent.target[index].text;
        const amount = "";

        let check = selectedPackCat.some(c => c._id === _id)
        if (!check) {
            const newCat = [...selectedPackCat, { _id, name, amount }];
            setSelectedPackCat(newCat)
            setSpValue("true");
        }
    }

    const updateSelectedPack = (e, Id) => {
        const { value } = e.target;
        var current = selectedPackCat.find(e => e._id === Id);
        current.amount = value;
        setSelectedPackCat(selectedPackCat.map(c => c._id === Id ? current : c));
    }

    useEffect(() => {
        props.handleCityChange(editData.city._id);
    }, [])

    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} value={editData.name}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Address</label>
                        <textarea rows="3" name="address" placeholder="Address"
                            onChange={handleChange} required value={editData.address}
                            className="form-control"></textarea>
                        <FieldFeedbacks for="address">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>City</label>
                                <select name="city"
                                    value={editData.city._id}
                                    onChange={(e) => {
                                        props.handleCityChange(e.target.value)
                                        handleChange(e)
                                    }} className="form-control">
                                    <option value="" >- Select City -</option>
                                    {props.city.map((city, index) => {
                                        return (<option key={index} value={city._id}>{city.name}</option>)
                                    })}
                                </select>
                                <FieldFeedbacks for="city">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Location</label>
                                <select name='location' required
                                    value={editData.location._id}
                                    onChange={handleChange}
                                    className="form-control">
                                    <option value="" >- Select Location -</option>
                                    {props.location.map((location, index) => {
                                        return (<option key={index} value={location._id}>{location.name}</option>)
                                    })}
                                </select>
                                <FieldFeedbacks for="location">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Placeholder Image</label>
                                <input type="file" className="form-control" name="file_image"
                                    onChange={handleChange} />
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Banner</label>
                                <input type="file" className="form-control" name="file_banner"
                                    onChange={handleChange} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Weekdays Opening Time</label>
                                <input type="text" required className="form-control" onChange={handleChange}
                                    value={editData.openings.weekdays} required
                                    name="weekdays_opening" placeholder="Weekdays Opening Time" />
                                <FieldFeedbacks for="weekdays_opening">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Weekend Opening Time</label>
                                <input type="text" required className="form-control" onChange={handleChange}
                                    value={editData.openings.weekends}
                                    name="weekends_opening" placeholder="Weekend Opening Time" />
                                <FieldFeedbacks for="weekends_opening">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Contact Person</label>
                                <input type="text" className="form-control"
                                    onChange={handleChangeContactName}
                                    name="contact_name" placeholder="Contact Person"
                                    value={editData.contact.name} required />
                                <FieldFeedbacks for="contact_name">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Contact Phone</label>
                                <input type="text" className="form-control" onChange={handleChangeContactPhone}
                                    name="contact_phone" placeholder="Contact Phone"
                                    value={editData.contact.phone} required />
                                <FieldFeedbacks for="contact_phone">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Delivery Time</label>
                                <input type="text" className="form-control" onChange={handleChange}
                                    value={editData.delivery}
                                    name="delivery" placeholder="Delivery Time" required />
                                <FieldFeedbacks for="delivery">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Status</label>
                                <select name="status" onChange={handleChange} value={editData.status}
                                    className="form-control" required>
                                    <option value="">- Select -</option>
                                    <option value={true}>Active</option>
                                    <option value={false}>Inactive</option>
                                </select>
                                <FieldFeedbacks for="status">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <div className="form-group">
                        <label>Cuisine</label>
                        {props.cuisinesLoading && (<div>..loading cuisines</div>)}
                        {props.cuisinesError && (<div>Can not load cuisines</div>)}
                        {props.cuisines && (
                            <div className="row">
                                {props.cuisines.map((cuisine, index) => {
                                    return (<div key={index} className="col-sm-4">
                                        <div className="checkbox-custom checkbox-primary checkbox-inline">
                                            <input name="selected_cuisines"
                                                defaultChecked={selectCuisines.includes(cuisine._id)}
                                                value={cuisine._id} type="checkbox" id={'cuisine_' + cuisine._id} />
                                            <label htmlFor={'cuisine_' + cuisine._id}>{cuisine.name}</label>
                                        </div>
                                    </div>)
                                })}

                            </div>
                        )}
                    </div>

                    <hr />

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>MOU</label>
                                <input type="file" className="form-control" name="file_mou"
                                    onChange={handleChange} />
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Logo</label>
                                <input type="file" className="form-control" name="file_logo"
                                    onChange={handleChange} />
                            </div>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-sm-4">
                            <div className="form-group">
                                <label>Bank Name</label>
                                <input type="text" className="form-control"
                                    onChange={handleChangeAccountBank}
                                    value={editData.account.bank}
                                    name="account_bank" placeholder="Bank Name"
                                />
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="form-group">
                                <label>Account Name</label>
                                <input type="text" className="form-control"
                                    onChange={handleChangeAccountName}
                                    value={editData.account.name}
                                    name="account_name" placeholder="Account Name"
                                />
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="form-group">
                                <label>Account Number</label>
                                <input type="text" className="form-control"
                                    onChange={handleChangeAccountNumber}
                                    value={editData.account.number}
                                    name="account_number" placeholder="Account Number"
                                />
                            </div>
                        </div>
                    </div>



                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Vendor Percentage</label>
                                <div className="input-group">
                                    <input type="text" className="form-control"
                                        onChange={handleChange} value={editData.share_percentage}
                                        name="share_percentage" placeholder="Charges Rate"
                                    />
                                    <span className="input-group-btn">
                                        <button type="button" className="btn btn-default btn-outline">%</button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Charge Consumption Fee</label>
                                <select name="charges" onChange={handleChange} value={editData.charges}
                                    className="form-control" required>
                                    <option value="">- Select -</option>
                                    <option value={true}>Yes</option>
                                    <option value={false}>No</option>
                                </select>
                                <FieldFeedbacks for="charges">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <br />





                    <div>

                        <div className="form-group">
                            <label>Pack Fee</label>
                            <select name="pack_category" className="form-control"
                                value={spValue}
                                onChange={addContainer} required>
                                <option value="" >- Add Pack Category -</option>
                                {selectedPackCat.length > 0 && (
                                    <option value={true} >
                                        {`${selectedPackCat.length} pack category added`}
                                    </option>
                                )}

                                {props.packCategory && props.packCategory.map((pack, index) => {
                                    return (<option key={index} value={pack._id}>{pack.name}</option>)
                                })}
                            </select>
                            <FieldFeedbacks for="pack_category">
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>

                        {selectedPackCat.length > 0 && selectedPackCat.map((pack, index) => {
                            const form_name = slug(pack.name, { lower: true, replacement: '_' });
                            return (<div key={index} className="form-group option-list">
                                <label>{pack.name}</label>
                                <i className="wb-close-mini" onClick={() => removePack(pack._id)} aria-hidden="true"></i>
                                <input type="number" className="form-control form-control-sm" min="1"
                                    value={pack.amount}
                                    onChange={(e) => updateSelectedPack(e, pack._id)}
                                    name={form_name} placeholder={`${pack.name} Amount`} required />
                                <FieldFeedbacks for={form_name}>
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>)
                        })}
                    </div>









                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal" data-dismiss="modal"
                        onClick={props.clearFormData}
                        href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default VendorFormAdd;