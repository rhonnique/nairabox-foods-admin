import React, { Fragment } from "react";

const VensorsTableRow = ({row, handleOpenEdit, handleDeletedata, handleOpenArrangeMenu}) => {
    
    return (
        <tr>
            
            <td className="cell-300">{row.name}</td>
            <td className="cell-300">{row.location.name}</td>
            <td>{row.city.name}</td>
            <td>
                {row.status?(
                    <span className="badge badge-outline badge-success">Active</span>
                ):(
                    <span className="badge badge-outline badge-default">Inactive</span>
                )}
            
            </td>
            <td className="cell-30">
                <div className="btn-group dropdown">
                    <button type="button" data-toggle="dropdown" aria-expanded="false"
                        className="btn btn-pure btn-default icon wb-more-vertical"></button>
                    <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">
                        <a className="dropdown-item" onClick={() => handleOpenEdit(row)} 
                        href="javascript:void(0)" role="menuitem"
                        data-toggle="modal" data-target="#modalForm">Edit</a>

                        <a className="dropdown-item" onClick={() => handleOpenArrangeMenu(row._id)} 
                        href="javascript:void(0)" role="menuitem"
                        data-toggle="modal" data-target="#modalArrangeMenu">Arrange Menu</a>

                        <a onClick={() => handleDeletedata(row._id)} 
                        className="dropdown-item" href="javascript:void(0)" role="menuitem">Delete</a>
                    </div>
                </div>
            </td>
        </tr>

    );
}

export default VensorsTableRow;