import React, { useState, useRef, Fragment, useEffect } from "react";
import { SortableContainer, SortableElement, sortableHandle } from 'react-sortable-hoc';
import arrayMove from 'array-move';

const DragHandle = sortableHandle(() => <i className="icon wb-menu" aria-hidden="true"></i>);

const ArrangeMenu = ({ meals, mealCat, handleSubmitArrangeMenu, clearArrangeMenu }) => {

    const [sortMenuList, setSortMenuList] = useState(mealCat);

    useEffect(() => {
        setSortMenuList(mealCat);
    }, [mealCat]);

    useEffect(() => {
        return () => {
            console.log('clear data...')
            clearArrangeMenu();
        }
    }, []);

    const SortableItem = SortableElement(({ value }) => {
        const category = meals.data.length  && meals.data.filter(mp=>mp.category._id === value)[0].category.name;
        return (<li><DragHandle />{category}</li>)
    });

    const SortableList = SortableContainer(({ items }) => {
        return (
            <ul className="sort-list">
                {items.map((value, index) => (
                    <SortableItem key={`item-${index}`} index={index} value={value} />
                ))}
            </ul>
        );
    });

    const onSortEnd = ({ oldIndex, newIndex }) => {
        const newSort = arrayMove(sortMenuList, oldIndex, newIndex);
        setSortMenuList(newSort);
    };

        return (
            <Fragment>
                <div className="modal-body">
                {meals.loading === true && (<div>loading menu, please wait...</div>)}
                {meals.error === true && (<div>An error occurred, please retry!</div>)}
                {meals.data.length > 0 && (<SortableList items={sortMenuList} onSortEnd={onSortEnd} />)}
                {meals.rd && meals.data.length === 0 && (<div>No menu added yet!</div>)}
                </div>
                {meals.rd && meals.data.length > 0 && (
                <div className="modal-footer">
                    <button 
                    onClick={()=> handleSubmitArrangeMenu(sortMenuList)}
                    className="btn btn-primary" type="button">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal" 
                    onClick={()=> clearArrangeMenu()}
                    data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>)}
            </Fragment>
        );
    }

    export default ArrangeMenu;