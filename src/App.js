import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import ProtectedRoute from "./Auth/ProtectedRoute";

import Master from './layout/Master';

import Login from "./Login/Login";
import Dashboard from "./Dashboard/Dashboard";
import Vendors from './Vendor/Home';
import PackCategory from './PackCategory/Home';
import MealCategory from './MealCategory/Home';
import Choices from './Choices/Home';
import Meals from './Meals/Home';
import CorporateDrink from './CorporateDrink/Home';
import CorporateMeal from './CorporateMeal/Home';
import Customers from './Customers/Home';
import Orders from './Orders/Home';
import Location from './Location/Home';
import Admin from './Admin/Home';
import Promotions from './Promo/Home';
import Settings from './Settings/Home';

function App() { 

    return (
        <Master>
            <Switch>
                <Route path="/login" component={Login} />
                <ProtectedRoute exact path="/" component={Dashboard} />
                <ProtectedRoute path="/administrators" component={Admin} />
                <ProtectedRoute path="/vendors" component={Vendors} />
                <ProtectedRoute path="/pack-category" component={PackCategory} />
                <ProtectedRoute path="/meal-category" component={MealCategory} />
                <ProtectedRoute path="/choices" component={Choices} />
                <ProtectedRoute path="/meals" component={Meals} />
                <ProtectedRoute path="/customers" component={Customers} />
                <ProtectedRoute path="/orders" component={Orders} />
                <ProtectedRoute path="/corporate-drinks" component={CorporateDrink} />
                <ProtectedRoute path="/corporate-meals" component={CorporateMeal} />
                <ProtectedRoute path="/locations" component={Location} />
                <ProtectedRoute path="/promotions" component={Promotions} />
                <ProtectedRoute path="/settings" component={Settings} />
            </Switch>
        </Master>

    )
}

export default App;
