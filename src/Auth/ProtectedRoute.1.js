import React, {useEffect, useState, Fragment} from "react"
import { Route, Redirect } from "react-router-dom";
import { withContext } from "../AppContext"

const ProtectedRoute = props => {
    const { component: Component, ...rest } = props;
    const [userData, setUserData] = useState({})
    const [userDataLoader, setUserDataLoader] = useState({})

    const fetchCustomer = () => {
        setUserDataLoader(dt => ({ ...dt, loading:true, error:false, rd: false }))
        props.customerGet()
        .then((response) => {
            console.log(response.data, 'fetchCustomer...')
            setUserData(response.data);
            setUserDataLoader(dt => ({ ...dt, loading:false, error:false, rd: true }))
        }).catch(error =>{
            console.log(error, 'fetchCustomer...')
            setUserDataLoader(dt => ({ ...dt, loading:false, error:false, rd: true }))
        })
    }

    useEffect(() => {
        fetchCustomer();
    },[props.location.pathname])

    return (
    
    {/* <Fragment>
        {!userDataLoader.rd &&  (<Route render={()=><span>loading.....</span>} />)}
        {userDataLoader.rd &&  (<Route {...rest} component={Component} />)}

    </Fragment> */}
        
    )

    
    /* 

    userDataLoader.rd &&  (<Route render={()=><span>loading.....</span>} />)
    
    return (
        {userDataLoader.rd && (<Route {...rest} component={Component} />)}
        props.token ?
            <Route {...rest} component={Component} /> :
            <Redirect to="/login" />
    ) */
}

export default withContext(ProtectedRoute);