import React, { Component } from 'react';
import { withContext } from "../AppContext"
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
//import { DisplayFields } from 'react-form-with-constraints-tools';
import $ from 'jquery';

class LoginForm extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      errorMessage: ""
    }
  }

  componentWillMount() {
    {
      (function (document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function () {
          Site.run();
        });
      })(document, window, $)
    }
    /* var linkElement = document.createElement('link');
    linkElement.setAttribute('rel', 'stylesheet');
    linkElement.setAttribute('type', 'text/css');
    linkElement.setAttribute('href', '/assets/css/site.min.css'); */

    var style = document.createElement('link')
    style.rel = 'stylesheet'
    style.type = 'text/css'
    style.href = '/assets/examples/css/pages/login-v3.css'
    document.head.appendChild(style)

    document.body.classList.add('layout-full');
    document.body.classList.add('page-login-v3');
  }

  handleChange = (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.login(this.state)
      .then(() => {
        //document.html.classList.add('js-menubar');
        //document.body.classList.add('site-menubar-unfold');
        //document.body.classList.add('layout-full');
        this.props.history.push("/todos")
        //window.location = '/todos';
        //this.props.setState({isLoggedIn: true})
      })
      .catch(err => {
        this.setState({ errorMessage: err.response.data.message })
      })
  }

  clearInputs = () => {
    this.setState({
      username: "",
      password: "",
      errorMessage: ""
    })
  }

  render() {
    return (

      <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
        <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
          <div class="panel">
            <div class="panel-body">
              <div class="brand">
                <img class="brand-img" src="/assets/images/logo-colored.png" alt="..." />
                <h2 class="brand-text font-size-18">Remark</h2>
              </div>
              <form onSubmit={this.handleSubmit}>
                {
                  this.state.errorMessage &&
                  <p style={{ color: "red" }}>{this.state.errorMessage}</p>
                }
                <div class="form-group">
                  <label>Email</label>
                  <input
                    onChange={this.handleChange}
                    value={this.state.username}
                    name="username"
                    type="text"
                    class="form-control"
                    placeholder="Email Address" />
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input
                    onChange={this.handleChange}
                    value={this.state.password}
                    name="password"
                    type="password"
                    class="form-control"
                    placeholder="password" />
                </div>
                <div class="form-group clearfix">
                  <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                    <input type="checkbox" id="inputCheckbox" name="remember" />
                    <label for="inputCheckbox">Remember me</label>
                  </div>
                  <a class="float-right" href="forgot-password.html">Forgot password?</a>
                </div>
                <button class="btn btn-primary btn-block btn-lg mt-40" type="submit">Sign in</button>
              </form>
              <p>Still no account? Please go to <a href="register-v3.html">Sign up</a></p>
            </div>
          </div>

          <footer class="page-copyright page-copyright-inverse">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
              <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                <i class="icon bd-twitter" aria-hidden="true"></i>
              </a>
              <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                <i class="icon bd-facebook" aria-hidden="true"></i>
              </a>
              <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                <i class="icon bd-google-plus" aria-hidden="true"></i>
              </a>
            </div>
          </footer>
        </div>
      </div>

    )
  }
}

export default withContext(LoginForm);