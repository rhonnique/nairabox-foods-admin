import React, { Fragment, useEffect, useState, useRef } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import $ from 'jquery'
import FeaturedRestaurantAdd from "./../FeaturedRestaurant/Add"
import FeaturedRestaurantHome from "./../FeaturedRestaurant/Home"

import FeaturedMealAdd from "./../FeaturedMeal/Add"
import FeaturedMealHome from "./../FeaturedMeal/Home"

import FeaturedAdHome from "./../FeaturedAd/Home"
import FeaturedAdAdd from "./../FeaturedAd/Add"
import FeaturedAdEdit from "./../FeaturedAd/Edit"

const Axios = axios.create();

const Dashboard = props => {
    const [recentOrders, setRecentOrders] = useState([]);
    const [recentOrdersLoader, setRecentOrdersLoader] = useState({});
    const [featuredRestaurant, setFeaturedRestaurant] = useState({});
    const [featuredMeal, setFeaturedMeal] = useState({});
    const [featuredAd, setFeaturedAd] = useState({});

    const [orderTotal, setOrderTotal] = useState('loading..');

    const fetchFeaturedRestaurant = () => {
        setFeaturedRestaurant(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.featuredRestaurantGet()
            .then((response) => {
                setFeaturedRestaurant(dt => ({ ...dt, data: response.data, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setFeaturedRestaurant(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    }

    const handleSubmitFeaturedRestaurant = (restaurant, status) => {
        const data = { restaurant, status }
        console.log(data, 'handleSubmitFeaturedRestaurant...')
        props.setLoader(true);
        props.featuredRestaurantAdd(data)
            .then((response) => {
                console.log(response, 'handleSubmitFeaturedRestaurant..')
                fetchFeaturedRestaurant();
                document.getElementById("close-modal-restaurant").click();
                document.getElementById("form-featured-restaurant").reset();
            })
            .catch(err => {
                props.errorHandler(err, 'Error!');
            }).then(() => props.setLoader(false))
    }

    const fetchFeaturedMeal = () => {
        setFeaturedMeal(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.featuredMealGet()
            .then((response) => {
                setFeaturedMeal(dt => ({ ...dt, data: response.data, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setFeaturedMeal(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    }

    const handleSubmitFeaturedMeal = (meal, status) => {
        const data = { meal, status }
        props.setLoader(true);
        props.featuredMealAdd(data)
            .then((response) => {
                fetchFeaturedMeal();
                document.getElementById("close-modal-meal").click();
                //document.getElementById("form-featured-meal").reset();
            })
            .catch(err => {
                props.errorHandler(err, 'Error!');
            }).then(() => props.setLoader(false))
    }

    const fetchFeaturedAd = () => {
        setFeaturedAd(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.featuredAdGet()
            .then((response) => {
                //console.log(response.data, 'fetchFeaturedAd...')
                setFeaturedAd(dt => ({ ...dt, data: response.data, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setFeaturedAd(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    }

    const handleSubmitFeaturedAd = (left_image, righttop_image, rightbottom_image, value) => {
        let formData = new FormData();
        if(left_image !== undefined) formData.append('left_image', left_image);
        if(righttop_image !== undefined) formData.append('righttop_image', righttop_image);
        if(rightbottom_image !== undefined) formData.append('rightbottom_image', rightbottom_image);

        formData.append('left_link', value.left_link);
        formData.append('righttop_link', value.righttop_link);
        formData.append('rightbottom_link', value.rightbottom_link);
        formData.append('status', value.status);   

        props.setLoader(true);
        props.featuredAdAdd(formData)
            .then((response) => {
                document.getElementById("close-modal-ad").click();
                setTimeout(() => {
                    fetchFeaturedAd();
                }, 500);     
            })
            .catch(err => {
                props.errorHandler(err, 'Error!');
            }).then(() => props.setLoader(false))
    }

    const order_total = () => {
        setOrderTotal('loading..');
        props.rpt_total_order()
            .then((response) => {
                setOrderTotal(response.data);
            })
            .catch(err => {
                setOrderTotal('N/A');
            });
    }

    useEffect(() => {
        (function(document, window, $){
            'use strict';
            var Site = window.Site;
            $(document).ready(function(){
              Site.run();
            });
          })(document, window, $);
          
        fetchFeaturedRestaurant();
        fetchFeaturedMeal();
        fetchFeaturedAd();
        //setMyChartRef(chartRef.current.getContext("2d"))
        //fetchRecentOrder(5);

        order_total();
    }, [])

    return (
        <MasterInner noBgWhite={true}>

            <div className="row">

                <div className="col-xl-6 col-md-6 info-panel">
                    <div className="card card-shadow">
                        <div className="card-block bg-white p-20">
                            <button type="button" className="btn btn-floating btn-xs btn-warning">
                                <i className="icon wb-shopping-cart"></i>
                            </button>
                            <span className="ml-15 font-weight-400">TODAY'S ORDER</span>
                            <div className="content-text mt-10">
                                <span className="font-size-20 font-weight-100">{orderTotal}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-6 col-md-6 info-panel">
                    <div className="card card-shadow">
                        <div className="card-block bg-white p-20">
                            <button type="button" className="btn btn-floating btn-xs btn-danger">
                                <i className="icon fa-dollar"></i>
                            </button>
                            <span className="ml-15 font-weight-400">TOTAL INCOME</span>
                            <div className="content-text  mt-10">
                                <span className="font-size-20 font-weight-100">19,800,628.40</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-6 col-md-6 info-panel">
                    <div className="card card-shadow">
                        <div className="card-block bg-white p-20">
                            <button type="button" className="btn btn-floating btn-xs btn-success">
                                <i className="icon wb-eye"></i>
                            </button>
                            <span className="ml-15 font-weight-400">ORDERS COMPLETED</span>
                            <div className="content-text  mt-10">
                                <span className="font-size-20 font-weight-100">23,456</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-6 col-md-6 info-panel">
                    <div className="card card-shadow">
                        <div className="card-block bg-white p-20">
                            <button type="button" className="btn btn-floating btn-xs btn-primary">
                                <i className="icon wb-user"></i>
                            </button>
                            <span className="ml-15 font-weight-400">ORDERS PENDING</span>
                            <div className="content-text mt-10">
                                <span className="font-size-20 font-weight-100">4,367</span>
                            </div>
                        </div>
                    </div>
                </div>



            </div>



{props.user && props.user.auth === 'ADMIN' && (<div>

            <div className="row" data-plugin="matchHeight" data-by-row="true">

            <div className="col-lg-12" id="ecommerceRecentOrder">
                    <div className="card card-shadow table-row">

                        <h3 className="card-header card-header-transparent blue-grey-700 font-size-14 mt-10 pb-0">
                            RECENT ORDERS
                    </h3>

                        <div className="card-block bg-white table-responsive">
                            {recentOrdersLoader.loading && (<div>Loading ...</div>)}
                            {recentOrdersLoader.error && (<div>an error occured</div>)}
                            {recentOrdersLoader.rd && recentOrders.length === 0 && (<div>No result found!</div>)}

                            {recentOrdersLoader.rd && recentOrders.length > 0 && (<table className="table">
                                <thead>
                                    <tr>
                                        <th>Vendor</th>
                                        <th>Order ID</th>
                                        <th>Location</th>
                                        <th>Delivery</th>
                                        <th>Amount</th>
                                        <th className="text-center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {recentOrders.map((row, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{row.restaurant.name}</td>
                                                <td>#{row.order_id}</td>
                                                <td>Awolowo, Ikeja</td>
                                                <td>{row.delivery ? `Delivery` : `Pickup`}</td>
                                                <td>{row.total_amount}</td>
                                                <td className="text-center"><span className="badge badge-success font-weight-100">Paid</span></td>
                                            </tr>)
                                    })}



                                </tbody>
                            </table>)}

                        </div>
                    </div>
                </div>



                <div className="col-xxl-6 col-xl-6 col-lg-6">
                    <FeaturedRestaurantHome featuredRestaurant={featuredRestaurant} />
                </div>


                <div class="col-xxl-6 col-xl-6 col-lg-6">
                    <FeaturedMealHome featuredMeal={featuredMeal} />
                </div>

                <div class="col-xxl-6 col-xl-6 col-lg-6">
                    <FeaturedAdHome featuredAd={featuredAd} />
                </div>


            </div>


            {/* FEATURED RESTAURANT MODAL */}
            <div className="modal fade" id="modal-featured-restaurant" aria-hidden="true"
                aria-labelledby="modal-featured-restaurant"
                role="dialog" tabIndex="-1">
                <div className="modal-dialog modal-simple">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" aria-hidden="true" data-dismiss="modal">×</button>
                            <h4 className="modal-title">FEATURED RESTAURANTS</h4>
                        </div>
                        <FeaturedRestaurantAdd
                            featuredRestaurant={featuredRestaurant}
                            handleSubmitFeaturedRestaurant={handleSubmitFeaturedRestaurant}
                        />
                    </div>
                </div>
            </div>


            {/* FEATURED MEAL MODAL */}
            <div className="modal fade" id="modal-featured-meal" aria-hidden="true"
                aria-labelledby="modal-featured-meal"
                role="dialog" tabIndex="-1">
                <div className="modal-dialog modal-simple">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" aria-hidden="true" data-dismiss="modal">×</button>
                            <h4 className="modal-title">FEATURED MEAL</h4>
                        </div>
                        <FeaturedMealAdd
                            featuredMeal={featuredMeal}
                            handleSubmitFeaturedMeal={handleSubmitFeaturedMeal}
                        />
                    </div>
                </div>
            </div>


            {/* FEATURED AD MODAL */}
            <div className="modal fade" id="modal-featured-ad" aria-hidden="true"
                aria-labelledby="modal-featured-ad"
                role="dialog" tabIndex="-1">
                <div className="modal-dialog modal-simple">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" aria-hidden="true" data-dismiss="modal">×</button>
                            <h4 className="modal-title">FEATURED AD</h4>
                        </div>
                        {featuredAd.rd && featuredAd.data && (
                            <FeaturedAdEdit
                                featuredAd={featuredAd.data}
                                handleSubmitFeaturedAd={handleSubmitFeaturedAd}
                            />)}
                        {featuredAd.rd && featuredAd.data === null && (
                            <FeaturedAdAdd handleSubmitFeaturedAd={handleSubmitFeaturedAd} />)}

                    </div>
                </div>
            </div>



            </div>)}

        </MasterInner>
    );
}

export default withContext(Dashboard);