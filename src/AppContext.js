import React, { Component, useState } from "react";
import axios from "axios";
import * as Toastr from 'toastr';
import $ from 'jquery'
//import Geocode from "react-geocode";

//Geocode.setApiKey("AIzaSyCPwoO1Pmtl3fYgmbkC-DlVg11jpGj7ocU");
//Geocode.enableDebug();

const Axios = axios.create();

Toastr.options.timeout = 5000;
//Toastr.options.extendedTimeOut = 5000;
Toastr.options.closeButton = true;

//const AxiosOpen = axios.create();


/* axios.interceptors.response.use((config) => {
    const token = localStorage.getItem("token");
    config.headers.Authorization = `Bearer ${token}`;
    return config;
}, (error) => {
    console.log(error.response, 'ressss erorrrrrr');
}) */



const AppContext = React.createContext();

export class AppContextProvider extends Component {
    constructor(props) {
        super(props)

        const protocol = window.location.protocol;
        const host = window.location.host;
        const basename = props.children.props.basename;

        this.state = {
            host: protocol + '//' + host + basename,
            // api_url: `http://localhost:8080`,
            // image_url: `http://localhost:8080`,
            api_url: `http://food-server.nairabox.com:8080`,
            image_url: `http://food-server.nairabox.com:8080`,
            token: localStorage.getItem("token") || "",
            isLoggedIn: false,
            vat_rate: 5,
            consumption_rate: 5,
            show_loader: false,
            user: null,
            user_loading: true,
        }
    }

    componentWillMount() {
        axios.defaults.baseURL = this.state.api_url;
        Axios.interceptors.request.use((config) => {
            config.baseURL = this.state.api_url;
            const token = localStorage.getItem("token");
            if (token) {
                config.headers.Authorization = `Bearer ${token}`;
            }
            return config;
        })

        this.auth().then(response => {
            console.log(response.data, 'authz....')
            this.setState({ user: response.data })
            this.setState({ user_loading: false })
        }).catch(error => {
            this.setState({ user_loading: true })
            console.log(error.response, 'error::response....')
            //console.log(error.response.status, 'error::status....')
            //console.log(JSON.stringify(error), 'error::authz....')
            if (error.toString().includes('Error: Network Error')) {
                this.setToastr('Connection Error!', 'There seems to be a connectivity error, please check your internet and retry', 'error');
            } else {
                const { status } = error.response;
                switch (status) {
                    case 401:
                        this.logout();
                        break;
                }
            }

        }).then()

        //Axios.interceptors.response.use(null, this.handleAxiosError);

        //console.log('ressss componentWillMount...');

    }

    setLoader = (show_loader) => {
        this.setState({
            show_loader
        })
    }

    getMealByVendor = async (restuarantId) => {
        const response = await Axios.get(`/api/site/mealByRestaurant/${restuarantId}`);
        return response;
    };

    // `userInfo` will be an object with `username` and `password` fields
    signup = async (userInfo) => {
        const response = await Axios.post("/auth/signup", userInfo);
        const { user, token } = response.data;
        localStorage.setItem("token", token);
        localStorage.setItem("user", JSON.stringify(user));
        this.setState({
            user,
            token
        });
        return response;
    }

    login = async (credentials) => {
        const response = await Axios.post(`/api/backend/admin/login`, credentials);
        //document.querySelector('html').classList.add('js-menubar');
        //document.body.classList.add('site-menubar-unfold');


        //window.location = 'window.location.host'
        //this.setState({ user, token });

        //window.location.reload()
        return response;

    }

    setLogin = (data) => {
        const { token } = data;
        localStorage.setItem("token", token);
        //localStorage.setItem("user", JSON.stringify(user));
    }

    auth = async () => {
        const response = await Axios.get(`/api/backend/admin/auth`);
        return response;
    }

    logout = () => {
        //localStorage.removeItem("user");
        localStorage.removeItem("token");
        window.location = this.state.host + '/login';
        /* document.body.classList.remove('site-menubar-unfold');
        document.querySelector('html').classList.remove('js-menubar'); */

    }

    /*********
     *  ******* SETTINGS *******
     ****************************/

    settingsGet = async () => {
        const response = await Axios.get(`/api/backend/settings`);
        return response;
    }

    settingsEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/settings/${id}`, data);
        return response;
    }

    /*********
     *  ******* CITY/LOCATION *******
     ****************************/

    citySelect = async () => {
        const response = await axios.get(`/api/site/city`);
        return response;
    };


    locationSelect = async (city) => {
        const response = await axios.get(`/api/site/locationByCity/${city}`);
        return response;
    };


    /*********
    *  ******* LOCATION *******
    ****************************/

    locationGet = async () => {
        const response = await Axios.get("/api/backend/location");
        return response;
    }

    locationAdd = async (data) => {
        const response = await Axios.post("/api/backend/location", data);
        return response;
    }

    locationEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/location/${id}`, data);
        return response;
    }

    locationDelete = async (id) => {
        const response = await Axios.delete(`/api/backend/location/${id}`);
        return response;
    }

    getLongLats = async (address) => {
        //const {};
        //const response = await Geocode.fromAddress(address);
        return {};
    }


    /*********
     *  ******* PACK CATEGORY *******
     ****************************/

    packCategoryGet = async () => {
        const response = await Axios.get("/api/backend/packcategory");
        return response;
    }

    packCategoryAdd = async (data) => {
        const response = await Axios.post("/api/backend/packcategory", data);
        return response;
    }

    packCategoryEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/packcategory/${id}`, data);
        return response;
    }

    packCategoryDelete = async (id) => {
        const response = await Axios.delete(`/api/backend/packcategory/${id}`);
        return response;
    }


    /*********
     *  ******* VENDORS *******
     ****************************/

    vendorSelect = async () => {
        const response = await axios.get("/api/site/restaurant");
        return response;
    }

    vendorGet = async (query) => {
        const response = await Axios.get(`/api/backend/restaurant${query}`);
        return response;
    }

    vendorAdd = async (data) => {
        const response = await Axios.post("/api/backend/restaurant", data);
        return response;
    }

    vendorEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/restaurant/${id}`, data);
        return response;
    }

    vendorDelete = async (id) => {
        const response = await Axios.delete(`/api/backend/restaurant/${id}`);
        return response;
    }


    /*********
     *  ******* PROMOTIONS *******
     ****************************/

    promoSelect = async () => {
        const response = await axios.get("/api/site/promo");
        return response;
    }

    promoGet = async (query) => {
        const response = await Axios.get(`/api/backend/promo${query}`);
        return response;
    }

    promoAdd = async (data) => {
        const response = await Axios.post("/api/backend/promo", data);
        return response;
    }

    promoEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/promo/${id}`, data);
        return response;
    }

    promoDelete = async (id) => {
        const response = await Axios.delete(`/api/backend/promo/${id}`);
        return response;
    }


    /*********
     *  ******* MEALS *******
     ****************************/

    mealSelect = async (query) => {
        const response = await axios.get(`/api/site/meal`);
        return response;
    }

    mealGet = async (query) => {
        const response = await Axios.get(`/api/backend/meal${query}`);
        return response;
    }

    mealAdd = async (data) => {
        const response = await Axios.post("/api/backend/meal", data);
        return response;
    }

    mealEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/meal/${id}`, data);
        return response;
    }

    mealDelete = async (id) => {
        const response = await Axios.delete(`/api/backend/meal/${id}`);
        return response;
    }

    /*********
     *  ******* ORDERS *******
     ****************************/

    orderGet = async (query) => {
        //console.log(query, 'orderGet....')
        const response = await Axios.get(`/api/backend/order${query}`);
        return response;
    }

    orderList = async (order_id) => {
        const response = await Axios.get(`/api/backend/order/list/${order_id}`);
        return response;
    }

    orderStatusList = async (order_id) => {
        const response = await Axios.get(`/api/backend/order/status/${order_id}`);
        return response;
    }

    orderAddStatus = async (data, order_id) => {
        const response = await Axios.post(`/api/backend/order/createstatus/${order_id}`, data);
        return response;
    }


    /*********
     *  ******* FEATURED RESTAURANT *******
     ****************************/

    featuredRestaurantGet = async (query) => {
        const response = await Axios.get(`/api/backend/featuredrestaurant`);
        return response;
    }

    featuredRestaurantAdd = async (data) => {
        const response = await Axios.post("/api/backend/featuredrestaurant", data);
        return response;
    }

    /*********
     *  ******* FEATURED MEAL *******
     ****************************/

    featuredMealGet = async (query) => {
        const response = await Axios.get(`/api/backend/featuredmeal`);
        return response;
    }

    featuredMealAdd = async (data) => {
        const response = await Axios.post("/api/backend/featuredmeal", data);
        return response;
    }

    /*********
     *  ******* PERMISSIONS *******
     ****************************/

    permissionGet = async () => {
        const response = await Axios.get(`/api/backend/permission`);
        return response;
    }

    /*********
     *  ******* FEATURED AD *******
     ****************************/

    featuredAdGet = async (query) => {
        const response = await Axios.get(`/api/backend/featuredad`);
        return response;
    }

    featuredAdAdd = async (data) => {
        const response = await Axios.post("/api/backend/featuredad", data);
        return response;
    }

    /*********
     *  ******* ADMINS *******
     ****************************/

    adminGet = async () => {
        const response = await Axios.get(`/api/backend/admin`);
        return response;
    }

    adminAdd = async (data) => {
        const response = await Axios.post("/api/backend/admin", data);
        return response;
    }

    adminEdit = async (id, data) => {
        const response = await Axios.put(`/api/backend/admin/${id}`, data);
        return response;
    }

    adminDelete = async (id) => {
        const response = await Axios.delete(`/api/backend/admin/${id}`);
        return response;
    }

    /*********
     *  ******* REPORTING *******
     ********************************/

    rpt_total_order = async (status) => {
        status = status ? '?status=' + status : '';
        const response = await Axios.get(`/api/backend/order/report/totalorder${status}`);
        return response;
    }

    rpt_total_income = async () => {
        const response = await Axios.get(`/api/backend/order/report/totalorder`);
        return response;
    }






    setToastr = (title, msg, type = 'info') => {
        Toastr[type](msg, title);
    }


    /*********
     *  ******* ERROR HANDLING *******
     ****************************/

    handleAxiosError = (error) => {
        console.log(error.response, 'response..');

        if (error.response) {
            switch (error.response.status) {
                case 401:

                    //Toastr.error('Invalid email address or password.', title);
                    break;
                case 500:
                /* if (!exclude.includes(500)) {
                    if (typeof err.response.data.error.code !== 'undefined') {
                        console.log(err.response.data.error.code, 'errffff')
                        switch (err.response.data.error.code) {
                            case 11000:
                                Toastr.error('Customer email address already exist.', title);
                                break;
                        }
                    } else {
                        Toastr.error('An error occurred, please retry', title);
                    }
                } */

            }
        } else {
            Toastr.error('No connectivity, please check your internet and try again.',
                'Connectivity Error!');
        }
    }

    errorHandler = (error, title, exclude = []) => {
        console.log(error, 'error..'); //TypeError
        console.log(error.response, 'error::response..');
        //console.log(!error.response.data.message, 'error..');
        if (error.response) {
            switch (error.response.status) {
                case 401:
                    if (!exclude.includes(401)) {
                        Toastr.error('Invalid email address or password.', title);
                    }
                    break;
                case 400:
                    if (!exclude.includes(400)) {
                        if (typeof error.response.data.message !== 'undefined') {
                            Toastr.error(error.response.data.message, title);
                        } else {
                            Toastr.error('A validation error occurred, please check and retry', title);
                        }
                    }
                    break;
                case 500:
                    if (!exclude.includes(500)) {
                        if (typeof error.response.data.message !== 'undefined') {
                            Toastr.error(error.response.data.message, title);
                        } else {
                            Toastr.error('An error occurred, please retry', title);
                        }
                    }
                /* if (!exclude.includes(500)) {
                    if (typeof error.response.data.error.code !== 'undefined') {
                        console.log(error.response.data.error.code, 'errffff')
                        switch (error.response.data.error.code) {
                            case 11000:
                                Toastr.error('Customer email address already exist.', title);
                                break;
                        }
                    } else {
                        Toastr.error('An error occurred, please retry', title);
                    }
                } */

            }
        } else {
            Toastr.error('No connectivity, please check your internet and try again.',
                'Connectivity Error!');
        }
    }

    /*********
    *  ******* HANDLERS *******
    ****************************/

    selectHandler = (class_name, state) => {
        if (state) {
            $(class_name).attr('disabled', true);
            //$(class_name).append(new Option("loading...", "__loading_")).val('__loading_').attr('disabled', true);
        } else {
            $(class_name).attr('disabled', false);
            //$(class_name).attr('disabled', false).find("option[value='__loading_']").remove();
        }
    }

    render() {
        return (
            <AppContext.Provider
                value={{
                    setLoader: this.setLoader,

                    signup: this.signup,
                    login: this.login,
                    setLogin: this.setLogin,
                    logout: this.logout,

                    adminGet: this.adminGet,
                    adminAdd: this.adminAdd,
                    adminEdit: this.adminEdit,
                    adminDelete: this.adminDelete,

                    citySelect: this.citySelect,
                    locationSelect: this.locationSelect,

                    packCategoryGet: this.packCategoryGet,
                    packCategoryAdd: this.packCategoryAdd,
                    packCategoryEdit: this.packCategoryEdit,
                    packCategoryDelete: this.packCategoryDelete,

                    vendorSelect: this.vendorSelect,
                    vendorGet: this.vendorGet,
                    vendorAdd: this.vendorAdd,
                    vendorEdit: this.vendorEdit,
                    vendorDelete: this.vendorDelete,

                    mealSelect: this.mealSelect,
                    mealGet: this.mealGet,
                    mealAdd: this.mealAdd,
                    mealEdit: this.mealEdit,
                    mealDelete: this.mealDelete,

                    orderGet: this.orderGet,
                    orderList: this.orderList,
                    orderAddStatus: this.orderAddStatus,
                    orderStatusList: this.orderStatusList,

                    getMealByVendor: this.getMealByVendor,

                    featuredRestaurantGet: this.featuredRestaurantGet,
                    featuredRestaurantAdd: this.featuredRestaurantAdd,

                    featuredMealGet: this.featuredMealGet,
                    featuredMealAdd: this.featuredMealAdd,
                    featuredAdGet: this.featuredAdGet,
                    featuredAdAdd: this.featuredAdAdd,

                    promoSelect: this.promoSelect,
                    promoGet: this.promoGet,
                    promoAdd: this.promoAdd,
                    promoEdit: this.promoEdit,
                    promoDelete: this.promoDelete,

                    permissionGet: this.permissionGet,

                    locationGet: this.locationGet,
                    locationAdd: this.locationAdd,
                    locationEdit: this.locationEdit,
                    locationDelete: this.locationDelete,

                    settingsGet: this.settingsGet,
                    settingsEdit: this.settingsEdit,

                    getLongLats: this.getLongLats,

                    selectHandler: this.selectHandler,
                    errorHandler: this.errorHandler,

                    setToastr: this.setToastr,

                    rpt_total_order: this.rpt_total_order,

                    ...this.state
                }}
            >
                {this.state.show_loader && (<div className="_loader_block__ vertical-align text-center">
                    <div className="loader vertical-align-middle loader-cube-grid"></div>
                </div>)}


                {this.props.children}

            </AppContext.Provider>
        )
    }
}

export const withContext = Component => {
    return props => {
        return (
            <AppContext.Consumer>
                {
                    globalState => {
                        return (
                            <Component
                                {...globalState}
                                {...props}
                            />
                        )
                    }
                }
            </AppContext.Consumer>
        )
    }
}
