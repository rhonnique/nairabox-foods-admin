import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import axios from "axios";
import TableRow from "./TableRow";
import * as library from './../Components/libraries'
import Add from "./Add";
import Edit from "./Edit";
import $ from 'jquery'

var pluralize = require('pluralize');

const axiosCreate = axios.create();

const Home = props => {
    const { api_url } = props;
    const title = `Pack Category`;
    const api_path = `packcategory`;
    const permission = `CATALOGS`;
    
    axiosCreate.interceptors.request.use((config) => {
        const token = localStorage.getItem("token");
        config.headers.Authorization = `Bearer ${token}`;
        config.baseURL = api_url;
        return config;
    })

    const [data, setData] = useState([]);
    const [dataLoader, setDataLoader] = useState(false);

    const [actionLoader, setActionLoader] = useState({});
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [actionLoading, setActionLoading] = useState(false);
    const [actionError, setActionError] = useState(false);

    const [toast, setToast] = useState({
        click: false,
        title:'', 
        type:'',
        msg:'',
    });
    const [toasType, setToastType] = useState('');

    const Actions = () => {
        return (
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)" 
            data-toggle="modal" data-target="#modalForm" 
                onClick={() => setModalTitle(`Add ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> Add {title}
        </a>
        )
    }

    // Get main data
    const fetchData = async () => {
        setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.packCategoryGet()
            .then((response) => {
                setData(response.data);
                setDataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setDataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    };


    const handleSubmitAdd = async (formData) => {
        props.setLoader(true);
        //setActionLoader(dt => ({ ...dt, loading: true, error: false, rd: true }));
        props.packCategoryAdd(formData)
            .then((response) => {
                fetchData();
                //const newData = [...data, response.data];
                //setData(newData);
                document.getElementById("close-modal").click();
                document.getElementById("form").reset();
                //setActionLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                //setActionLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            }).then(()=>props.setLoader(false))
            
    };


    const handleSubmitEdit = async (formData, Id) => {
        //setActionLoader(dt => ({ ...dt, loading: true, error: false, rd: true }));
        props.setLoader(true);
        props.packCategoryEdit(Id, formData)
            .then((response) => {
                setData(data.map(d => d._id === Id ? response.data : d));
                document.getElementById("close-modal").click();
                document.getElementById("form").reset();
                //props.setLoader(false);
                //setActionLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                //props.setLoader(false);
                //setActionLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            }).then(()=>props.setLoader(false))
    }

    const deleteData = async (Id) => {
        const conf = window.confirm('Do you want to delete selected value?');
        if(!conf)
           return false;

           props.setLoader(true);
        //setActionLoader(dt => ({ ...dt, loading: true, error: false, rd: true }));
        const response = await props.packCategoryDelete(Id);
        setData(data.filter(data => data._id !== Id));
        //setActionLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
        props.setLoader(false);
    };

    const handleOpenEdit = (row) =>{
        setModalTitle(`Edit ${title}`)
        setEditMode(true);
        setEditData(row);
        
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        
        fetchData();
    }, [])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions />}
            pageContentCss={`page-content-table`}
        >


            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col" className="cell-200 text-center">status</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                    />)
                            })}
                        </tbody>
                    </table>
                )}


                <div className="modal fade" id="modalForm" aria-hidden="true" aria-labelledby="modalForm"
            role="dialog" tabIndex="-1">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    {editMode?(
                    <Edit 
                       handleSubmitEdit={handleSubmitEdit}
                       editData={editData}
                       clearFormData={clearFormData}
                    />
                    ):(
                    <Add handleSubmitAdd={handleSubmitAdd} />
                    )}
                    


                </div>
            </div>
        </div>
        {/* <Toastr 
           trigger={toast}
        /> */}

        {/* <a className="btn btn-primary btn-outline" 
        data-plugin="toastr" style={{display:'none'}}
        ref={click => this.click = click}
                      data-message={toast.msg}
                      data-container-id="toast-top-right" data-title={toast.title} 
                      data-icon-class={`toast-just-text toast-${toast.type}`}
                      href="javascript:void(0)" role="button"></a> */}

        </MasterInner>
    );
}



export default withContext(Home);