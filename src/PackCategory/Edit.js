import React, { useState, useRef, Fragment } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';


const Edit = props => {
    const form = useRef(null);
    const [values, setValues] = useState(props.editData);

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        await form.current.validateFields();

        if (form.current.isValid()) {
            props.handleSubmitEdit(values, values._id);
        }
    }


    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">


                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            value={values.name}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" value={values.status} onChange={handleChange} 
                        className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>


                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal" 
                    onClick={props.clearFormData}
                    data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default Edit;