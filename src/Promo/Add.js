import React, { useState, useRef, Fragment, useEffect } from "react";
import {
    FormWithConstraints, FieldFeedbacks, FieldFeedback,
    Async as Async_, AsyncProps
} from 'react-form-with-constraints';
import * as library from './../Components/libraries';

const Add = props => {
    const form = useRef(null);
    const [values, setValues] = useState({
        promo_type: '',
        status: true
    });

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const target = e.target;
        await form.current.validateFields();

        const name = target.elements.name.value;
        const code = target.elements.code.value;
        const percentage = target.elements.percentage.value;
        const promo_type = target.elements.promo_type.value;
        const restaurant = target.elements.restaurant ? target.elements.restaurant.value : null;
        const expire = target.elements.expire.value;
        const status = target.elements.status.value;

        const data = { name, code, percentage, promo_type, restaurant, expire, status }

        if (form.current.isValid()) {
            props.handleSubmitAdd(data);
        }
    }


    return (
        <Fragment>
            <FormWithConstraints id="form-add" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Promo Code</label>
                                <input type="text" className="form-control"
                                    onChange={handleChange} value={values.code}
                                    name="code" placeholder="Promo Code" required
                                />

                                <FieldFeedbacks for="code">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>

                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Discount Percentage</label>
                                <div className="input-group">
                                    <input type="number" className="form-control"
                                        onChange={handleChange} min="1" value={values.percentage}
                                        name="percentage" placeholder="Discount Percentage"
                                    />
                                    <span className="input-group-btn">
                                        <button type="button" className="btn btn-default btn-outline">%</button>
                                    </span>
                                </div>

                            <FieldFeedbacks for="percentage">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <label>Promo Source</label>
                        <select name="promo_type" onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value="GENERAL">GENERAL</option>
                            <option value="VENDOR">VENDOR</option>
                        </select>

                        <FieldFeedbacks for="promo_type">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>



                    {values.promo_type === 'VENDOR' && (<div className="form-group">
                        <label>Restaurant</label>
                        <select name="restaurant" className="form-control" onChange={handleChange}>
                            <option value="" >- Vendor -</option>
                            {library.checkObject(props.restaurants) && props.restaurants.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>

                        <FieldFeedbacks for="restaurant">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>)}

                    <div className="form-group">
                        <label>Expires On</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} onPaste={handleChange} data-plugin="datepicker"
                            name="expire" placeholder="Expires On" autoComplete="off" required
                        />

                        <FieldFeedbacks for="expire">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" value={values.status} onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal-add"
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default Add;