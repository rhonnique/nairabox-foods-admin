import React, { Fragment } from "react";
import moment from "moment";

const TableRow = ({row, handleOpenEdit, handleDeletedata}) => {
    
    return (
        <tr>
            
            <td>{row.name}</td>
            <td>{row.code}</td>
            <td>{row.percentage}%</td>
            <td>{row.promo_type}</td>
            <td>{moment(row.expire).format(`MMM D, YYYY`)}</td>
            <td className="text-center">
                {row.status?(
                    <span className="badge badge-outline badge-success">Active</span>
                ):(
                    <span className="badge badge-outline badge-default">Inactive</span>
                )}
            
            </td>
            <td className="text-right">{moment(row.created_at).format(`MMM D, YYYY[\n\n]h:mm a`)}</td>
            <td className="cell-30">
                <div className="btn-group dropdown">
                    <button type="button" data-toggle="dropdown" aria-expanded="false"
                        className="btn btn-pure btn-default icon wb-more-vertical"></button>
                    <div className="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">
                        <a className="dropdown-item" onClick={() => handleOpenEdit(row)} 
                        href="javascript:void(0)" role="menuitem"
                        data-toggle="modal" data-target="#modalFormEdit">Edit</a>
                        <a onClick={() => handleDeletedata(row._id)} 
                        className="dropdown-item" href="javascript:void(0)" role="menuitem">Delete</a>
                    </div>
                </div>
            </td>
        </tr>

    );
}

export default TableRow;