import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import MasterInner from "../layout/MasterInner";
import TableRow from "./TableRow";
import Add from "./Add";
import Edit from "./Edit";
import * as library from './../Components/libraries'
import $ from 'jquery'

var pluralize = require('pluralize');

const initEditData = () =>{
    return {
        id:'',
        name:'',
        code:'',
        percentage:'',
        promo_type:'',
        restaurant:'',
        expire:'',
        status:''
    }
}

const Home = props => {
    const { api_url } = props;
    const title = `Promotion`;
    const permission = `PROMOTIONS`;

    const [data, setData] = useState([]);
    const [dataLoader, setDataLoader] = useState(false);

    
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState(initEditData);

    const [restaurants, setRestaurants] = useState();

    const Actions = () => {
        return (
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)" 
            data-toggle="modal" data-target="#modalFormAdd" 
                onClick={() => setModalTitle(`Add ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> Add {title}
        </a>
        )
    }

    // Get main data
    const fetchData = async () => {
        setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.promoGet('')
            .then((response) => {
                setData(response.data);
                setDataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setDataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            });
    };

    // Get Vendor
    const fetchRestaurant = async () => {
        props.vendorSelect()
            .then((response) => {
                setRestaurants(response.data);
            })
            .catch(err => {
            });
    };


    const handleSubmitAdd = async (formData) => {
        props.setLoader(true);
        props.promoAdd(formData)
            .then(() => {
                fetchData();
                document.getElementById("close-modal-add").click();
                document.getElementById("form-add").reset();
            })
            .catch(error => {
                if(error && error.response){
                    if(error.response.status === 409){
                        props.setToastr("Duplicate Error",'Promotion code already exist','error')
                    }
                } else {
                    props.setToastr("Error",'An error occured, please retry','error')
                }
            }).then(()=>props.setLoader(false))
            
    };


    const handleSubmitEdit = async (formData, Id) => {
        props.setLoader(true);
        props.promoEdit(Id, formData)
            .then((response) => {
                setData(data.map(d => d._id === Id ? response.data : d));
                document.getElementById("close-modal-edit").click();
                document.getElementById("form-edit").reset();
            })
            .catch(error => {
                if(error && error.response){
                    if(error.response.status === 409){
                        props.setToastr("Duplicate Error",'Promotion code already exist','error')
                    }
                } else {
                    props.setToastr("Error",'An error occured, please retry','error')
                }
            }).then(()=>props.setLoader(false))
    }

    const deleteData = async (Id) => {
        const conf = window.confirm('Do you want to delete selected value?');
        if(!conf)
           return false;

        props.setLoader(true);
        const response = await props.promoDelete(Id);
        setData(data.filter(data => data._id !== Id));
        props.setLoader(false);
    };

    const handleOpenEdit = (row) =>{
        setEditData(row);
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData(initEditData)
    }

    useEffect(() => {
        fetchRestaurant();
        fetchData();
    }, [])

    
    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions />}
            pageContentCss={`page-content-table`}
        >


            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Promo Code</th>
                                <th scope="col">Discount %</th>
                                <th scope="col">Promo Source</th>
                                <th scope="col">Expires At</th>
                                <th scope="col" className="text-center">status</th>
                                <th scope="col" className="text-right">Created At</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                    />)
                            })}
                        </tbody>
                    </table>
                )}


                <div className="modal fade" id="modalFormAdd" aria-hidden="true" aria-labelledby="modalFormAdd"
            role="dialog" tabIndex="-1">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">Add {title}</h4>
                    </div>
                    <Add handleSubmitAdd={handleSubmitAdd} restaurants={restaurants} />
                </div>
            </div>
        </div>


        <div className="modal fade" id="modalFormEdit" aria-hidden="true" aria-labelledby="modalFormEdit"
            role="dialog" tabIndex="-1">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">Edit {title}</h4>
                    </div>
                    <Edit 
                       handleSubmitEdit={handleSubmitEdit}
                       editData={editData}
                       clearFormData={clearFormData}
                       restaurants={restaurants}
                       
                    />
                </div>
            </div>
        </div>

        </MasterInner>
    );
}



export default withContext(Home);