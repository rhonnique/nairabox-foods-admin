import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "./../AppContext";
import { Route, Redirect } from "react-router-dom";
import MasterInner from "../layout/MasterInner";
import TableRow from "./TableRow";
import Toastr from "../Components/Toastr";
import Add from "./Add";
import Edit from "./Edit";
import * as library from './../Components/libraries'
import $ from 'jquery'

var pluralize = require('pluralize');

const Home = props => {
    const { api_url } = props;
    const title = `Admin Accounts`;
    const api_path = `admin`;
    const permission = `ADMIN`;

    const [data, setData] = useState([]);
    const [dataLoader, setdataLoader] = useState({});
    const [restaurants, setRestaurants] = useState([]);
    const [permissions, setPermissions] = useState([]);
    
    const [modalTitle, setModalTitle] = useState('');
    const [editMode, setEditMode] = useState(false);
    const [editData, setEditData] = useState({});

    const [formReset, setFormReset] = useState(false);

    const Actions = () => {
        return (
            <a id="addLabelToggle" className="list-group-item" href="javascript:void(0)" 
            data-toggle="modal" data-target="#modalForm" 
                onClick={() => setModalTitle(`Add ${title}`)}>
                <i className="icon wb-plus" aria-hidden="true"></i> Add {title}
        </a>
        )
    }

    // Get main data
    const fetchData = async () => {
        setdataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.adminGet()
            .then(response =>{
                setData(response.data);
                setdataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setdataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            })
    };

    const fetchRestaurant = async () => {
        props.vendorSelect()
            .then((response) => {
                setRestaurants(response.data);
            })
            .catch(err => {
                props.setToastr('Reastaurant Error!','Can not fetch restaurant, please refresh page','error');
            });
    };

    const fetchPermission = async () => {
        props.permissionGet()
            .then((response) => {
                setPermissions(response.data);
            })
            .catch(err => {
                props.setToastr('Permission Error!','Can not fetch permissions, please refresh page','error');
            });
    };

    const handleSubmitAdd = async (formData) => {
        props.setLoader(true);
        props.adminAdd(formData)
            .then(() => {
                fetchData();
                setFormReset(true);
                document.getElementById("close-modal").click();
                document.getElementById("form").reset();
            })
            .catch(err => {
                props.setToastr('Add Error!','An error occurred, please retry','error');
            }).then(props.setLoader(false))
    };


    const handleSubmitEdit = async (formData, Id) => {
        
    }

    const deleteData = async (Id) => {
        const conf = window.confirm('Are you sure, user will be deleted?');
        if(!conf){
            return;
        }

        props.setLoader(true);
        props.adminDelete(Id)
            .then(() => {
                setData(data.filter(data => data._id !== Id));
            })
            .catch(err => {
                props.setToastr('Add Error!','An error occurred, please retry','error');
            }).then(props.setLoader(false))
    };

    const handleOpenEdit = (row) =>{
        setModalTitle(`Edit ${title}`)
        setEditMode(true);
        setEditData(row);
        
    }

    const handleDeletedata = id =>{
        deleteData(id)
    }

    const clearFormData = () => {
        setEditMode(false);
        setEditData({})
    }

    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        
        fetchData();
        fetchRestaurant();
        fetchPermission();
    }, [])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (<Fragment>
        
        <MasterInner
            setHeader={true}
            title={pluralize(title)}
            actions={<Actions />}
            pageContentCss={`page-content-table`}
        >

            {dataLoader.loading && (<div>Loading ...</div>)}
            {dataLoader.error && (<div>an error occured</div>)}
            {dataLoader.rd && data.length === 0  && (<div>No result found!</div>)}

            {dataLoader.rd && data.length > 0 && (
                    <table className="table is-indent" data-plugin="animateList" data-animate="fade" data-child="tr"
                        data-selectable="selectable">
                        <thead>
                            <tr>
                                <th scope="col">Username</th>
                                <th scope="col">Email</th>
                                <th scope="col">Name</th>
                                <th scope="col" className="cell-200 text-center">status</th>
                                <th className="cell-30" scope="col">
                                <button type="button" className="btn btn-pure btn-default 
                                icon wb-more-vertical"></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((row, index) => {
                                return (
                                    <TableRow
                                        key={index}
                                        row={row}
                                        handleOpenEdit={handleOpenEdit}
                                        handleDeletedata={handleDeletedata}
                                    />)
                            })}
                        </tbody>
                    </table>
                )}


                <div className="modal fade" id="modalForm" aria-hidden="true" aria-labelledby="modalForm"
            role="dialog" tabIndex="-1">
            <div className="modal-dialog modal-simple">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={clearFormData} aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 className="modal-title">{modalTitle}</h4>
                    </div>
                    {editMode?(
                    <Edit 
                    restaurants={restaurants}
                    permissions={permissions}
                       handleSubmitEdit={handleSubmitEdit}
                       editData={editData}
                       clearFormData={clearFormData}
                       formReset={formReset}
                    />
                    ):(
                    <Add 
                    restaurants={restaurants}
                    permissions={permissions}
                    handleSubmitAdd={handleSubmitAdd}
                    formReset={formReset}
                     />
                    )}
                    


                </div>
            </div>
        </div>

        </MasterInner>
        </Fragment>);
}



export default withContext(Home);