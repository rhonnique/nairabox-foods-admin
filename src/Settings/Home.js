import React, { Fragment, useState, useEffect, useRef } from "react";
import { withContext } from "./../AppContext";
import { Route, Redirect } from "react-router-dom";
import MasterInner from "../layout/MasterInner";
import TableRow from "./TableRow";
import Toastr from "../Components/Toastr";
import Add from "./Add";
import Edit from "./Edit";
import * as library from './../Components/libraries'
import $ from 'jquery'
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';


var pluralize = require('pluralize');

const Home = props => {
    const { api_url } = props;
    const title = `Settings`;
    const api_path = `admin`;
    const permission = `SETTINGS`;

    const form = useRef(null);
    //const [values, setValues] = useState(props.editData);

    const [data, setData] = useState({});
    const [dataLoader, setdataLoader] = useState({});


    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setData({ ...data, [name]: value })
    }


    // Get main data
    const fetchData = async () => {
        setdataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
        props.settingsGet()
            .then(response => {
                setData(response.data);
                setdataLoader(dt => ({ ...dt, loading: false, error: false, rd: true }));
            })
            .catch(err => {
                setdataLoader(dt => ({ ...dt, loading: false, error: true, rd: false }));
            })
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        await form.current.validateFields();

        if (form.current.isValid()) {
            props.setLoader(true);
            props.settingsEdit(data._id, data)
                .then(response => {
                    setData(response.data);
                    props.setLoader(false);
                    props.setToastr('Success','Settings successfully modified','success')
                })
                .catch(err => {
                    props.setLoader(false);
                    props.setToastr('Error','An error occurred, please retry','error')
                })
        }

    }

    useEffect(() => {
        {(function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                Site.run();
            });
        })(document, window, $)}
        fetchData();
    }, [])

    useEffect(() => {
        library.permission(props.user, permission, props.host)
    }, [props.user])

    return (<Fragment>

        <MasterInner
            setHeader={true}
            title={pluralize(title)}
        //pageContentCss={`page-content-table`}
        >

            <div className="row py-25">
                <div className="col-md-7">
                    <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>

                        <div className="form-group">
                            <label>Vat Rate</label>
                            <div className="input-group">
                                <input type="number" className="form-control"
                                    onChange={handleChange}
                                    value={data.vat_rate} min="0"
                                    name="vat_rate" placeholder="Vat Rate" required
                                />
                                <span className="input-group-btn">
                                    <button type="button" className="btn btn-default btn-outline">%</button>
                                </span>
                            </div>
                            <FieldFeedbacks for="vat_rate">
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>


                        <div className="form-group">
                            <label>Consumption Rate</label>
                            <div className="input-group">
                                <input type="number" className="form-control"
                                    onChange={handleChange}
                                    value={data.consumption_rate} min="0"
                                    name="consumption_rate" placeholder="Consumption Rate" required
                                />
                                <span className="input-group-btn">
                                    <button type="button" className="btn btn-default btn-outline">%</button>
                                </span>
                            </div>

                            <FieldFeedbacks for="consumption_rate">
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>

                        <br />

                        <h4>Delivery Settings</h4>


                        <div className="form-group">
                            <label>Delivery Base Rate</label>
                            <input type="number" className="form-control"
                                onChange={handleChange}
                                value={data.delivery_base_amount} min="0"
                                name="delivery_base_amount" placeholder="Delivery Base Rate" required
                            />

                            <FieldFeedbacks for="delivery_base_amount">
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>


                        <div className="form-group">
                            <label>Delivery Rate Per km</label>
                            <input type="number" className="form-control"
                                onChange={handleChange}
                                value={data.amount_per_distance} min="0"
                                name="amount_per_distance" placeholder="Delivery Rate Per km" required
                            />

                            <FieldFeedbacks for="amount_per_distance">
                                <FieldFeedback when="*" />
                            </FieldFeedbacks>
                        </div>

                        <div className="form-group">
                            <button className="btn btn-primary" type="submit">Update Settings</button>
                        </div>
                    </FormWithConstraints>

                </div>
            </div>


        </MasterInner>
    </Fragment>);
}



export default withContext(Home);