import React, { useState, useRef, Fragment, useEffect } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "./../AppContext";
import * as library from './../Components/libraries';
import $ from 'jquery'

const initValues = {
    type: '',
    restaurant: '',
    username: '',
    name: '',
    email: '',
    set_password: '',
    status: true
}

const Add = props => {
    const form = useRef(null);
    const password = useRef(null);
    //const set_password = useRef(null);
    //const [set_password, setSet_password] = useState(false);
    const [values, setValues] = useState(initValues);

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        //let target = e.target.elements;
        await form.current.validateFields();

        if (form.current.isValid()) {
            const selected = document.getElementsByName('selected_permission');
            var selected_permission = [];
            for (var i = 0; i < selected.length; i++) {
                if (selected[i].checked) {
                    selected_permission.push(selected[i].value);
                }
            }

            const data = Object.assign(values, { 'permissions': selected_permission })
            props.handleSubmitAdd(data)
        } else {
            props.setToastr('Validation Error!', 'Fields required', 'error');
        }
    }

    const setPassword = (e) => {
        if (e.target.checked) {
            password.current.disabled = false;
            //setSet_password(true)
            setValues({ ...values, 'set_password': true })
            $("[name=password]").attr('required', true);
        } else {
            password.current.disabled = true;
            //setSet_password(false)
            setValues({ ...values, 'set_password': false })
            $("[name=password]").attr('required', false);
        }
    }

    useEffect(() => {
        /* console.log(props.formReset, 'props.formReset...')
        //form.current.reset();
        if(props.formReset === true){
            document.getElementById("form").reset();
        } */
        setValues(initValues);
    }, [props.formReset])

    useEffect(() => {
        if (values.restaurant) {
            const select = props.restaurants.filter(s => s._id === values.restaurant);
            setValues({ ...values, 'name': select[0].name, 'username': select[0].contact.phone })
            console.log(select, 'select....')
        }

    }, [values.restaurant])


    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Account Type</label>
                        <select name="type" onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value="ADMIN">Admin</option>
                            <option value="VENDOR">Vendor</option>
                            <option value="PARTNER">Partner</option>
                        </select>

                        <FieldFeedbacks for="type">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    {values.type === 'VENDOR' && (<div className="form-group">
                        <label>Vendor</label>
                        <select name="restaurant" className="form-control"
                            required onChange={handleChange}>
                            <option value="" >- Select -</option>
                            {library.checkObject(props.restaurants) && props.restaurants.map((rs, index) => {
                                return (<option key={index} value={rs._id}>{rs.name}</option>)
                            })}
                        </select>

                        <FieldFeedbacks for="restaurant">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>)}



                    {/* ****** Permissions ****** */}
                    {values.type === 'ADMIN' && (<div className="form-group">
                        <label>Permissions</label>
                        {library.checkObject(props.permissions) && (
                            <div className="row">
                                {props.permissions.map((row, index) => {
                                    return (<div key={index} className="col-sm-4">
                                        <div className="checkbox-custom checkbox-primary checkbox-inline">
                                            <input name="selected_permission"
                                                value={row._id} type="checkbox" id={'permission_' + row._id} />
                                            <label htmlFor={'permission_' + row._id}>{row.name}</label>
                                        </div>
                                    </div>)
                                })}
                            </div>

                        )}
                    </div>)}
                    {/* ****** /Permissions ****** */}






                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} value={values.name}
                            name="name" placeholder="Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Email Address</label>
                        <input type="email" className="form-control"
                            onChange={handleChange} value={values.email}
                            name="email" placeholder="Email Address"
                        />
                    </div>

                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} value={values.username}
                            name="username" placeholder="Username" required
                        />

                        <FieldFeedbacks for="username">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <div className="checkbox-custom checkbox-primary">
                            <input name="set_password" id="set_password" onChange={setPassword}
                                type="checkbox" />
                            <label htmlFor="set_password">Set Password Manually</label>
                        </div>

                        <input type="password" className="form-control"
                            onChange={handleChange} ref={password}
                            name="password" placeholder="Password" disabled
                        />
                        {!values.set_password && (<div className="text-primary">Password will be sent to user's contact</div>)}

                        <FieldFeedbacks for="password">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>

                    </div>

                    <div className="form-group">
                        <label>Status</label>
                        <select name="status" value={values.status} onChange={handleChange}
                            className="form-control" required>
                            <option value="">- Select -</option>
                            <option value={true}>Active</option>
                            <option value={false}>Inactive</option>
                        </select>

                        <FieldFeedbacks for="status">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal"
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default withContext(Add);